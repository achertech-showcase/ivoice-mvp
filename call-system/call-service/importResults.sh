#usage: importResults.sh <client> <campaign>
docker cp ../../clients/$1/$2-res.json recogn-mongo:/$1-$2-res.json
docker exec recogn-mongo mongoimport --upsert --drop --db callsystem --collection callTask --file $1-$2-res.json