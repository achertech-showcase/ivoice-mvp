// ++++++ campaign
//query:
db.getCollection('campaign').find({name:/nils_samara.*/})

// export (local)
mongoexport --db callsystem --collection campaign --query '{name:/nils_samara.*/}' --out nils_samara.json

// import (remote)
mongoimport --port 27710 --db callsystem --collection campaign --file nils_samara.json

// +++++++ task
db.getCollection('callTask').find({campaign:/nils_samara.*/}).count()

// export (local)
mongoexport --db callsystem --collection callTask --query '{campaign:/nils_samara.*/}' --out nils_samara_tasks.json

// import (remote)
mongoimport --port 27710 --db callsystem --collection callTask --file nils_samara_tasks.json