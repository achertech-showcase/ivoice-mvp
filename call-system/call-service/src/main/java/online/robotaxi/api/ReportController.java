package online.robotaxi.api;

import com.fasterxml.jackson.databind.ObjectMapper;
import online.robotaxi.model.CallTask;
import online.robotaxi.repository.CallTaskRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.util.StringUtils;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.io.UnsupportedEncodingException;
import java.time.Instant;
import java.time.LocalDateTime;
import java.time.ZoneId;
import java.time.format.DateTimeFormatter;
import java.util.Arrays;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

import static java.lang.String.format;

@RestController
@RequestMapping("report")
public class ReportController {
    private static final ObjectMapper OBJECT_MAPPER = new ObjectMapper();
    private static DateTimeFormatter DT_FORMATTER = DateTimeFormatter.ofPattern("dd.MM.yyyy HH:mm");
    private final CallTaskRepository callTaskRepository;

    @Autowired
    public ReportController(CallTaskRepository callTaskRepository) {
        this.callTaskRepository = callTaskRepository;
    }

    @RequestMapping("campaign/{name}/csv")
    public String reportCvs(@PathVariable("name") String campaignName) throws UnsupportedEncodingException {
        List<CallTask> tasks = callTaskRepository.findAllByCampaign(campaignName);
        if (tasks.isEmpty()) {
            return "";
        }
        StringBuilder result = new StringBuilder();

        CallTask firstTask = tasks.get(0);
        boolean hasOriginalId = !firstTask.getOriginalDbId().isEmpty();

        result.append("id задачи,");
        if (hasOriginalId) {
            result.append("id исходной бд,");
        }
        result.append("телефон,")
                .append("диалог состоялся,")
                .append("время звонка (мск),")
                .append("результат,")
                .append("уверенность,")
                .append("история распознавания,")
                .append("попыток дозвониться,")
                .append("постпроцесс-распознавание")
                .append("\r\n");

        tasks.forEach(t -> {
            result.append(t.getId().toString()).append(",");

            if (hasOriginalId) {
                result.append(t.getOriginalDbId()).append(",");
            }

            result.append(t.getTargetPhone()).append(",")
                    .append(ruStatus(t.getState())).append(",");

            if (t.getState() == CallTask.State.PERFORMED) {
                LocalDateTime initDt;
                if (t.getLog().containsValue("инициирован звонок")) {
                    //noinspection ConstantConditions
                    Map.Entry<Long, String> entry = t.getLog().entrySet().stream()
                            .filter(e -> e.getValue().equals("инициирован звонок"))
                            .sorted((e1, e2) -> Long.compare(e2.getKey(), e1.getKey()
                            ))
                            .findFirst().get();
                    initDt = Instant.ofEpochSecond(entry.getKey()).atZone(ZoneId.systemDefault()).toLocalDateTime();

                } else {
                    Long initTimestamp = t.getLog().keySet().iterator().next();
                    initDt = Instant.ofEpochSecond(initTimestamp).atZone(ZoneId.systemDefault()).toLocalDateTime();
                }

                result.append(DT_FORMATTER.format(initDt)).append(",");
                if (!StringUtils.isEmpty(t.getResult())) {
                    result.append(t.getResult()).append(",");
                } else {
                    result.append("ошибка распознавания").append(",");
                }

                result.append(t.getResultConfidence()).append(",");
                result.append(parsedRecognitions(t.getRecognitions())).append(",");

            } else if (t.getState() == CallTask.State.FAILED) {
                Long initTimestamp = t.getLog().keySet().iterator().next();
                LocalDateTime initDt = Instant.ofEpochSecond(initTimestamp).atZone(ZoneId.systemDefault()).toLocalDateTime();
                result.append(DT_FORMATTER.format(initDt)).append(",");
                if (t.getResult().contains("ошибка")) {
                    result.append("ошибка программы,");
                } else if (t.getResult().contains("исчерпаны")) {
                    result.append("недозвонились,");
                } else {
                    result.append(t.getResult()).append(","); // result
                }

                result.append(" ").append(","); // confidence
                result.append(" ").append(","); // recognitions
            } else {
                result.append("-,-,-,-,");
            }

            result.append(t.getAttemptsPerformed()).append(",");
            if (!StringUtils.isEmpty(t.getPostprocessResult())){
                result.append(t.getPostprocessResult());
            } else {
                result.append(" ,");
            }
            result.append("\r\n");
        });
        return result.toString();
    }

    private String parsedRecognitions(String recognitionsStr) {
        if (StringUtils.isEmpty(recognitionsStr)) {
            return "";
        }
        RecognitionDto[] recognitions;
        try {
            recognitions = OBJECT_MAPPER.readValue(recognitionsStr, RecognitionDto[].class);
            //recognitions = OBJECT_MAPPER.convertValue(recognitionsStr, List.class);
        } catch (Exception e) {
            return "";
        }

        return Arrays.stream(recognitions)
                .map(r -> format("%s(%s)", r.getInput(), r.getConfidence()))
                .collect(Collectors.joining(";"));
    }

    @RequestMapping("campaign/{name}/table")
    public String reportTable(@PathVariable("name") String campaignName) {
        List<CallTask> tasks = callTaskRepository.findAllByCampaign(campaignName);
        StringBuilder result = new StringBuilder();
        result.append("<table>");
        tasks.forEach(t -> {
            result.append("<tr>");
            result.append("<td>").append(t.getId().toString()).append("</td>")
                    .append("<td>").append(t.getTargetPhone()).append("</td>")
                    .append("<td>").append(ruStatus(t.getState())).append("</td>");

            if (t.getState() == CallTask.State.PERFORMED || t.getState() == CallTask.State.FAILED) {
                Long initTimestamp = t.getLog().keySet().iterator().next();
                LocalDateTime initDt = Instant.ofEpochSecond(initTimestamp).atZone(ZoneId.systemDefault()).toLocalDateTime();
                result.append("<td>").append(initDt.toString()).append("</td>");
            }

            result.append("<td>").append(t.getResult()).append("</td>");
            result.append("<td>").append(t.getAttemptsPerformed()).append("</td>");
            result.append("</tr>");
        });
        result.append("</table>");
        return result.toString();
    }

    private String ruStatus(CallTask.State state) {
        switch (state) {
            case NOT_PERFORMED:
                return "ожидает обработки";
            case ONGOING:
                return "сейчас звоним";
            case PERFORMED:
                return "да";
            case FAILED:
                return "нет";
            default:
                throw new IllegalArgumentException(format("unexpected task state %s", state));
        }
    }
}
