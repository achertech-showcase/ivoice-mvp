package online.robotaxi.api;

import online.robotaxi.coldcall.ColdCallCampaignService;
import online.robotaxi.coldcall.TaskService;
import online.robotaxi.freeswitch.InboundClient;
import online.robotaxi.model.Address;
import online.robotaxi.model.CallTask;
import online.robotaxi.model.TaxiOrder;
import online.robotaxi.repository.CallTaskRepository;
import online.robotaxi.repository.TaxiOrderRepository;
import org.freeswitch.esl.client.transport.SendMsg;
import org.freeswitch.esl.client.transport.message.EslMessage;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Sort;
import org.springframework.util.StringUtils;
import org.springframework.web.bind.annotation.*;

import java.time.Instant;
import java.time.LocalDateTime;
import java.time.ZoneId;
import java.time.format.DateTimeFormatter;
import java.util.Collections;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

import static java.lang.String.format;

@RestController
@RequestMapping("demo-api")
public class DemoApiController {
    private static final Logger LOG = LoggerFactory.getLogger(DemoApiController.class);
    private static DateTimeFormatter DT_FORMATTER = DateTimeFormatter.ofPattern("dd.MM.yyyy HH:mm");

    private final TaskService taskService;
    private final ColdCallCampaignService campaignService;
    private final CallTaskRepository callTaskRepository;
    private final TaxiOrderRepository taxiOrderRepository;

    private final InboundClient client;

    @Autowired
    public DemoApiController(
            TaskService taskService,
            ColdCallCampaignService campaignService,
            CallTaskRepository callTaskRepository,
            TaxiOrderRepository taxiOrderRepository, InboundClient client
    ) {
        this.taskService = taskService;
        this.campaignService = campaignService;
        this.callTaskRepository = callTaskRepository;
        this.taxiOrderRepository = taxiOrderRepository;
        this.client = client;
    }

    @RequestMapping("orders/last")
    public @ResponseBody
    List<OrderDto> lastOrders() {
//        List<OrderDto> all = taxiOrderRepository.findAll().stream()
                List<OrderDto> all = taxiOrderRepository.findAllByOrderByIdDesc().stream()
                .map(
                        r -> new OrderDto(
                                formattedTime(r.getCreatedTime()),
                                hiddenPhone(r.getPhone()),
                                fullAddress(r.getArrivalAddress()),
                                r.getComments()
                        )
                ).collect(Collectors.toList());
        return all.subList(0, Math.min(10, all.size()));
    }

    @RequestMapping("orders/last/format")
    public @ResponseBody
    String lastOrdersAsList() {
        StringBuilder sb = new StringBuilder("<ul>");
        lastOrders().forEach(o -> {
            sb.append("<li>");
            sb.append(format(
                    "%s, телефон: %s; адрес подачи: %s; комментарий: %s",
                    o.getCreated(),
                    o.getPhone(),
                    o.getAddressFrom(),
                    o.getComments()
            ));
            sb.append("</li>");
        });

        sb.append("</ul>");
        return sb.toString();
    }

    @RequestMapping("call/list/{campaign}/{phone}")
    public String calls(@PathVariable("campaign") String campaign, @PathVariable("phone") String targetPhone) {
        List<CallTask> tasks = callTaskRepository.findAllByCampaignAndTargetPhone(campaign, targetPhone);
        StringBuilder sb = new StringBuilder();
        sb.append("<div>");

        tasks.forEach(t -> {
            sb.append("<div>");

            sb.append("<p>");
            String phone = t.getTargetPhone();
            if (phone.length() > 4) {
                phone = phone.substring(phone.length() - 4, phone.length());
            }

            sb.append("Лог обработки звонка по номеру #-###-##-").append(phone)
                    .append(", id=").append(t.getId()).append(":");
            sb.append("</p>");

            if (t.getState() == CallTask.State.PERFORMED) {
                sb.append("<p>");
                sb.append("Результат: ").append(t.getResult()).append(" (").append(t.getResultConfidence()).append(")");
                sb.append("<br>Распознавания: ").append(t.getRecognitions());
                if (!StringUtils.isEmpty(t.getPostprocessResult())) {
                    sb.append("<br>Постпроцессинг: ").append(t.getPostprocessResult());
                }
                sb.append("</p>");
            }
            sb.append("cdr: <br>").append("<textarea>").append(t.getCdr()).append("</textarea>");
            sb.append("<p>").append(t.getState()).append("</p>");

            sb.append("<ul>");
            t.getLog().forEach((timeSeconds, value) -> {
                LocalDateTime ldt = Instant.ofEpochSecond(timeSeconds).atZone(ZoneId.systemDefault()).toLocalDateTime();

                sb.append("<li>");

                sb.append(ldt.toString());
                sb.append(" - ");
                sb.append(value);

                sb.append("</li>");
            });
            sb.append("</ul>");

            sb.append("</div>");
            sb.append("<hr>");
        });

        sb.append("</div>");

        return sb.toString();
    }

    @RequestMapping("call/add")
    public void addInnerCallTask(String campaign, String phone, Boolean restartCampaign) {
        taskService.create(campaign, phone);
        if (Optional.ofNullable(restartCampaign).isPresent() && restartCampaign) {
            campaignService.start(campaign);
        }
    }

    @RequestMapping("sofia/status")
    public String sofiaStatus() {
        EslMessage response = client.sendSyncApiCommand("sofia status", "");
        return String.join(", ", response.getBodyLines());
    }

    @RequestMapping("test/localcall/{phone}/{script}")
    public String originateTestingCall(@PathVariable("phone") String phone, @PathVariable("script") String script) {
        LOG.info(format("originating call to %s", phone));
        // originating call using gateway: originate sofia/gateway/megafon/79771368592 yesno_demo
        EslMessage response = client.sendSyncApiCommand(
                format("originate [script=%s]user/%s@192.168.1.100 calltask", script, phone),
                ""
        );
        return String.join(", ", response.getBodyLines());
    }

    @RequestMapping("test/call/{gateway}/{phone}")
    public String originateTestingGatewayCall(
            @PathVariable("gateway") String gateway,
            @PathVariable("phone") String phone
    ) {
        LOG.info(format("originating call to gateway %s, phone %s", gateway, phone));
        EslMessage response = client.sendSyncApiCommand(
                format("originate sofia/gateway/%s/%s yesno_demo", gateway, phone),
                ""
        );
        return String.join(", ", response.getBodyLines());
    }

    @RequestMapping("test/sendMsg")
    public void sendEvent(@RequestParam("uuid") String uuid) {
        SendMsg msg = new SendMsg(uuid);
        /*
        sendmsg <uuid>
        call-command: execute
        execute-app-name: playback
        execute-app-arg: /tmp/test.wav
         */
//        msg.addCallCommand("execute");
//        msg.addExecuteAppName("set"); // set variable
//        msg.addExecuteAppArg("varFromServer=hi");
//
//        CommandResponse commandResponse = client.sendMessage(msg);
//        LOG.info(commandResponse.getReplyText());

        LOG.info(client.sendEvent(Collections.singletonList("serverValue:hi")).getHeaders().toString());
        LOG.info(client.sendEvent(Collections.singletonList("serverValue:hi2")).getHeaders().toString());
        LOG.info(client.sendEvent(Collections.singletonList("serverValue:hi3")).getHeaders().toString());
    }

    private String hiddenPhone(String phone) {
        if (StringUtils.isEmpty(phone)) {
            return "";
        }
        int phoneLength = phone.length();
        if (phoneLength < 4) {
            return "* * * *";
        }
        String hidden = phone.substring(0, phoneLength - 4);
        String notHidden = phone.substring(phoneLength - 4, phoneLength);
        //noinspection ReplaceAllDot
        hidden = hidden.replaceAll(".", "*");
        return hidden + notHidden;
    }

    private String fullAddress(Address arrivalAddress) {
        return format(
                "%s, %s, %s",
                arrivalAddress.getCity(),
                arrivalAddress.getStreet(),
                arrivalAddress.getHouse()
        );
    }

    private String formattedTime(LocalDateTime time) {
        return DT_FORMATTER.format(time);
    }
}
