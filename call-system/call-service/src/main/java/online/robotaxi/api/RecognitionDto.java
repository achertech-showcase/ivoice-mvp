package online.robotaxi.api;

public class RecognitionDto {
    private String input;
    private float confidence;

    public RecognitionDto() {
    }

    public RecognitionDto(String input, float confidence) {
        this.input = input;
        this.confidence = confidence;
    }

    public String getInput() {
        return input;
    }

    public float getConfidence() {
        return confidence;
    }
}
