package online.robotaxi.api;

import online.robotaxi.coldcall.Caller;
import online.robotaxi.coldcall.ColdCallCampaignService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("api")
public class ApiController {
    private final static Logger LOG = LoggerFactory.getLogger(ApiController.class);

    private final ColdCallCampaignService campaignService;
    private final Caller caller;

    @Autowired
    public ApiController(
            ColdCallCampaignService campaignService,
            Caller caller
    ) {
        this.campaignService = campaignService;
        this.caller = caller;
    }

//    @RequestMapping(value = "init", method = RequestMethod.POST)
//    public boolean initOrder(@RequestBody String orderId) {
//        if (StringUtils.isEmpty(orderId)) {
//            LOG.error("empty request from asterisk");
//            return false;
//        }
//        // fix '=' in the end of data from asterisk
//        orderId = orderId.substring(0, orderId.length() - 1);
//        //return scenarioService.init(orderId);
//        throw new RuntimeException("not implemented");
//    }

//    /**
//     * @param orderIdAndMeaning orderId__meaning
//     * @return record processed successfully, so dialog can progress forward
//     */
//    @RequestMapping(value = "recorded", method = RequestMethod.POST)
//    public boolean addressRequested(@RequestBody String orderIdAndMeaning) {
//        if (StringUtils.isEmpty(orderIdAndMeaning)) {
//            LOG.error("empty request from asterisk");
//            return false;
//        }
//        // fix '=' in the end of data from asterisk
//        orderIdAndMeaning = orderIdAndMeaning.substring(0, orderIdAndMeaning.length() - 1);
//        String[] split = orderIdAndMeaning.split("__");
//        String orderId = split[0];
//        String meaning = split[1];
////        return scenarioService.processRecord(orderId, meaning);
//        throw new RuntimeException("not implemented");
//    }

    @RequestMapping("ping")
    public String ping() {
        return "pong";
    }

    @RequestMapping("campaign/{name}/start")
    public boolean startCampaign(@PathVariable("name") String campaignName) {
        return campaignService.start(campaignName);
    }

    @RequestMapping("campaign/{name}/stop")
    public boolean stopCampaign(@PathVariable("name") String campaignName) {
        return campaignService.stop(campaignName);
    }

    @RequestMapping("caller/setLines")
    public int setCallerLines(@RequestParam("lines") int lines) {
        return caller.updateLines(lines);
    }

}
