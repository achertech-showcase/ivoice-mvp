package online.robotaxi.api;

public class OrderDto {
    private final String created;
    private final String phone;
    private final String addressFrom;
    private final String comments;

    OrderDto(String created, String phone, String addressFrom, String comments) {
        this.created = created;
        this.phone = phone;
        this.addressFrom = addressFrom;
        this.comments = comments;
    }

    public String getCreated() {
        return created;
    }

    public String getPhone() {
        return phone;
    }

    public String getAddressFrom() {
        return addressFrom;
    }

    public String getComments() {
        return comments;
    }
}
