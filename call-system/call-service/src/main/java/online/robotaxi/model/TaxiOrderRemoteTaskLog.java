package online.robotaxi.model;

import com.google.common.base.MoreObjects;
import online.robotaxi.taxi.model.OrderTask;
import org.springframework.data.annotation.PersistenceConstructor;

/**
 * time offset in order zone
 */
public class TaxiOrderRemoteTaskLog implements EmbeddedDocument {
    private OrderTask.Task task;
    private long timestamp;
    private Type type;
    private String comment;

    @PersistenceConstructor
    private TaxiOrderRemoteTaskLog() {
    }

    public TaxiOrderRemoteTaskLog(
            OrderTask.Task task,
            long timestamp,
            Type type
    ) {
        this.task = task;
        this.timestamp = timestamp;
        this.type = type;
    }

    public TaxiOrderRemoteTaskLog(
            OrderTask.Task task,
            long timestamp,
            Type type,
            String comment
    ) {
        this.task = task;
        this.timestamp = timestamp;
        this.type = type;
        this.comment = comment;
    }

    public OrderTask.Task getTask() {
        return task;
    }

    public long getTimestamp() {
        return timestamp;
    }

    public Type getType() {
        return type;
    }

    public String getComment() {
        return comment;
    }

    @Override
    public String toString() {
        return MoreObjects.toStringHelper(this)
                .add("task", task)
                .add("timestamp", timestamp)
                .add("type", type)
                .add("comment", comment)
                .toString();
    }

    public enum Type {
        START,
        FINISH_SUCCESS,
        FINISH_FAILURE
    }
}
