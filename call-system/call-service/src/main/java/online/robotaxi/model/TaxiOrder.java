package online.robotaxi.model;

import com.google.common.base.MoreObjects;
import online.robotaxi.taxi.model.OrderTask;
import online.robotaxi.time.TimeService;
import org.apache.commons.lang3.ObjectUtils;
import org.springframework.data.annotation.PersistenceConstructor;

import java.time.Instant;
import java.time.LocalDateTime;
import java.time.ZoneId;
import java.time.ZonedDateTime;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.Map;

/**
 * timestamps in seconds since epoch
 */
@org.springframework.data.mongodb.core.mapping.Document
public class TaxiOrder extends Document {
    private long createdTimestamp;
    private long arrivalTimestamp;
    private String zoneOffset;

    private String phone;
    private Address arrivalAddress;
    private String comments;
    private Map<String, Integer> selectedOptions;

    private String callSessionUuid;
    private String remoteId;
    private String remoteUuid;

    private State state;

    private String placingFailedReason;

    private List<TaxiOrderRemoteTaskLog> remoteTaskLogs = new ArrayList<>();

    @PersistenceConstructor
    private TaxiOrder() {
    }

    public TaxiOrder(
            String callSessionUuid,
            ZonedDateTime created,
            String phone,
            ZonedDateTime arrivalTime,
            Address arrivalAddress,
            String comments,
            Map<String, Integer> selectedOptions
    ) {
        this.callSessionUuid = callSessionUuid;

        this.createdTimestamp = created.toEpochSecond();
        this.phone = phone;
        this.arrivalTimestamp = arrivalTime.toEpochSecond();
        this.zoneOffset = arrivalTime.getOffset().getId();
        this.arrivalAddress = arrivalAddress;
        this.comments = comments;
        this.selectedOptions = selectedOptions;

        this.state = State.NEW;
    }

    public String getCallSessionUuid() {
        return callSessionUuid;
    }

    public LocalDateTime getCreatedTime() {
        return TimeService.fromSecondsFromEpoch(createdTimestamp);
    }

    public String getPhone() {
        return phone;
    }

    public Address getArrivalAddress() {
        return arrivalAddress;
    }

    public String getComments() {
        return comments;
    }

    public long getArrivalTimestamp() {
        return arrivalTimestamp;
    }

    public String getZoneOffset() {
        return zoneOffset;
    }

    public Map<String, Integer> getSelectedOptions() {
        //noinspection unchecked
        return ObjectUtils.firstNonNull(selectedOptions, Collections.emptyMap());
    }

    public long getCreatedTimestamp() {
        return createdTimestamp;
    }

    public String getRemoteId() {
        return remoteId;
    }

    public State getState() {
        return state;
    }

    public void setState(State state) {
        this.state = state;
    }

    public String getPlacingFailedReason() {
        return placingFailedReason;
    }

    public ZonedDateTime zonedArrivalDateTime() {
        return Instant.ofEpochSecond(arrivalTimestamp).atZone(ZoneId.of(zoneOffset));
    }

    public String getRemoteUuid() {
        return remoteUuid;
    }

    public void setRemoteUuid(String remoteUuid) {
        this.remoteUuid = remoteUuid;
    }

    public List<TaxiOrderRemoteTaskLog> getRemoteTaskLogs() {
        return remoteTaskLogs;
    }

    public TaxiOrder placed(String remoteOrderId) {
        this.remoteId = remoteOrderId;
        this.state = State.PLACED;
        return this;
    }

    public TaxiOrder placingFailed(String reason) {
        this.placingFailedReason = reason;
        this.state = State.PLACING_FAILED;
        return this;
    }

    @Override
    public String toString() {
        return MoreObjects.toStringHelper(this)
                .add("phone", phone)
                .add("arrivalTime", zonedArrivalDateTime())
                .add("arrivalAddress", arrivalAddress)
                .add("comments", comments)
                .toString();
    }

    public TaxiOrder remoteTaskStarted(OrderTask.Task task) {
        this.remoteTaskLogs.add(
                new TaxiOrderRemoteTaskLog(
                        task,
                        orderZonedDateTimeNow().toEpochSecond(),
                        TaxiOrderRemoteTaskLog.Type.START
                )
        );
        return this;
    }

    public TaxiOrder remoteTaskPerformedSuccessfully(OrderTask.Task task) {
        this.remoteTaskLogs.add(
                new TaxiOrderRemoteTaskLog(
                        task,
                        orderZonedDateTimeNow().toEpochSecond(),
                        TaxiOrderRemoteTaskLog.Type.FINISH_SUCCESS
                )
        );
        return this;
    }

    public TaxiOrder remoteTaskFailed(OrderTask.Task task, String comment) {
        this.remoteTaskLogs.add(
                new TaxiOrderRemoteTaskLog(
                        task,
                        orderZonedDateTimeNow().toEpochSecond(),
                        TaxiOrderRemoteTaskLog.Type.FINISH_FAILURE,
                        comment
                )
        );
        return this;
    }

    private ZonedDateTime orderZonedDateTimeNow() {
        return ZonedDateTime.now(ZoneId.of(this.zoneOffset));
    }

    public enum State {
        NEW,
        PLACED, // placed, but state not yet refreshed from remote
        PLACING_FAILED,

        // remote states
        CREATED, // создан
        ARRIVING, // водитель выехал на заказ
        DRIVING, // в пути
        CANCELLED
    }

}
