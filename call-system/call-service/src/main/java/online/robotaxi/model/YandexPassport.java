package online.robotaxi.model;

import org.springframework.data.annotation.PersistenceConstructor;
import org.springframework.data.mongodb.core.mapping.Document;

@Document
public class YandexPassport extends NamedDocument {
    private String login;
    private String pass;

    @PersistenceConstructor
    private YandexPassport() {
    }

    public YandexPassport(String name, String login, String pass) {
        super(name);
        this.login = login;
        this.pass = pass;
    }

    public String getLogin() {
        return login;
    }

    public String getPass() {
        return pass;
    }
}
