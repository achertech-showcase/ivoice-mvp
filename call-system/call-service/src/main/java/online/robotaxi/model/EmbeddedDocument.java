package online.robotaxi.model;

/**
 * marker interface, meaning that persistence object resides inside other object,
 * and has no personal collection
 */
public interface EmbeddedDocument {
}
