package online.robotaxi.model;

import com.google.common.base.MoreObjects;
import org.springframework.data.annotation.PersistenceConstructor;
import org.springframework.data.mongodb.core.mapping.Document;

@Document
public class Campaign extends NamedDocument {
    private String client;
    private String script; // corresponds to agi script name
    private State state;
    private CampaignSchedule schedule;
    private int callTimeoutSec;

    @PersistenceConstructor
    private Campaign() {
        super();
    }

    public Campaign(String name, String script, String client, int callTimeoutSec, CampaignSchedule schedule) {
        super(name);
        this.client = client;
        this.script = script;
        this.state = State.PREPARING;
        this.callTimeoutSec = callTimeoutSec;
        this.schedule = schedule;
    }

    public String getClient() {
        return client;
    }

    public State getState() {
        return state;
    }

    public String getScript() {
        return script;
    }

    public CampaignSchedule getSchedule() {
        return schedule;
    }

    public Campaign start() {
        this.state = State.STARTED;
        return this;
    }

    public Campaign stop() {
        this.state = State.PREPARING;
        return this;
    }

    public Campaign finish() {
        this.state = State.FINISHED;
        return this;
    }

    public int getCallTimeoutSec() {
        return callTimeoutSec;
    }

    @Override
    public String toString() {
        return MoreObjects.toStringHelper(this)
                .add("client", client)
                .add("script", script)
                .add("state", state)
                .add("schedule", schedule)
                .toString();
    }

    public enum State {
        PREPARING,
        STARTED,
        FINISHED
    }
}
