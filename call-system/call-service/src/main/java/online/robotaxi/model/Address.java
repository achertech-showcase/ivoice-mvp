package online.robotaxi.model;

import com.google.common.base.MoreObjects;
import org.springframework.data.annotation.PersistenceConstructor;

public class Address implements EmbeddedDocument {
    private String fullAddress;
    private String city;
    private String street;
    private String house;

    @PersistenceConstructor
    public Address() {
    }

    public Address(String fullAddress, String city, String street, String house) {
        this.fullAddress = fullAddress;
        this.city = city;
        this.street = street;
        this.house = house;
    }

    public String getFullAddress() {
        return fullAddress;
    }

    public String getCity() {
        return city;
    }

    public String getStreet() {
        return street;
    }

    public String getHouse() {
        return house;
    }

    @Override
    public String toString() {
        return MoreObjects.toStringHelper(this)
                .add("fullAddress", fullAddress)
                .add("city", city)
                .add("street", street)
                .add("house", house)
                .toString();
    }
}
