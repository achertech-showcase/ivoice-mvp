package online.robotaxi.model;

import java.util.List;

public class CallResult extends Document {
    /**
     * all recognized responses in order, consistent with call task scenario
     * file records could be located by CallResilt.id in records db
     */
    private List<String> recognizedResponses;
}
