package online.robotaxi.model;

import com.google.common.base.MoreObjects;
import online.robotaxi.coldcall.repeat.RepeatOptions;
import org.springframework.data.annotation.PersistenceConstructor;

import java.util.List;

public class CampaignSchedule implements EmbeddedDocument {
    /**
     * see ZoneOffset.getId
     * format: "+03" for moscow
     */
    private String zoneOffsetId;

    /**
     * Intervals local to campaign timeZone when call can be made.
     * Timezone for each interval can be overrided in CallTask.
     * see time-handling.md
     */
    private List<Interval> intervals;
    private RepeatOptions repeatOptions;

    @PersistenceConstructor
    private CampaignSchedule() {
    }

    public CampaignSchedule(
            String zoneOffsetId,
            List<Interval> intervals,
            RepeatOptions repeatOptions
    ) {
        this.zoneOffsetId = zoneOffsetId;
        this.intervals = intervals;
        this.repeatOptions = repeatOptions;
    }

    public String getZoneOffsetId() {
        return zoneOffsetId;
    }

    public RepeatOptions getRepeatOptions() {
        return repeatOptions;
    }

    @Override
    public String toString() {
        return MoreObjects.toStringHelper(this)
                .add("zoneOffsetId", zoneOffsetId)
                .add("intervals", intervals)
                .add("repeatOptions", repeatOptions)
                .toString();
    }
}
