package online.robotaxi.model;

import org.springframework.data.annotation.PersistenceConstructor;
import org.springframework.data.mongodb.core.index.Indexed;

/**
 * all db relations defined through foreign document name
 */
public abstract class NamedDocument extends Document {
    @Indexed(unique = true)
    private String name;

    @PersistenceConstructor
    NamedDocument() {
    }

    NamedDocument(String name) {
        this.name = name;
    }

    public String getName() {
        return name;
    }
}
