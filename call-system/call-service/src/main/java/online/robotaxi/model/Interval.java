package online.robotaxi.model;

import com.google.common.base.MoreObjects;
import org.springframework.data.annotation.PersistenceConstructor;

import java.time.LocalTime;

import static java.lang.String.format;

/**
 * interval in the day in local time zone
 */
public class Interval implements EmbeddedDocument {
    private static final int LAST_DAY_SECOND = 24 * 60 * 60 - 1;
    /**
     * format in seconds from day start:
     * 0 - LAST_DAY_SECOND
     */
    private int start;
    private int end;

    @PersistenceConstructor
    public Interval() {
    }

    public Interval(int startHour, int endHour) {
        checkHourValid(startHour);
        checkHourValid(endHour);
        this.start = hourToSec(startHour);
        this.end = hourToSec(endHour);
    }

    private void checkHourValid(int hour) {
        if (hour < 0 || hour > 24) {
            throw new IllegalArgumentException(format("invalid hour %s", hour));
        }
    }

    private static int hourToSec(int hour) {
        int result = hour * 60 * 60;
        if (hour == 24) {
            return result - 1;
        }
        return result;
    }

    public LocalTime getStartTime() {
        return toTime(start);
    }

    public LocalTime getEndTime() {
        return toTime(end);
    }

    private static LocalTime toTime(long secondsOfDay) {
        return LocalTime.ofSecondOfDay(secondsOfDay);
    }

    @Override
    public String toString() {
        return MoreObjects.toStringHelper(this)
                .add("start", getStartTime())
                .add("end", getEndTime())
                .toString();
    }
}
