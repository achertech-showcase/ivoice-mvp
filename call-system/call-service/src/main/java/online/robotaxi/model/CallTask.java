package online.robotaxi.model;

import com.google.common.base.MoreObjects;
import online.robotaxi.coldcall.repeat.RepeatOptions;
import org.bson.types.ObjectId;
import org.springframework.data.annotation.PersistenceConstructor;

import java.util.HashMap;
import java.util.Map;

import static java.lang.String.format;

/**
 * All times (long) created with respect to target timezone
 */
@org.springframework.data.mongodb.core.mapping.Document
public class CallTask extends Document {
    private String originalDbId; // from phones list, from witch task was created
    private String targetPhone;
    private String campaign;
    private State state;

    /**
     * time when task ready to be performed (maybe in the past)
     */
    private long readySince; // seconds since epoch without offset
    private String timeOffsetId = "Z";

    private long performed;
    private long lineStarted;
    private long durationOnLineSeconds;
    private int attemptsPerformed;
    // repeats by reason. performed, or planned
    private Map<RepeatOptions.RepeatReason, Integer> repeats;
    private String sourceId; // foreignKey to id of this task in source, from which tasks was created
    private Map<Long, String> log = new HashMap<>();
    private String cdr;
    private String recognitions;
    private String result;
    private float resultConfidence;
    private String postprocessDescriptor;
    private String postprocessResult;

    @PersistenceConstructor
    private CallTask() {
    }

    public CallTask(String campaign, String targetPhone) {
        this.targetPhone = targetPhone;
        this.campaign = campaign;
        this.state = State.NOT_PERFORMED;

        attemptsPerformed = 0;
        repeats = new HashMap<>();
    }

    public String getOriginalDbId() {
        return originalDbId;
    }

    public void setOriginalDbId(String originalDbId) {
        this.originalDbId = originalDbId;
    }

    public String getTargetPhone() {
        return targetPhone;
    }

    public String getCampaign() {
        return campaign;
    }

    public State getState() {
        return state;
    }

    public long getPerformed() {
        return performed;
    }

    public int getAttemptsPerformed() {
        return attemptsPerformed;
    }

    public Map<RepeatOptions.RepeatReason, Integer> getRepeats() {
        return repeats;
    }

    public long getReadySince() {
        return readySince;
    }

    public void setCdr(String cdr) {
        this.cdr = cdr;
    }

    public String getRecognitions() {
        return recognitions;
    }

    public void setRecognitions(String recognitions) {
        this.recognitions = recognitions;
    }

    public float getResultConfidence() {
        return resultConfidence;
    }

    public void setResultConfidence(float resultConfidence) {
        this.resultConfidence = resultConfidence;
    }

    public String getPostprocessDescriptor() {
        return postprocessDescriptor;
    }

    public String getPostprocessResult() {
        return postprocessResult;
    }

    public void setPostprocessResult(String postprocessResult) {
        this.postprocessResult = postprocessResult;
    }

    public CallTask lineStarted(long time, String comment) {
        this.lineStarted = time;
        log(time, comment);
        return this;
    }

    public CallTask performed(long time, String result) {
        this.state = State.PERFORMED;
        this.performed = time;
        this.durationOnLineSeconds = performed - lineStarted;
        this.result = result;
        log(time, result);
        return this;
    }

    public CallTask failed(long time, String result) {
        this.state = State.FAILED;
        this.result = result;
        return log(time, result);
    }

    @Override
    public String toString() {
        return MoreObjects.toStringHelper(this)
                .add("objectId", findObjectId())
                .add("targetPhone", targetPhone)
                .add("campaign", campaign)
                .add("state", state)
                .add("performed", performed)
                .add("recognitions", recognitions)
//                .add("cdr", cdr)
                .add("readySince", getReadySince())
                .add("attemptsPerformed", attemptsPerformed)
                .toString();
    }

    public CallTask log(long secondsSinceEpoch, String message) {
        log.put(secondsSinceEpoch, message);
        return this;
    }

    public CallTask postpone(long nextAttempt, long now, String logComment) {
        state = State.NOT_PERFORMED;
        readySince = nextAttempt;
        return log(now, logComment);
    }

    public CallTask start(long secondsSinceEpoch, String message) {
        log(secondsSinceEpoch, message);
        attemptsPerformed++;
        this.state = State.ONGOING;
        return this;
    }

    public CallTask postprocess(long secondsSinceEpoch, String postprocessDescriptor) {
        log(secondsSinceEpoch, format("setup for postprocessing: %s", postprocessDescriptor));
        this.postprocessDescriptor = postprocessDescriptor;
        this.state = State.TO_POSTPROCESS;
        return this;
    }

    public CallTask stop() {
        this.state = State.NOT_PERFORMED;
        return this;
    }

    public Map<Long, String> getLog() {
        return log;
    }

    public String getResult() {
        return result;
    }

    public void setResult(String result) {
        this.result = result;
    }

    public String getCdr() {
        return cdr;
    }

    public void setState(State state) {
        this.state = state;
    }

    public ObjectId findObjectId() {
        return new ObjectId(getId().toString(16));
    }

    public enum State {
        NOT_PERFORMED,
        ONGOING,
        PERFORMED,
        FAILED,
        TO_POSTPROCESS
    }
}
