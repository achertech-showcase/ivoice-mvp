package online.robotaxi.config;

import online.robotaxi.repository.CallTaskRepository;
import online.robotaxi.repository.CampaignRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Profile;

@Profile("demo")
//@Configuration
public class DemoDataConfig {
    private final CampaignRepository campaignRepository;
    private final CallTaskRepository callTaskRepository;

    @Autowired
    public DemoDataConfig(
            CampaignRepository campaignRepository,
            CallTaskRepository callTaskRepository
    ) {
        this.campaignRepository = campaignRepository;
        this.callTaskRepository = callTaskRepository;
    }

//    @PostConstruct
//    void initDemoData() {
//        callTaskRepository.deleteAllByCampaign("demo-campaign");
//        campaignRepository.deleteAllByName("demo-campaign");
//
//        Interval iv = new Interval(0, 24);
//        RepeatOptions repeatOptions = new RetryWithIntervalOptions(4, 10, ChronoUnit.SECONDS);
//        CampaignSchedule schedule = new CampaignSchedule(
//                "+03",
//                Collections.singletonList(iv),
//                repeatOptions
//        );
////
//        Campaign campaign = new Campaign(
//                "demo-campaign",
//                "demo-script",
//                "demo-client",
//                5,
//                schedule
//        );
//        campaignRepository.save(campaign);
//
//        for (int i = 0; i < 3; i++) {
//            CallTask task = new CallTask(campaign.getName(), format("demo-silent-%s", i));
//            callTaskRepository.save(task);
//        }
//
//        campaignRepository.save(campaign.start());
//    }
}
