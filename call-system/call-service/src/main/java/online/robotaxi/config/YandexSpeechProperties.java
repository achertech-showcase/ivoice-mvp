package online.robotaxi.config;

import org.hibernate.validator.constraints.NotEmpty;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.stereotype.Component;
import org.springframework.validation.annotation.Validated;

@Validated
@Component
@ConfigurationProperties(prefix = "yandex.speechcloud")
public class YandexSpeechProperties {
    @NotEmpty
    private String key;
    @NotEmpty
    private String clientUuid;
    @NotEmpty
    private String synthesisMethod;
    @NotEmpty
    private String synthesisQuality;
    @NotEmpty
    private String speaker;
    @NotEmpty
    private String format;

    public String getKey() {
        return key;
    }

    public void setKey(String key) {
        this.key = key;
    }

    public String getClientUuid() {
        return clientUuid;
    }

    public void setClientUuid(String clientUuid) {
        this.clientUuid = clientUuid;
    }

    public String getSynthesisMethod() {
        return synthesisMethod;
    }

    public void setSynthesisMethod(String synthesisMethod) {
        this.synthesisMethod = synthesisMethod;
    }

    public String getSynthesisQuality() {
        return synthesisQuality;
    }

    public void setSynthesisQuality(String synthesisQuality) {
        this.synthesisQuality = synthesisQuality;
    }

    public String getSpeaker() {
        return speaker;
    }

    public void setSpeaker(String speaker) {
        this.speaker = speaker;
    }

    public String getFormat() {
        return format;
    }

    public void setFormat(String format) {
        this.format = format;
    }
}
