package online.robotaxi.config;

import online.robotaxi.model.YandexPassport;
import online.robotaxi.repository.YandexPassportRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Configuration;
import org.springframework.util.StringUtils;

import javax.annotation.PostConstruct;

@Configuration
public class YandexPasswordConfig {
    @Autowired
    private YandexPassportRepository passportRepository;
    @Autowired
    private YandexSecretProperties secretProperties;

    @PostConstruct
    public void initPassport() {
        if (!passportRepository.findAll().isEmpty()) {
            return;
        }

        String name = "passport";
        String login = "login";
        String password = "password";

        if (!StringUtils.isEmpty(secretProperties.getPassportName())) {
            name = secretProperties.getPassportName();
            login = secretProperties.getLogin();
            password = secretProperties.getPassword();
        }

        passportRepository.save(new YandexPassport(name, login, password));
    }
}
