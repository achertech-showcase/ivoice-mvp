package online.robotaxi.config;

import org.hibernate.validator.constraints.NotEmpty;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.stereotype.Component;
import org.springframework.validation.annotation.Validated;

import javax.validation.constraints.Min;

@Validated
@Component
@ConfigurationProperties(prefix = "mongo")
public class MongoProperties {
    @Min(1)
    private int port;
    @NotEmpty
    private String host;
    @NotEmpty
    private String database;

    int getPort() {
        return port;
    }

    public void setPort(int port) {
        this.port = port;
    }

    String getHost() {
        return host;
    }

    public void setHost(String host) {
        this.host = host;
    }

    String getDatabase() {
        return database;
    }

    public void setDatabase(String database) {
        this.database = database;
    }
}
