package online.robotaxi.config;

import org.hibernate.validator.constraints.NotEmpty;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.stereotype.Component;
import org.springframework.validation.annotation.Validated;

import javax.validation.constraints.Min;

@Validated
@Component
@ConfigurationProperties(prefix = "freeswitch")
public class FreeswitchProperties implements RecordsProperty {
    @NotEmpty
    private String host = "freeswitch-test";
    @Min(1)
    private int port = 8021;
    @NotEmpty
    private String password = "ClueCon";
    @NotEmpty
    private String originateMode;
    @NotEmpty
    private String recordsParent;

    private String calluser;

    public String getHost() {
        return host;
    }

    public void setHost(String host) {
        this.host = host;
    }

    public int getPort() {
        return port;
    }

    public void setPort(int port) {
        this.port = port;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public String getOriginateMode() {
        return originateMode;
    }

    public void setOriginateMode(String originateMode) {
        this.originateMode = originateMode;
    }

    public String getCalluser() {
        return calluser;
    }

    public void setCalluser(String calluser) {
        this.calluser = calluser;
    }

    @Override
    public String getRecordsParent() {
        return recordsParent;
    }

    public void setRecordsParent(String recordsParent) {
        this.recordsParent = recordsParent;
    }
}
