package online.robotaxi.config;

import org.openqa.selenium.Capabilities;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.remote.DesiredCapabilities;
import org.openqa.selenium.remote.RemoteWebDriver;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

import java.net.MalformedURLException;
import java.net.URL;

@Configuration
public class WebdriverConfig {
    @Bean("placingOrder")
    WebDriver orderPlacing(WebdriverProperties properties, Capabilities capabilities) throws MalformedURLException {
        return new RemoteWebDriver(new URL(properties.getDriver1()), DesiredCapabilities.chrome());
    }

    @Bean("workingOrder")
    WebDriver orderInfo(WebdriverProperties properties, Capabilities capabilities) throws MalformedURLException {
        return new RemoteWebDriver(new URL(properties.getDriver2()), DesiredCapabilities.chrome());
    }

    @Bean
    Capabilities capabilities(WebdriverProperties properties) {
        DesiredCapabilities capabilities = DesiredCapabilities.chrome();
        capabilities.setJavascriptEnabled(true);
        capabilities.setCapability("takesScreenshot", properties.isScreenshots());
        return capabilities;
    }
}
