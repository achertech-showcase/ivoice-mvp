package online.robotaxi.config;

import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.stereotype.Component;
import org.springframework.validation.annotation.Validated;

import javax.validation.constraints.Min;

@Validated
@Component
@ConfigurationProperties(prefix = "caller")
public class CallerProperties {
    @Min(1)
    private int lines;

    public int getLines() {
        return lines;
    }

    public void setLines(int lines) {
        this.lines = lines;
    }
}
