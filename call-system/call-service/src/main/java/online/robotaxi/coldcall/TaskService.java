package online.robotaxi.coldcall;

import online.robotaxi.model.CallTask;

public interface TaskService {
    CallTask create(String campaign, String phone);
}
