package online.robotaxi.coldcall.repeat;

public interface RepeatOptions {
    enum RepeatReason {
        TIMEOUT,
        BUSY
    }
}
