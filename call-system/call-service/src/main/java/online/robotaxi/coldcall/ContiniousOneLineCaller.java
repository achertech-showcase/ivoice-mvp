package online.robotaxi.coldcall;

import online.robotaxi.coldcall.repeat.RepeatOptions;
import online.robotaxi.config.CallerProperties;
import online.robotaxi.freeswitch.InboundClient;
import online.robotaxi.model.CallTask;
import online.robotaxi.model.Campaign;
import online.robotaxi.repository.CallTaskRepository;
import online.robotaxi.repository.CampaignRepository;
import online.robotaxi.time.TimeService;
import org.freeswitch.esl.client.IEslEventListener;
import org.freeswitch.esl.client.transport.event.EslEvent;
import org.freeswitch.esl.client.transport.message.EslMessage;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Component;
import org.w3c.dom.Document;
import org.w3c.dom.NodeList;
import org.xml.sax.InputSource;
import org.xml.sax.SAXException;

import javax.annotation.PostConstruct;
import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;
import java.io.ByteArrayInputStream;
import java.io.IOException;
import java.io.UnsupportedEncodingException;
import java.math.BigInteger;
import java.net.URLDecoder;
import java.time.LocalDateTime;
import java.util.*;
import java.util.concurrent.atomic.AtomicInteger;

import static java.lang.String.format;

/**
 * continuously processes call tasks in campaign one by one from latest not performed;
 * when no more tasks, it waits for NO_TASK_REPEAT_DELAY_MS
 * <p>
 * contract:
 * 1. postponed tasks immediatelly saved
 */
@Component
public class ContiniousOneLineCaller implements Caller {
    private static final Logger LOG = LoggerFactory.getLogger(ContiniousOneLineCaller.class);
    private static final int NO_TASK_REPEAT_DELAY_MS = 5000;

    private final CallTaskRepository callTaskRepository;
    private final CampaignRepository campaignRepository;
    private final Map<String, RepeatStrategy> repeatStrategyMap = new HashMap<>();
    private final InboundClient client;

    private AtomicInteger lines = new AtomicInteger(1);

    @Autowired
    public ContiniousOneLineCaller(
            CampaignRepository campaignRepository,
            CallTaskRepository callTaskRepository,
            List<RepeatStrategy> repeatStrategies,
            InboundClient client,
            CallerProperties callerProperties
    ) {
        this.callTaskRepository = callTaskRepository;
        this.campaignRepository = campaignRepository;
        this.client = client;

        this.lines.set(callerProperties.getLines());

        for (RepeatStrategy rs : repeatStrategies) {
            repeatStrategyMap.put(rs.getProcessingOptions().getSimpleName(), rs);
        }
    }

    public int updateLines(int lines) {
        if (lines < 1) {
            throw new IllegalArgumentException(format("at least 1 line expected, but trying to update to %d", lines));
        }
        if (lines > 50 * 4) {
            throw new IllegalArgumentException(format("expected 200 lines max, but trying to set %d", lines));
        }
        this.lines.set(lines);
        LOG.info(format("lines updated: %s", this.lines));
        return this.lines.get();
    }

    @Scheduled(fixedDelay = NO_TASK_REPEAT_DELAY_MS)
    public void processCampaigns() {
        // if any ongoing task exist, do nothing, because of linear implementation (see callTask for event search)
//        Optional<CallTask> maybeOngoingTask = callTaskRepository.findFirstByState(CallTask.State.ONGOING);
//        if (maybeOngoingTask.isPresent()) {
//            return;
//        }

        List<CallTask> ongoingTasks = callTaskRepository.findAllByState(CallTask.State.ONGOING);
        if (ongoingTasks.size() >= lines.get()) {
            return;
        }

        int callToRun = lines.get() - ongoingTasks.size();

        campaignRepository.findAllByState(Campaign.State.STARTED).stream()
                .filter(this::isCallingTime)
                .forEach(c -> {
                    if (allTasksPerformed(c)) {
                        campaignRepository.save(c.finish());
                        LOG.info(format("campaign %s finished", c.getName()));
                        return;
                    }

                    for (int i = 0; i < callToRun; i++) {
                        Optional<CallTask> nextTask = findNextTask(c);
                        nextTask.ifPresent(callTask -> process(c, callTask));
                    }
                });
    }

    @SuppressWarnings("unused")
    @PostConstruct
    public void listenEsl() {
        client.subscribe(new IEslEventListener() {
            @Override
            public void eventReceived(EslEvent event) {
                String eventName = event.getEventName().toUpperCase();
                switch (eventName) {
                    case "CHANNEL_HANGUP_COMPLETE":
                        List<String> cdr = event.getEventBodyLines();
                        if (cdr.isEmpty()) {
                            // ignore, there will be second event, closing ongoing call
                            return;
                        }

                        String cdrStr = String.join("", cdr);
                        Document document = null;
                        try {
                            document = toXml(cdrStr);
                        } catch (ParserConfigurationException | IOException | SAXException e) {
                            LOG.error(format(
                                    "error parsing cdr to xml: %s; %s; need to fix task manually",
                                    cdrStr,
                                    e.getMessage()
                            ));
                            return;
                        }
                        String hangupCause = document.getElementsByTagName("hangup_cause").item(0).getTextContent();
                        String calltaskid = document.getElementsByTagName("calltaskid").item(0).getTextContent();
                        LOG.info(format("hangup %s for task %s", hangupCause, calltaskid));

                        CallTask callTask = callTaskRepository.findOne(new BigInteger(calltaskid));
                        try {
                            callTask.setCdr(URLDecoder.decode(cdrStr, "UTF-8"));
                        } catch (UnsupportedEncodingException e) {
                            LOG.error(format(
                                    "error trying decode cdr %s; saving not decoded; %s",
                                    cdr,
                                    e.getMessage()
                            ));
                            callTask.setCdr(cdrStr);
                        }

                        if (hangupCause.equalsIgnoreCase("CALL_REJECTED")) {
                            callTask.log(TimeService.localDtNowSinceEpochSeconds(), "сброс во время вызова");
                            callTaskRepository.save(processPostponing(callTask, RepeatOptions.RepeatReason.BUSY));
                            return;
                        } else if (hangupCause.equalsIgnoreCase("USER_BUSY")) {
                            callTask.log(TimeService.localDtNowSinceEpochSeconds(), "абонент занят");
                            callTaskRepository.save(processPostponing(callTask, RepeatOptions.RepeatReason.BUSY));
                            return;
                        } else if (hangupCause.equalsIgnoreCase("UNALLOCATED_NUMBER")) {
                            callTaskRepository.save(callTask.failed(
                                    TimeService.localDtNowSinceEpochSeconds(),
                                    "номер отключен"
                            ));
                            return;
                        } else if (hangupCause.equalsIgnoreCase("NO_ANSWER") || hangupCause.equalsIgnoreCase(
                                "NO_USER_RESPONSE")) {
                            callTask.log(TimeService.localDtNowSinceEpochSeconds(), "таймаут соединения");
                            callTaskRepository.save(processPostponing(callTask, RepeatOptions.RepeatReason.TIMEOUT));
                            return;
                        } else if (hangupCause.equalsIgnoreCase("NETWORK_OUT_OF_ORDER")) {
                            callTask.log(TimeService.localDtNowSinceEpochSeconds(), "ошибка SIP: NETWORK_OUT_OF_ORDER");
                            callTaskRepository.save(processPostponing(callTask, RepeatOptions.RepeatReason.TIMEOUT));
                            return;
                        } else if (hangupCause.equalsIgnoreCase("NORMAL_CLEARING")) {
                            // for remote, normal clearing is both timeout and drop when dialog
                            boolean isDialogStarted = findIfDialogStarted(document);
                            if (isDialogStarted) {
                                Optional<String> maybeResult = findResult(calltaskid, document);
                                StringBuilder result = new StringBuilder();
                                maybeResult.ifPresent(s -> result.append(s).append("; "));

                                byte answersAmount = findAnswersAmount(calltaskid, document);
                                if (answersAmount > 0) {
                                    processResult(callTask, document);
                                } else {
                                    result.append("сброс во время разговора");
                                    callTaskRepository.save(callTask.failed(
                                            TimeService.localDtNowSinceEpochSeconds(),
                                            result.toString()
                                    ));
                                }

                                return;
                            } else {
                                callTask.log(TimeService.localDtNowSinceEpochSeconds(), "таймаут соединения");
                                callTaskRepository.save(processPostponing(
                                        callTask,
                                        RepeatOptions.RepeatReason.TIMEOUT
                                ));
                                return;
                            }
                        }

                        processResult(callTask, document);

                        break;
                    default:
                        // do nothing
                        break;
                }
            }

            @Override
            public void backgroundJobResultReceived(EslEvent event) {

            }
        });
    }

    private void processResult(CallTask callTask, Document document) {
        Optional<String> maybeResult = findResult(callTask.getId().toString(), document);
        if (maybeResult.isPresent()) {
            Optional<String> recognitions = findRecognitions(document);
            if (recognitions.isPresent()) {
                try {
                    callTask.setRecognitions(URLDecoder.decode(recognitions.get(), "UTF-8"));
                } catch (UnsupportedEncodingException e) {
                    LOG.error(format("error decoding recignitions: %s; %s", recognitions.get(), e.getMessage()));
                }
            } else {
                LOG.warn(format(
                        "recognitions for result not found, but expected, check cdr: %s",
                        callTask
                ));
            }
            Optional<Float> maybeConfidence = findConfidence(document);
            maybeConfidence.ifPresent(callTask::setResultConfidence);

            Optional<String> maybePostprocessing = findPostprocessing(document);
            if (maybePostprocessing.isPresent()) {
                callTask.setResult(maybeResult.get());
                callTaskRepository.save(callTask.postprocess(
                        TimeService.localDtNowSinceEpochSeconds(),
                        maybePostprocessing.get()
                ));
                LOG.info(format("task %s set for postprocessing", callTask.findObjectId()));
            } else {
                callTaskRepository.save(callTask.performed(
                        TimeService.localDtNowSinceEpochSeconds(),
                        maybeResult.get()
                ));
                LOG.info(format("result for %s saved", callTask.getId().toString()));
            }
        } else {
            callTaskRepository.save(
                    callTask.failed(
                            TimeService.localDtNowSinceEpochSeconds(),
                            "результат неопределен, проверьте лог"
                    ));
        }
    }

    private Optional<String> findPostprocessing(Document document) {
        NodeList resultNode = document.getElementsByTagName("postprocess");
        if (resultNode.getLength() == 1) {
            String resultVar = resultNode.item(0).getTextContent();
            try {
                return Optional.of(URLDecoder.decode(resultVar, "UTF-8"));
            } catch (UnsupportedEncodingException e) {
                LOG.error(format("postprocess descriptor decoding error: %s", e.getMessage()));
                return Optional.empty();
            }
        } else {
            return Optional.empty();
        }
    }

    private boolean findIfDialogStarted(Document document) {
        NodeList resultNode = document.getElementsByTagName("answer_stamp");
        return resultNode.getLength() == 1;
    }

    private byte findAnswersAmount(String calltaskid, Document document) {
        NodeList resultNode = document.getElementsByTagName("answersAmount");
        if (resultNode.getLength() == 1) {
            String resultVar = resultNode.item(0).getTextContent();
            return Byte.valueOf(resultVar);
        } else {
            return 0;
        }
    }

    private Optional<String> findRecognitions(Document document) {
        NodeList resultNode = document.getElementsByTagName("recognitions");
        if (resultNode.getLength() == 1) {
            String resultVar = resultNode.item(0).getTextContent();
            return Optional.of(resultVar);
        } else {
            return Optional.empty();
        }
    }

    private Optional<Float> findConfidence(Document document) {
        NodeList resultNode = document.getElementsByTagName("confidence");
        if (resultNode.getLength() == 1) {
            String resultVar = resultNode.item(0).getTextContent();
            return Optional.of(Float.valueOf(resultVar));
        } else {
            return Optional.empty();
        }
    }

    private Optional<String> findResult(String calltaskid, Document document) {
        NodeList resultNode = document.getElementsByTagName("results");
        if (resultNode.getLength() == 1) {
            String resultVar = resultNode.item(0).getTextContent();
            String decodedResult;
            try {
                decodedResult = URLDecoder.decode(resultVar, "UTF-8");
                return Optional.of(decodedResult);
            } catch (UnsupportedEncodingException e) {
                throw new IllegalStateException(format(
                        "can't decode result %s for task %s",
                        resultVar,
                        calltaskid
                ));
            }
        } else {
            return Optional.empty();
        }
    }

    private Document toXml(String cdr) throws ParserConfigurationException, IOException, SAXException {
        DocumentBuilderFactory dbf = DocumentBuilderFactory.newInstance();
        DocumentBuilder db = dbf.newDocumentBuilder();
        InputSource source = null;
        try {
            source = new InputSource(new ByteArrayInputStream(cdr.getBytes("utf-8")));
        } catch (UnsupportedEncodingException e) {
            throw new IllegalStateException(e);
        }
        return db.parse(source);
    }

    private Optional<CallTask> findNextTask(Campaign campaign) {
        Optional<CallTask> maybeTask = callTaskRepository.findFirstByCampaignAndStateOrderByReadySinceAsc(
                campaign.getName(),
                CallTask.State.NOT_PERFORMED
        );
        if (!maybeTask.isPresent()) {
            return Optional.empty();
        }

        CallTask task = maybeTask.get();
        if (isCallingTime(task)) {
            return Optional.of(task);
        } else {
            return Optional.empty();
        }
    }

    private boolean isCallingTime(Campaign campaign) {
//        CampaignSchedule schedule = campaign.getSchedule();
//        if (!isCallingTime(schedule)) {
//
//        }
        // TODO implement calling again
        return true;
    }

    boolean isCallingTime(CallTask task) {
        // TODO fix, using zone adjustment
        LocalDateTime now = LocalDateTime.now();
        LocalDateTime readySince = TimeService.fromSecondsFromEpoch(task.getReadySince());
        return now.isAfter(readySince);
    }

    private boolean allTasksPerformed(Campaign campaign) {
        List<CallTask> notPerformedTasks = callTaskRepository.findFirstByCampaignAndStateIn(
                campaign.getName(),
                Arrays.asList(CallTask.State.NOT_PERFORMED, CallTask.State.ONGOING)
        );
        return notPerformedTasks.isEmpty();
    }

    private CallTask process(Campaign campaign, CallTask callTask) {
        callTask = callTaskRepository.save(callTask.start(
                TimeService.localDtNowSinceEpochSeconds(),
                "инициирован звонок"
        ));
        try {
            EslMessage result = originateCall(
                    campaign,
                    callTask
            );// all future state updates performed by esl events
            if (result.getBodyLines().size() == 1 && result.getBodyLines().get(0).equalsIgnoreCase(
                    "-ERR SUBSCRIBER_ABSENT")) {
                return callTaskRepository.save(
                        callTask.failed(
                                TimeService.localDtNowSinceEpochSeconds(),
                                "абонент отсутствует"
                        )
                );
            } else if (result.getBodyLines().size() == 1 && result.getBodyLines().get(0).contains("ERR")) {
                return callTaskRepository.save(
                        callTask.failed(
                                TimeService.localDtNowSinceEpochSeconds(),
                                format("ошибка телефонии %s", result.getBodyLines().get(0))
                        )
                );
            } else {
                // initiated successfully, call will be updated by event
                if (LOG.isDebugEnabled()) {
                    LOG.debug("call originated: %s", result);
                }
                return callTask;
            }
        } catch (Exception e) {
            callTask.log(TimeService.localDtNowSinceEpochSeconds(), "ошибка связи");
            LOG.error(format("originate call failed with exception %s, return task to not performed", e.getMessage()));
            callTask.setState(CallTask.State.NOT_PERFORMED);
            callTaskRepository.save(callTask);
            LOG.warn(format("stopping campaign %s", campaign.getName()));
            this.campaignRepository.save(campaign.stop());
            throw new IllegalStateException(e);
        }
    }

    private EslMessage originateCall(Campaign campaign, CallTask callTask) throws IOException {
        LOG.info(format("originate call for %s", callTask));
        // TODO process originating errors
        return client.originateCall(
                campaign.getScript(),
                callTask.getId().toString(),
                callTask.getTargetPhone()
        );
    }

    private CallTask processPostponing(CallTask callTask, RepeatOptions.RepeatReason repeatReason) {
        Optional<Campaign> maybeCampaign = campaignRepository.findOneByName(callTask.getCampaign());
        if (!maybeCampaign.isPresent()) {
            throw new IllegalStateException(format(
                    "campaign %s not found for task %s",
                    callTask.getCampaign(),
                    callTask
            ));
        }
        return processPostponing(callTask, maybeCampaign.get().getSchedule().getRepeatOptions(), repeatReason);
    }

    private <T extends RepeatOptions> CallTask processPostponing(
            CallTask callTask,
            T repeatOptions,
            RepeatOptions.RepeatReason repeatReason
    ) {
        //noinspection unchecked
        RepeatStrategy<T> repeatStrategy = (RepeatStrategy<T>) repeatStrategyMap
                .get(repeatOptions.getClass().getSimpleName());
        if (Objects.isNull(repeatStrategy)) {
            LOG.error(format("repeat strategy not found: %s, %s; failing task", callTask, repeatOptions));
            callTask.failed(TimeService.localDtNowSinceEpochSeconds(), "отмена из-за внутренней ошибки системы");
        }
        return repeatStrategy.processPostponing(callTask, repeatOptions, repeatReason);
    }
}
