package online.robotaxi.coldcall.repeat;

import com.google.common.base.MoreObjects;
import org.springframework.data.annotation.PersistenceConstructor;

import java.time.Duration;
import java.time.temporal.TemporalUnit;

public final class RetryWithIntervalOptions implements RepeatOptions {
    private int timeoutAttempts;
    private long secondsBeforeNextTimeoutAttempt;

    private int busyAttempts;
    private long secondsBeforeNextBusyAttempt;

    @PersistenceConstructor
    private RetryWithIntervalOptions() {
    }

    public RetryWithIntervalOptions(
            int timeoutAttempts,
            long beforeNextTimeoutAttempt,
            int busyAttempts,
            long beforeNextBusyAttempt,
            TemporalUnit temporalUnit
    ) {
        this.timeoutAttempts = timeoutAttempts;
        this.secondsBeforeNextTimeoutAttempt = Duration.of(beforeNextTimeoutAttempt, temporalUnit).getSeconds();

        this.busyAttempts = busyAttempts;
        this.secondsBeforeNextBusyAttempt = Duration.of(beforeNextBusyAttempt, temporalUnit).getSeconds();
    }

    public int getTimeoutAttempts() {
        return timeoutAttempts;
    }

    public long getSecondsBeforeNextTimeoutAttempt() {
        return secondsBeforeNextTimeoutAttempt;
    }

    public int getBusyAttempts() {
        return busyAttempts;
    }

    public long getSecondsBeforeNextBusyAttempt() {
        return secondsBeforeNextBusyAttempt;
    }

    @Override
    public String toString() {
        return MoreObjects.toStringHelper(this)
                .add("timeoutAttempts", timeoutAttempts)
                .add("secondsBeforeNextTimeoutAttempt", secondsBeforeNextTimeoutAttempt)
                .add("busyAttempts", busyAttempts)
                .add("secondsBeforeNextBusyAttempt", secondsBeforeNextBusyAttempt)
                .toString();
    }
}
