package online.robotaxi.coldcall;

public interface ColdCallCampaignService {
    /**
     * @return started successfully
     */
    boolean start(String campaignName);

    boolean stop(String campaignName);
}
