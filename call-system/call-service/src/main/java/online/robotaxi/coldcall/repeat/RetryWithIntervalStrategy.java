package online.robotaxi.coldcall.repeat;

import online.robotaxi.coldcall.RepeatStrategy;
import online.robotaxi.model.CallTask;
import online.robotaxi.time.TimeService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Service;

import java.time.LocalDateTime;
import java.time.temporal.ChronoUnit;

import static java.lang.String.format;

@Service
public class RetryWithIntervalStrategy implements RepeatStrategy<RetryWithIntervalOptions> {
    private static final Logger LOG = LoggerFactory.getLogger(RetryWithIntervalStrategy.class);

    @Override
    public CallTask processPostponing(
            CallTask callTask,
            RetryWithIntervalOptions repeatOptions,
            RepeatOptions.RepeatReason repeatReason
    ) {
        switch (repeatReason) {
            case TIMEOUT:
                return postponeTimeout(callTask, repeatOptions);
            case BUSY:
                return postponeBusy(callTask, repeatOptions);
            default:
                throw new IllegalStateException(format("unexpected repeat reason %s", repeatReason));
        }
    }

    private CallTask postponeTimeout(CallTask callTask, RetryWithIntervalOptions repeatOptions) {
        LOG.info(format("process postponing for timeout %s, %s", callTask, repeatOptions));
        callTask = failIfAttemptsExceeded(
                callTask,
                repeatOptions.getTimeoutAttempts(),
                RepeatOptions.RepeatReason.TIMEOUT
        );
        if (callTask.getState() == CallTask.State.FAILED) {
            return callTask;
        }

        LocalDateTime nextAttempt = LocalDateTime.now().plus(
                repeatOptions.getSecondsBeforeNextTimeoutAttempt(),
                ChronoUnit.SECONDS
        );
        return callTask.postpone(
                TimeService.toEpochSeconds(nextAttempt),
                TimeService.toEpochSeconds(LocalDateTime.now()),
                "запланирована следующая попытка дозвониться"
        );
    }

    private CallTask postponeBusy(CallTask callTask, RetryWithIntervalOptions repeatOptions) {
        LOG.info(format("process postponing for busy for %s, %s", callTask, repeatOptions));
        callTask = failIfAttemptsExceeded(
                callTask,
                repeatOptions.getBusyAttempts(),
                RepeatOptions.RepeatReason.TIMEOUT
        );
        if (callTask.getState() == CallTask.State.FAILED) {
            return callTask;
        }

        LocalDateTime nextAttempt = LocalDateTime.now().plus(
                repeatOptions.getSecondsBeforeNextTimeoutAttempt(),
                ChronoUnit.SECONDS
        );
        return callTask.postpone(
                TimeService.toEpochSeconds(nextAttempt),
                TimeService.toEpochSeconds(LocalDateTime.now()),
                "запланирована следующая попытка дозвониться"
        );
    }

    @Override
    public Class<RetryWithIntervalOptions> getProcessingOptions() {
        return RetryWithIntervalOptions.class;
    }

    private CallTask failIfAttemptsExceeded(CallTask callTask, long maxAttempts, RepeatOptions.RepeatReason reason) {
        if (!callTask.getRepeats().containsKey(reason)) {
            callTask.getRepeats().put(reason, 0);
        }

        // on this stage, repeats already performed
        int repeatsPerformed = callTask.getRepeats().get(reason);

        if (repeatsPerformed == maxAttempts) {
            return failBecaiseAttemptsExceeded(callTask);
        } else if (repeatsPerformed > maxAttempts) {
            return failBecaiseAttemptsExceeded(callTask);
        }

        // on this stage, repeats planned; but if next repeat will fail, they will count as performed
        callTask.getRepeats().put(reason, ++repeatsPerformed);
        return callTask;
    }

    private CallTask failBecaiseAttemptsExceeded(CallTask callTask) {
        long now = TimeService.localDtNowSinceEpochSeconds();
        return callTask.failed(now, "недозвонились, попытки дозвониться исчерпаны");
    }


}
