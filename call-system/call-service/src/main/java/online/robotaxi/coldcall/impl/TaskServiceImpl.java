package online.robotaxi.coldcall.impl;

import online.robotaxi.coldcall.TaskService;
import online.robotaxi.model.CallTask;
import online.robotaxi.repository.CallTaskRepository;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import static java.lang.String.format;

@Service
public class TaskServiceImpl implements TaskService {
    private static final Logger LOG = LoggerFactory.getLogger(TaskServiceImpl.class);
    private final CallTaskRepository taskRepository;

    @Autowired
    public TaskServiceImpl(CallTaskRepository taskRepository) {
        this.taskRepository = taskRepository;
    }

    @Override
    public CallTask create(String campaign, String phone) {
        CallTask callTask = new CallTask(campaign, phone);
        LOG.info(format("adding task %s", callTask));
        return taskRepository.save(callTask);
    }
}
