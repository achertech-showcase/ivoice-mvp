package online.robotaxi.coldcall;

import online.robotaxi.coldcall.repeat.RepeatOptions;
import online.robotaxi.model.CallTask;

/**
 * how to handle busy numbers
 */
public interface RepeatStrategy<T extends RepeatOptions> {
    CallTask processPostponing(CallTask callTask, T repeatOptions, RepeatOptions.RepeatReason repeatReason);

    Class<T> getProcessingOptions();
}
