package online.robotaxi.coldcall.impl;

import online.robotaxi.coldcall.ColdCallCampaignService;
import online.robotaxi.model.Campaign;
import online.robotaxi.repository.CampaignRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.Optional;

@Service
public class ColdCallCampaignServiceImpl implements ColdCallCampaignService {
    private final CampaignRepository campaignRepository;

    @Autowired
    public ColdCallCampaignServiceImpl(CampaignRepository campaignRepository) {this.campaignRepository = campaignRepository;}

    @Override
    public boolean start(String campaignName) {
        Optional<Campaign> maybeCampaign = campaignRepository.findOneByName(campaignName);
        if (!maybeCampaign.isPresent()) {
            return false;
        }
        Campaign campaign = maybeCampaign.get();
        return campaignRepository.save(campaign.start()).getState() == Campaign.State.STARTED;
    }

    @Override
    public boolean stop(String campaignName) {
        Optional<Campaign> maybeCampaign = campaignRepository.findOneByName(campaignName);
        if (!maybeCampaign.isPresent()) {
            return false;
        }
        Campaign campaign = maybeCampaign.get();
        return campaignRepository.save(campaign.stop()).getState() == Campaign.State.PREPARING;
    }
}
