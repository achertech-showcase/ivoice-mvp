package online.robotaxi.freeswitch;

import org.freeswitch.esl.client.IEslEventListener;
import org.freeswitch.esl.client.transport.CommandResponse;
import org.freeswitch.esl.client.transport.SendMsg;
import org.freeswitch.esl.client.transport.message.EslMessage;

import java.util.List;

public interface InboundClient {
    EslMessage sendSyncApiCommand(String command, String arg);

    CommandResponse sendMessage(SendMsg sendMsg);

    EslMessage originateCall(String script, String taskId, String phone);

    void subscribe(IEslEventListener listener);

    EslMessage sendEvent(List<String> headers);

    void addEventFilter(String eventHeader, String headerValue);
}
