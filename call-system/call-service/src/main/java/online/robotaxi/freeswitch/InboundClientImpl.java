package online.robotaxi.freeswitch;

import online.robotaxi.config.FreeswitchProperties;
import org.freeswitch.esl.client.IEslEventListener;
import org.freeswitch.esl.client.inbound.Client;
import org.freeswitch.esl.client.inbound.InboundConnectionFailure;
import org.freeswitch.esl.client.transport.CommandResponse;
import org.freeswitch.esl.client.transport.SendMsg;
import org.freeswitch.esl.client.transport.message.EslMessage;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Component;

import javax.annotation.PostConstruct;
import javax.annotation.PreDestroy;

import java.util.List;

import static java.lang.String.format;

@Component
public class InboundClientImpl implements InboundClient {
    private static Logger LOG = LoggerFactory.getLogger(InboundClientImpl.class);

    private final String host;
    private final int port;
    private final String password;
    private final String originateMode;

    private final Client client;

    public InboundClientImpl(FreeswitchProperties properties) {
        host = properties.getHost();
        port = properties.getPort();
        password = properties.getPassword();
        originateMode = properties.getOriginateMode();

        client = new Client();
    }

    @PostConstruct
    void connectOnStart() throws InterruptedException {
        LOG.info("Waiting 1 min for freeswitch to start");
        LOG.info("Client connecting ..");
        int tries = 0;
        boolean connected = false;
        while (!connected && tries < 10) {
            try {
                tries++;
                client.connect(host, port, password, 20);
                connected = true;
            } catch (InboundConnectionFailure inboundConnectionFailure) {
                LOG.info("sleep 10s before next try");
                Thread.sleep(10_000);
            }
        }
//
//        client.setEventSubscriptions("plain", "CHANNEL_HANGUP_COMPLETE CUSTOM");
        client.setEventSubscriptions("plain", "ALL");
//        client.addEventFilter("Event-Name", "CUSTOM");
        LOG.info("Client connected ..");
    }

    @PreDestroy
    void disconnectOnDestroy() {
        LOG.info("Client closing ..");
        client.close();
        LOG.info("Client closed ..");
    }

    @Override
    public EslMessage sendSyncApiCommand(String command, String arg) {
        return client.sendSyncApiCommand(command, arg);
    }

    @Override
    public CommandResponse sendMessage(SendMsg sendMsg) {
        return client.sendMessage(sendMsg);
    }

    @Override
    public EslMessage originateCall(String script, String taskId, String phone) {
        if (originateMode.equalsIgnoreCase("remote")) {
            //originate sofia/gateway/megafon/79771368592 yesno_demo
            String command = format(
                    "originate [script=%s,hangup_complete_with_xml=true,calltaskid=%s,call_timeout=20]sofia/gateway/megafon/%s calltask",
                    script,
                    taskId,
                    phone
            );
            return client.sendSyncApiCommand(command, "");
        } else if (originateMode.equalsIgnoreCase("local")) {
            return client.sendSyncApiCommand(
                    format(
                            "originate [script=%s,hangup_complete_with_xml=true,calltaskid=%s,call_timeout=20]user/%s@192.168.1.100 calltask",
                            script,
                            taskId,
                            phone
                    ),
                    ""
            );
        } else {
            throw new IllegalStateException(format("unexpected originate mode %s", originateMode));
        }
    }

    @Override
    public void subscribe(IEslEventListener listener) {
        client.addEventListener(listener);
    }

    @Override
    public EslMessage sendEvent(List<String> headers) {
        return client.sendEvent(headers);
    }

    @Override
    public void addEventFilter(String eventHeader, String headerValue){
        client.addEventFilter(eventHeader, headerValue);
    }
}
