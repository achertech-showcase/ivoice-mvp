package online.robotaxi.freeswitch;

import online.robotaxi.config.FreeswitchProperties;
import org.freeswitch.esl.client.IEslEventListener;
import org.freeswitch.esl.client.inbound.Client;
import org.freeswitch.esl.client.inbound.InboundConnectionFailure;
import org.freeswitch.esl.client.transport.event.EslEvent;
import org.freeswitch.esl.client.transport.message.EslHeaders;
import org.freeswitch.esl.client.transport.message.EslMessage;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import javax.annotation.PostConstruct;
import java.util.Map;

//@Component
@Deprecated
public class InboundClientDemo {
    private static final Logger LOG = LoggerFactory.getLogger(InboundClientDemo.class);

    private String host;
    private int port;
    private String password;

    @Autowired
    public InboundClientDemo(FreeswitchProperties properties) {
        host = properties.getHost();
        port = properties.getPort();
        password = properties.getPassword();
    }

    @SuppressWarnings("unused")
    @PostConstruct
    public void connect() throws InterruptedException {
        Client client = new Client();

        client.addEventListener(new IEslEventListener() {
            public void eventReceived(EslEvent event) {
                LOG.info("Event received [{}]", event);
            }

            public void backgroundJobResultReceived(EslEvent event) {
                LOG.info("Background job result received [{}]", event);
            }

        });

        LOG.info("Client connecting ..");
        try {
            client.connect(host, port, password, 2);
        } catch (InboundConnectionFailure e) {
            LOG.error("Connect failed", e);
            return;
        }
        LOG.info("Client connected ..");

//      client.setEventSubscriptions( "plain", "heartbeat CHANNEL_CREATE CHANNEL_DESTROY BACKGROUND_JOB" );
//        client.setEventSubscriptions("plain", "all");
//        client.addEventFilter("Event-Name", "heartbeat");
//        client.cancelEventSubscriptions();
        client.setEventSubscriptions("plain", "all");
//        client.addEventFilter("Event-Name", "heartbeat");
//        client.addEventFilter("Event-Name", "channel_create");
//        client.addEventFilter("Event-Name", "background_job");
        client.sendSyncApiCommand("echo", "Foo foo bar");

//        client.sendSyncCommand( "originate", "sofia/internal/101@192.168.100.201! sofia/internal/102@192.168.100.201!" );

//        client.sendSyncApiCommand( "sofia status", "" );
        String jobId = client.sendAsyncApiCommand("status", "");
        LOG.info("Job id [{}] for [status]", jobId);
        client.sendSyncApiCommand("version", "");
//        client.sendAsyncApiCommand( "status", "" );
//        client.sendSyncApiCommand( "sofia status", "" );
//        client.sendAsyncApiCommand( "status", "" );
        EslMessage response = client.sendSyncApiCommand("sofia status", "");
        LOG.info("sofia status = [{}]", response.getBodyLines().get(3));

        // wait to see the heartbeat events arrive
//        Thread.sleep(25000);
//        client.close();

        client.addEventListener(new IEslEventListener() {
            @Override
            public void eventReceived(EslEvent event) {
                if (event.getEventName().equalsIgnoreCase("custom")) {
                    LOG.info(event.getEventBodyLines().toString());
                }
            }

            @Override
            public void backgroundJobResultReceived(EslEvent event) {

            }
        });
    }

    public void multiConnects() throws InterruptedException {
        Client client = new Client();

        LOG.info("Client connecting ..");
        try {
            client.connect(host, port, password, 2);
        } catch (InboundConnectionFailure e) {
            LOG.error("Connect failed", e);
            return;
        }
        LOG.info("Client connected ..");

        LOG.info("Client connecting ..");
        try {
            client.connect(host, port, password, 2);
        } catch (InboundConnectionFailure e) {
            LOG.error("Connect failed", e);
            return;
        }
        LOG.info("Client connected ..");

        client.close();
    }

    public void sofiaContact() {
        Client client = new Client();
        try {
            client.connect(host, port, password, 2);
        } catch (InboundConnectionFailure e) {
            LOG.error("Connect failed", e);
            return;
        }

        EslMessage response = client.sendSyncApiCommand("sofia_contact", "internal/102@192.168.100.201");

        LOG.info("Response to 'sofia_contact': [{}]", response);
        for (Map.Entry<EslHeaders.Name, String> header : response.getHeaders().entrySet()) {
            LOG.info(" * header [{}]", header);
        }
        for (String bodyLine : response.getBodyLines()) {
            LOG.info(" * body [{}]", bodyLine);
        }
        client.close();
    }
}
