package online.robotaxi.speech.impl;

import online.robotaxi.config.AppProperties;
import online.robotaxi.speech.ConversionService;
import online.robotaxi.speech.ExchangeAudioWithAsteriskService;
import online.robotaxi.speech.yandex.AudioFile;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.Optional;

import static java.lang.String.format;

//@Service
public class ExchangeAudioWithAsteriskServiceImpl implements ExchangeAudioWithAsteriskService {
    private static final Logger LOG = LoggerFactory.getLogger(ExchangeAudioWithAsteriskServiceImpl.class);
    private final Path records;

    private final ConversionService conversionService;

    @Autowired
    public ExchangeAudioWithAsteriskServiceImpl(
            AppProperties config,
            ConversionService conversionService
    ) {
        this.conversionService = conversionService;
        records = Paths.get(config.getRecords());
        if (Files.notExists(records)) {
            throw new IllegalStateException(format("records path %s not exist", records));
        }
        LOG.info(format("records path initialized: %s", records));
    }

    @Override
    public boolean push(
            String orderId, AudioFile file, AudioMeaning meaning
    ) {
        LOG.info(format("writing converted to asterisk audio for orderId %s with meaning %s", orderId, meaning));
        return conversionService.writeConvertedToAsterisk(orderId, meaning, file);
    }

    @Override
    public Optional<AudioFile> pull(
            String orderId, AudioMeaning meaning
    ) {
        throw new RuntimeException("not implemented");
    }

    @Override
    public Optional<AudioFile> pullAndRemove(
            String orderId, AudioMeaning meaning
    ) {
        String recordFileName = ExchangeAudioWithAsteriskService.recordFileName(orderId, meaning);
        Path record = records.resolve(recordFileName);
        if (Files.notExists(record)) {
            LOG.error(format("audio record file not exists: %s; orderId = %s, meaning = %s", record, orderId, meaning));
            return Optional.empty();
        }
        try {
            AudioFile audioFile = new AudioFile(Files.readAllBytes(record));
            return Optional.of(audioFile);
        } catch (IOException e) {
            LOG.error(e.getMessage());
            return Optional.empty();
        } finally {
            removeRecord(record);
        }
    }

    @Override
    public String recordPath(String fileName) {
        return records.resolve(fileName).toString();
    }

    @Override
    public Optional<AudioFile> loadAndRemove(String fileName, String extension) {
        Path record = records.resolve(format("%s.%s", fileName, extension));
        try {
            return load(record);
        } finally {
            removeRecord(record);
        }
    }


    @Override
    public Optional<AudioFile> load(String fileName, String extension) {
        Path record = records.resolve(format("%s.%s", fileName, extension));
        return load(record);
    }

    private Optional<AudioFile> load(Path record) {
        if (Files.notExists(record)) {
            LOG.error(format("audio record file not exists: %s", record));
            return Optional.empty();
        }
        try {
            AudioFile audioFile = new AudioFile(Files.readAllBytes(record));
            return Optional.of(audioFile);
        } catch (IOException e) {
            LOG.error(e.getMessage());
            return Optional.empty();
        }
    }

    private void removeRecord(Path record) {
        try {
            Files.delete(record);
        } catch (IOException e) {
            LOG.error(format("can't remove record: %s", e.getMessage()));
        }
    }
}
