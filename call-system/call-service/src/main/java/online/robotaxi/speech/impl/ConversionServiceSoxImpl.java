package online.robotaxi.speech.impl;

import ie.corballis.sox.Sox;
import ie.corballis.sox.WrongParametersException;
import online.robotaxi.config.AppProperties;
import online.robotaxi.speech.ConversionService;
import online.robotaxi.speech.ExchangeAudioWithAsteriskService;
import online.robotaxi.speech.yandex.AudioFile;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.nio.file.StandardOpenOption;

import static java.lang.String.format;
import static online.robotaxi.speech.ExchangeAudioWithAsteriskService.AudioMeaning.CONVERSION;

//@Service
public class ConversionServiceSoxImpl implements ConversionService {
    private static final Logger LOG = LoggerFactory.getLogger(ConversionServiceSoxImpl.class);
    private static final String SOX_BIN = "/usr/bin/sox";

    private final Path recordsDir;

    @Autowired
    public ConversionServiceSoxImpl(AppProperties config) {
        recordsDir = Paths.get(config.getRecords());
        if (Files.notExists(recordsDir)) {
            throw new IllegalStateException(format("conversion path %s not exist", recordsDir));
        }
    }

    @Override
    public boolean writeConvertedToAsterisk(
            String orderId,
            ExchangeAudioWithAsteriskService.AudioMeaning meaning,
            AudioFile original
    ) {
        Sox sox = new Sox(SOX_BIN);
        String originalRecordFileName = ExchangeAudioWithAsteriskService.recordFileName(orderId, CONVERSION);
        Path originalPath = recordsDir.resolve(originalRecordFileName);

        String resultFileName = ExchangeAudioWithAsteriskService.recordFileName(orderId, meaning, "sln");
        Path resultPath = recordsDir.resolve(resultFileName);

        try {
            //-t raw -r 8k -e signed-integer -b 16 -c 1
            Files.write(
                    originalPath,
                    original.getBytes(),
                    StandardOpenOption.CREATE // may override existing
            );
            sox.argument("-t", "raw")
                    .argument("-r", "8k")
                    .argument("-e", "signed-integer")
                    .argument("-b", "16")
                    .argument("-c", "1")
                    .inputFile(originalPath.toAbsolutePath().toString())
                    .outputFile(resultPath.toAbsolutePath().toString())
                    .execute();
            Files.delete(originalPath);
            return true;
        } catch (IOException | WrongParametersException e) {
            LOG.error(format("audio conversion failed for order %s with exception %s", orderId, e.getMessage()));
            return false;
        }
    }
}
