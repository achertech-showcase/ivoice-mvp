package online.robotaxi.speech;

import online.robotaxi.speech.yandex.AudioFile;

import java.util.Optional;

import static java.lang.String.format;

/**
 * Protocol for audio exchange:
 * number_time_audioMeaning.ext
 * <p>
 * fileName format is uid for current session
 * number_time - id of order
 * <p>
 * for example, when user speak address, then file: number_time_addr-request.wav
 * when server validates address, then file: number_time_addr-validate.wav
 */
public interface ExchangeAudioWithAsteriskService {
    String AUDIO_FORMAT = "wav";
    String RECORD_FILE_NAME_FORMAT = "%s_%s.%s";

    /**
     * @return push performed, file available for asterisk
     */
    boolean push(String orderId, AudioFile file, AudioMeaning meaning);

    Optional<AudioFile> pull(String orderId, AudioMeaning meaning);

    Optional<AudioFile> pullAndRemove(String orderId, AudioMeaning meaning);

    /**
     * something like /var/spool/asterisk/custom/client/campaign/{fileName}
     */
    String recordPath(String fileName);

    static String recordFileName(String orderId, AudioMeaning audioMeaning) {
        return recordFileName(orderId, audioMeaning, AUDIO_FORMAT);
    }

    static String recordFileName(String orderId, AudioMeaning audioMeaning, String format) {
        return format(RECORD_FILE_NAME_FORMAT, orderId, audioMeaning.recordFilePostfix, format);
    }

    /**
     * file name without records folder part
     */
    Optional<AudioFile> loadAndRemove(String file, String extension);

    /**
     * file name without records folder part;
     * do not remove record
     */
    Optional<AudioFile> load(String file, String extension);

    enum AudioMeaning {
        ADDRESS_REQUEST(10, "addr-request"),
        ADDRESS_VALIDATION(20, "addr-validate"),
        YES_NO_RESPONSE(800, "yesno"),
        CONVERSION(900, "conv");

        private final int value;
        private final String recordFilePostfix;

        AudioMeaning(int value, String recordFilePostfix) {
            this.value = value;
            this.recordFilePostfix = recordFilePostfix;
        }

        public int getValue() {
            return value;
        }
    }
}
