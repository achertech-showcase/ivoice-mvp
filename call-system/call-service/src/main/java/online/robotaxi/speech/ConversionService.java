package online.robotaxi.speech;

import online.robotaxi.speech.yandex.AudioFile;

import java.nio.file.Path;
import java.util.Optional;

public interface ConversionService {
    boolean writeConvertedToAsterisk(String orderId, ExchangeAudioWithAsteriskService.AudioMeaning meaning, AudioFile original);
}
