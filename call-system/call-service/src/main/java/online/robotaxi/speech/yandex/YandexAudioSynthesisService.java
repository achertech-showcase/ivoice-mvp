package online.robotaxi.speech.yandex;

import com.google.common.io.ByteStreams;
import online.robotaxi.config.YandexSpeechProperties;
import online.robotaxi.speech.AudioSynthesisService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpMethod;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Service;
import org.springframework.web.client.RestClientException;
import org.springframework.web.client.RestTemplate;
import org.springframework.web.util.UriComponentsBuilder;

import java.util.Optional;

/**
 * https://tech.yandex.ru/speechkit/cloud/doc/guide/concepts/tts-http-request-docpage/
 */
@Service
public class YandexAudioSynthesisService implements AudioSynthesisService<AudioFile> {
    private static final Logger LOG = LoggerFactory.getLogger(YandexAudioSynthesisService.class);

    private final RestTemplate restClient;
    private final String synthesisMethodUrl;
    private final String apiKey;
    private final String quality;
    private final String speaker;
    private final String format;

    @Autowired
    public YandexAudioSynthesisService(RestTemplate restClient, YandexSpeechProperties config) {
        this.restClient = restClient;
        synthesisMethodUrl = config.getSynthesisMethod();
        quality = config.getSynthesisQuality();
        format = config.getFormat();
        apiKey = config.getKey();
        this.speaker = config.getSpeaker();
    }

    @Override
    public Optional<AudioFile> synthesize(String text) {
        return callApi(text);
    }

    /**
     * @return empty if error
     */
    private Optional<AudioFile> callApi(String text) {
        UriComponentsBuilder builder = UriComponentsBuilder.fromUriString(synthesisMethodUrl)
                .queryParam("text", text)
                .queryParam("format", "wav")
                .queryParam("quality", quality)
                .queryParam("format", format)
                .queryParam("lang", "ru-RU")
                .queryParam("speaker", speaker)
                .queryParam("key", apiKey);

        try {
            return restClient.execute(
                    builder.build().encode().toUri(),
                    HttpMethod.GET,
                    request -> {
                    },
                    response -> {
                        if (response.getStatusCode() == HttpStatus.OK) {
                            AudioFile file = new AudioFile(ByteStreams.toByteArray(response.getBody()));
                            return Optional.of(file);
                        } else {
                            LOG.error(response.toString());
                            return Optional.empty();
                        }
                    }
            );
        } catch (RestClientException e) {
            LOG.error(e.getMessage());
            return Optional.empty();
        }
    }
}
