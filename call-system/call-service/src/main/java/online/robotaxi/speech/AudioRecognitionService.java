package online.robotaxi.speech;

import java.util.Optional;

public interface AudioRecognitionService<T extends Audio> {
    /**
     * @return empty in case failed to recognize
     */
    Optional<String> recognize(T audio, RecognitionTopic topic);
}
