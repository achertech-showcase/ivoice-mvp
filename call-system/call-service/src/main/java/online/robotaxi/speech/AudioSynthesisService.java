package online.robotaxi.speech;

import java.util.Optional;

public interface AudioSynthesisService<T> {
    /**
     * @return audio, or empty if failed to synthesize
     */
    Optional<T> synthesize(String text);
}
