package online.robotaxi.speech;

import java.nio.file.Path;

public interface VolumeService {
    Path increase(Path original, int gain);
}
