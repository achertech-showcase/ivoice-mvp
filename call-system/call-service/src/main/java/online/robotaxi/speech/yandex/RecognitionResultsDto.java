package online.robotaxi.speech.yandex;

import com.google.common.base.Objects;

import java.util.Collections;
import java.util.List;

public class RecognitionResultsDto {
    private final boolean success;
    private final List<Variant> variants;

    private RecognitionResultsDto(
            boolean success,
            List<Variant> variants
    ) {
        this.success = success;
        this.variants = variants;
    }

    boolean isSuccess() {
        return success;
    }

    List<Variant> getVariants() {
        return variants;
    }

    static RecognitionResultsDto successfull(List<Variant> variants) {
        return new RecognitionResultsDto(true, variants);
    }

    static RecognitionResultsDto unsuccessfull() {
        return new RecognitionResultsDto(false, Collections.emptyList());
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        RecognitionResultsDto that = (RecognitionResultsDto) o;
        return success == that.success &&
                Objects.equal(variants, that.variants);
    }

    @Override
    public int hashCode() {
        return Objects.hashCode(success, variants);
    }

    @Override
    public String toString() {
        return "RecognitionResultsDto{" +
                "success=" + success +
                ", variants=" + variants +
                '}';
    }

    public static final class Variant {
        private final float confidence;
        private final String value;

        Variant(float confidence, String value) {
            this.confidence = confidence;
            this.value = value;
        }

        public float getConfidence() {
            return confidence;
        }

        String getValue() {
            return value;
        }

        @Override
        public boolean equals(Object o) {
            if (this == o) return true;
            if (o == null || getClass() != o.getClass()) return false;
            Variant variant = (Variant) o;
            return Float.compare(variant.confidence, confidence) == 0 &&
                    Objects.equal(value, variant.value);
        }

        @Override
        public int hashCode() {
            return Objects.hashCode(confidence, value);
        }

        @Override
        public String toString() {
            return "Variant{" +
                    "confidence=" + confidence +
                    ", value='" + value + '\'' +
                    '}';
        }
    }
}
