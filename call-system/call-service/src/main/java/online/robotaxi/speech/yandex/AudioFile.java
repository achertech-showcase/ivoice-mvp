package online.robotaxi.speech.yandex;

import online.robotaxi.speech.Audio;

public class AudioFile implements Audio {
    private final byte[] bytes;

    public AudioFile(byte[] bytes) {
        this.bytes = bytes;
    }

    public byte[] getBytes() {
        return bytes;
    }
}
