package online.robotaxi.speech.yandex;

import online.robotaxi.config.YandexSpeechProperties;
import online.robotaxi.speech.AudioRecognitionService;
import online.robotaxi.speech.RecognitionTopic;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpMethod;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.stereotype.Service;
import org.springframework.web.client.RestClientException;
import org.springframework.web.client.RestTemplate;
import org.springframework.web.util.UriComponentsBuilder;
import org.w3c.dom.Document;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;
import org.xml.sax.SAXException;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;
import java.io.IOException;
import java.io.InputStream;
import java.util.*;
import java.util.zip.GZIPInputStream;

import static java.lang.String.format;

/**
 * https://tech.yandex.ru/speechkit/cloud/doc/guide/concepts/asr-quick-start-docpage/
 */
@Service
public class YandexAudioFileRecognition implements AudioRecognitionService<AudioFile> {
    private static final Logger LOG = LoggerFactory.getLogger(YandexAudioFileRecognition.class);
    private static final String HOST = "asr.yandex.net";
    private static final String METHOD = format("https://%s/asr_xml", HOST);

    private static final Map<RecognitionTopic, String> TOPIC_TO_YANDEX_PARAMETER;

    static {
        // https://tech.yandex.ru/speechkit/cloud/doc/guide/concepts/asr-overview-technology-docpage/#model
        TOPIC_TO_YANDEX_PARAMETER = new HashMap<>();
        TOPIC_TO_YANDEX_PARAMETER.put(RecognitionTopic.ADDRESS, "maps");
        // TODO yes no could be recognized using static local recognition, maybe sphinx cmu
        TOPIC_TO_YANDEX_PARAMETER.put(RecognitionTopic.ANY, "queries");
    }

    private static final String LANG = "ru-RU";
    private final RestTemplate restClient;
    private final String clientUid;
    private final String key;

    @Autowired
    YandexAudioFileRecognition(RestTemplate restClient, YandexSpeechProperties config) {
        this.restClient = restClient;
        this.clientUid = config.getClientUuid();
        this.key = config.getKey();
//        uri = format(
//                "https://%s/%s?uuid=%s&key=%s&topic=%s&lang=%s",
//                HOST,
//                METHOD,
//                config.getClientUuid(),
//                config.getKey(),
//                ADDRESS_TOPIC,
//                LANG
//        );
    }

    @Override
    public Optional<String> recognize(AudioFile audio, RecognitionTopic topic) {
        Optional<RecognitionResultsDto> maybeResponse = requestApi(audio, TOPIC_TO_YANDEX_PARAMETER.get(topic));
        if (!maybeResponse.isPresent()) {
            return Optional.empty();
        }
        RecognitionResultsDto response = maybeResponse.get();
        LOG.info(response.toString());
        if (!response.isSuccess()) {
            return Optional.empty();
        }
        return Optional.of(response.getVariants().get(0).getValue());
    }

    /**
     * @return empty if error
     */
    private Optional<RecognitionResultsDto> requestApi(AudioFile audioFile, String topic) {
        UriComponentsBuilder builder = UriComponentsBuilder.fromUriString(METHOD)
                .queryParam("uuid", clientUid)
                .queryParam("key", key)
                .queryParam("topic", topic)
                .queryParam("lang", LANG);
        try {
            return restClient.execute(
                    builder.build().encode().toUri(),
                    HttpMethod.POST,
                    request -> {
                        HttpHeaders headers = request.getHeaders();
                        headers.setContentType(MediaType.valueOf("audio/x-wav"));
                        headers.set("host", HOST);
                        headers.set("Transfer-Encoding", "chunked");
                        headers.set("accept-encoding", "gzip, deflate");
                        request.getBody().write(audioFile.getBytes());
                        request.getBody().flush();
                    },
                    response -> {
                        if (response.getStatusCode() == HttpStatus.OK) {
                            LOG.debug(response.getHeaders().toString());
                            return parseResponse(new GZIPInputStream(response.getBody()));
                        } else {
                            LOG.error(response.toString());
                            return Optional.empty();
                        }
                    }
            );
        } catch (RestClientException e) {
            LOG.error(e.getMessage());
            return Optional.empty();
        }
    }

    Optional<RecognitionResultsDto> parseResponse(InputStream responseBody) throws IOException {
        DocumentBuilderFactory dbf = DocumentBuilderFactory.newInstance();
        try {
            DocumentBuilder db = dbf.newDocumentBuilder();
            Document document = db.parse(responseBody);
            Node recognitionResults = document.getElementsByTagName("recognitionResults").item(0);
            Node successAttribute = recognitionResults.getAttributes().getNamedItem("success");
            byte successByte = Byte.valueOf(successAttribute.getNodeValue());
            boolean success = successByte == 1;
            if (!success) {
                return Optional.of(RecognitionResultsDto.unsuccessfull());
            }
            NodeList variantNodes = recognitionResults.getChildNodes();
            List<RecognitionResultsDto.Variant> variants = new ArrayList<>(variantNodes.getLength());
            // each
            for (int i = 0; i < variantNodes.getLength(); i++) {
                Node variantNode = variantNodes.item(i);
                if (variantNode.getNodeType() != Node.ELEMENT_NODE) {
                    continue;
                }
                Float confidence = Float.valueOf(
                        variantNode.getAttributes().getNamedItem("confidence").getNodeValue()
                );
                String value = variantNode.getFirstChild().getNodeValue();
                RecognitionResultsDto.Variant variant = new RecognitionResultsDto.Variant(confidence, value);
                variants.add(variant);
            }
            return Optional.of(RecognitionResultsDto.successfull(variants));
        } catch (ParserConfigurationException | SAXException e) {
            throw new IllegalStateException(e);
        }
    }
}
