package online.robotaxi.speech.impl;

import online.robotaxi.speech.VolumeService;
import org.springframework.stereotype.Service;

import java.io.IOException;
import java.nio.file.Path;

import static java.lang.String.format;

@Service
public class VolumeServiceSoxImpl implements VolumeService {
    @Override
    public Path increase(Path original, int gain) {
        try {
            Path name = original.getFileName();
            String nameWithoutExtension = com.google.common.io.Files.getNameWithoutExtension(name.toString());
            Path gainPath = original.getParent().resolve(nameWithoutExtension + "_loud.wav");
            Process exec = Runtime.getRuntime().exec(format("sox %s %s gain -n %s", original, gainPath, gain));
            try {
                exec.waitFor();
                return gainPath;
            } catch (InterruptedException e) {
                throw new IllegalStateException(e);
            }
        } catch (IOException e) {
            e.printStackTrace();
            throw new IllegalStateException("e");
        }
    }
}
