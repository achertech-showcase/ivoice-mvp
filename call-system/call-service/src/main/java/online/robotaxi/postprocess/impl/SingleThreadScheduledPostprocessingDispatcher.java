package online.robotaxi.postprocess.impl;

import online.robotaxi.model.CallTask;
import online.robotaxi.postprocess.PostprocessingDispatcher;
import online.robotaxi.postprocess.Postprocessor;
import online.robotaxi.repository.CallTaskRepository;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Map;
import java.util.function.Function;
import java.util.stream.Collectors;

import static java.lang.String.format;

@Service
public class SingleThreadScheduledPostprocessingDispatcher implements PostprocessingDispatcher {
    private static final Logger LOG = LoggerFactory.getLogger(SingleThreadScheduledPostprocessingDispatcher.class);
    private final CallTaskRepository taskRepository;

    private Map<String, Postprocessor> postprocessors;

    public SingleThreadScheduledPostprocessingDispatcher(
            CallTaskRepository taskRepository,
            List<Postprocessor> postprocessors
    ) {
        this.taskRepository = taskRepository;

        this.postprocessors = postprocessors.stream().collect(Collectors.toMap(p -> p.getName(), Function.identity()));
    }

    @Scheduled(fixedDelay = 5000)
    public void process() {
        List<CallTask> toProcess = taskRepository.findAllByState(CallTask.State.TO_POSTPROCESS);
        for (CallTask callTask : toProcess) {
            String postprocessDescriptor = callTask.getPostprocessDescriptor();
            String postprocessorName = postprocessDescriptor.split(":")[0];
            if (!postprocessors.containsKey(postprocessorName)) {
                LOG.error(format("postprocessor %s not found for task %s", postprocessorName, callTask.findObjectId()));
                continue;
            }

            Postprocessor postprocessor = postprocessors.get(postprocessorName);
            postprocessor.process(callTask);
        }
    }

}
