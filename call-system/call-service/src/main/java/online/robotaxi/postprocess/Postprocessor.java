package online.robotaxi.postprocess;

import online.robotaxi.model.CallTask;

public interface Postprocessor {
    String getName();
    void process(CallTask callTask);
}
