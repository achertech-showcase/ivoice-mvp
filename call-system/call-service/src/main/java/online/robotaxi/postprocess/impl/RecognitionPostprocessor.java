package online.robotaxi.postprocess.impl;

import online.robotaxi.config.RecordsProperty;
import online.robotaxi.model.CallTask;
import online.robotaxi.postprocess.Postprocessor;
import online.robotaxi.repository.CallTaskRepository;
import online.robotaxi.speech.AudioRecognitionService;
import online.robotaxi.speech.RecognitionTopic;
import online.robotaxi.speech.yandex.AudioFile;
import online.robotaxi.time.TimeService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Service;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.Optional;

import static java.lang.String.format;

@Service
public class RecognitionPostprocessor implements Postprocessor {
    private static final Logger LOG = LoggerFactory.getLogger(RecognitionPostprocessor.class);

    private final Path recordsParent;
    private final AudioRecognitionService<AudioFile> recognitionService;
    private final CallTaskRepository taskRepository;

    public RecognitionPostprocessor(
            RecordsProperty recordsProperty,
            AudioRecognitionService<AudioFile> recognitionService,
            CallTaskRepository taskRepository
    ) {
        this.recordsParent = Paths.get(recordsProperty.getRecordsParent());
        this.recognitionService = recognitionService;
        this.taskRepository = taskRepository;
        if (!Files.exists(recordsParent)) {
            throw new IllegalStateException(format("records parent path %s not exist", recordsParent));
        }
    }

    @Override
    public String getName() {
        return "recognize";
    }

    @Override
    public void process(CallTask callTask) {
        String descriptor = callTask.getPostprocessDescriptor();
        String recordName = descriptor.split(":")[1];
        Path recordFile = recordsParent.resolve(recordName);
        if (!Files.exists(recordFile)) {
            callTask.setPostprocessResult("бросили трубку до начала записи");
            callTask.setState(CallTask.State.PERFORMED);
            taskRepository.save(callTask);
            return;
        }

        Optional<String> recognition;
        try {
            AudioFile audioFile = audioFile(recordFile);
            recognition = recognitionService.recognize(audioFile, RecognitionTopic.ANY);
            callTask.log(TimeService.localDtNowSinceEpochSeconds(), "распознавание завершено");
        } catch (IOException e) {
            String error = format("record file reading error: %s; task %s", e.getMessage(), callTask.findObjectId());
            LOG.error(error);
            callTask.log(TimeService.localDtNowSinceEpochSeconds(), "ошибка чтения записи для распознавания");
            recognition = Optional.empty();
        }

        callTask.setState(CallTask.State.PERFORMED);
        callTask.setPostprocessResult(recognition.orElse("не удалось распознать запись"));

        taskRepository.save(callTask);
        LOG.info(format(
                "%s postprocessing finished with result %s",
                callTask.findObjectId(),
                callTask.getPostprocessResult()
        ));
    }

    private AudioFile audioFile(Path recordFile) throws IOException {
        return new AudioFile(Files.readAllBytes(recordFile));
    }
}
