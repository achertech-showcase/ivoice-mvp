/*
 * Copyright 2012 the original author or authors.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package online.robotaxi.repository;

import online.robotaxi.model.CallTask;
import org.springframework.data.repository.PagingAndSortingRepository;
import org.springframework.transaction.annotation.Transactional;

import java.math.BigInteger;
import java.util.Collection;
import java.util.List;
import java.util.Optional;

public interface CallTaskRepository extends PagingAndSortingRepository<CallTask, BigInteger> {
    @Transactional
    void deleteAllByCampaign(String campaign);

    Optional<CallTask> findFirstByCampaignAndStateOrderByReadySinceAsc(String campaig, CallTask.State state);

    List<CallTask> findFirstByCampaignAndState(String campaignName, CallTask.State state);

    List<CallTask> findAllByState(CallTask.State state);

    Optional<CallTask> findFirstByState(CallTask.State state);

    List<CallTask> findFirstByCampaignAndStateIn(String name, Collection<CallTask.State> states);

    List<CallTask> findAllByStateIn(List<CallTask.State> states);

    @Override
    List<CallTask> findAll();

    List<CallTask> findAllByCampaign(String campaignName);

    List<CallTask> findAllByCampaignAndTargetPhone(String campaignName, String targetPhone);
}
