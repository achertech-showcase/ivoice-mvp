package online.robotaxi.repository;

import online.robotaxi.model.YandexPassport;

import java.util.List;

public interface YandexPassportRepository extends NamedEntityRepository<YandexPassport> {
    List<YandexPassport> findAll();
}
