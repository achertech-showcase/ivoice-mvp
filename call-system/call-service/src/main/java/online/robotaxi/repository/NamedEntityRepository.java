package online.robotaxi.repository;

import online.robotaxi.model.NamedDocument;
import org.springframework.data.repository.NoRepositoryBean;
import org.springframework.data.repository.PagingAndSortingRepository;

import java.math.BigInteger;
import java.util.Optional;

@NoRepositoryBean
public interface NamedEntityRepository<T extends NamedDocument> extends PagingAndSortingRepository<T, BigInteger> {
    Optional<T> findOneByName(String name);
    void deleteAllByName(String name);
}
