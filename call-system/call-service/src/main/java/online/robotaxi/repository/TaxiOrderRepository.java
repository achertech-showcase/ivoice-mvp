package online.robotaxi.repository;

import online.robotaxi.model.TaxiOrder;
import org.springframework.data.repository.PagingAndSortingRepository;

import java.math.BigInteger;
import java.util.Collection;
import java.util.List;
import java.util.Optional;

public interface TaxiOrderRepository extends PagingAndSortingRepository<TaxiOrder, BigInteger> {
    List<TaxiOrder> findAll();

    List<TaxiOrder> findAllByOrderByIdDesc();

    Optional<TaxiOrder> findFirstByPhoneAndState(String phone, TaxiOrder.State state);

    List<TaxiOrder> findAllByPhone(String phone);

    Optional<TaxiOrder> findFirstByPhoneAndStateIn(String phone, Collection<TaxiOrder.State> states);

    Optional<TaxiOrder> findByRemoteId(String orderId);
}
