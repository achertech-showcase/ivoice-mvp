package online.robotaxi.taxi.processing;

import online.robotaxi.model.TaxiOrder;
import online.robotaxi.repository.TaxiOrderRepository;
import online.robotaxi.taxi.exception.PageFormatChangedException;
import online.robotaxi.taxi.model.OrderTask;
import org.openqa.selenium.By;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;
import org.springframework.stereotype.Component;
import org.springframework.util.StringUtils;

import java.util.List;
import java.util.Optional;

import static java.lang.String.format;

/**
 * for optimization, processor has 2 responsibilities
 */
@Component
public class FindUuidAndRefreshStateProcessor extends AbstractProcessor {

    FindUuidAndRefreshStateProcessor(TaxiOrderRepository taxiOrderRepository) {
        super(taxiOrderRepository);
    }

    @Override
    public OrderTask.Task processingTask() {
        return OrderTask.Task.FIND_UUID_AND_REFRESH_STATE;
    }

    @Override
    protected TaxiOrder processInternal(
            WebDriver webDriver, OrderTask orderTask
    ) throws PageFormatChangedException, IllegalOrderState {
        JavascriptExecutor javascriptExecutor = (JavascriptExecutor) webDriver;
        String remoteId = orderTask.getOrder().getRemoteId();
        if (StringUtils.isEmpty(remoteId)) {
            throw new IllegalStateException(
                    format("order remote id should be saved before trying to find uuid, %s", orderTask.getOrder())
            );
        }

        webDriver.get("https://lk.taximeter.yandex.ru/dispatcher");
        javascriptExecutor.executeScript("document.getElementById('filter-search-query').selectedIndex=0");
        new WebDriverWait(webDriver, 5)
                .until(ExpectedConditions.attributeToBe(By.id("filter-search-query"), "value", "0"));
        WebElement searchInput = findElement(webDriver, By.id("filter-search"));
        searchInput.clear();
        searchInput.sendKeys(orderTask.getOrder().getRemoteId());
        new WebDriverWait(webDriver, 2)
                .until(ExpectedConditions.attributeToBe(
                        By.id("filter-search"),
                        "value",
                        orderTask.getOrder().getRemoteId()
                ));

        findElement(webDriver, By.id("btn-search")).click();
        // #table1 > div.datagrid-body.nonbounce > div > table > tbody > tr:nth-child(2)
        new WebDriverWait(webDriver, 5)
                .until(ExpectedConditions.presenceOfElementLocated(
                        By.cssSelector("[data-phone]")
                ));

        // search by presence of attribute
        List<WebElement> rows = webDriver.findElements(By.cssSelector("[data-phone]"));

        if (rows.size() == 0) {
            throw new IllegalOrderState(
                    format("order not found by id %s", orderTask.getOrder().getRemoteId()),
                    orderTask
            );
        }

        // filter by phone number attribute then by id cell
        String phone;
        if (orderTask.getOrder().getPhone().length() > 4) {
            phone = orderTask.getOrder().getPhone();
        } else {
            // testing phone
            phone = "79771368592";
        }
        Optional<WebElement> maybeRow = rows.stream()
                .filter(r -> r.getAttribute("data-phone").equals(phone))
                .filter(r -> r.findElement(By.xpath("td[1]")).getText().equals(remoteId))
                .findFirst();

        if (!maybeRow.isPresent()) {
            throw new IllegalOrderState(
                    format("order not found by id %s", orderTask.getOrder().getRemoteId()),
                    orderTask
            );
        }
        WebElement row = maybeRow.get();
        String orderUuid = row.getAttribute("data-guid");
        if (StringUtils.isEmpty(orderUuid)) {
            throw new IllegalOrderState(format("order uuid not found; row %s;", row), orderTask);
        }

        String state = row.getAttribute("data-status");
        if (StringUtils.isEmpty(state)) {
            throw new IllegalOrderState(format(
                    "order uuid found, but state empty; row = %s, %s",
                    row,
                    orderTask.getOrder()
            ), orderTask);
        }
        TaxiOrder.State orderState = RefreshStateProcessor.STATE_MAPPING.get(state);

        orderTask.getOrder().setRemoteUuid(orderUuid);
        orderTask.getOrder().setState(orderState);
        LOG.info(format("order uuid = %s; state = %s found for %s", orderUuid, orderState, orderTask));
        return orderTask.getOrder();
    }
}
