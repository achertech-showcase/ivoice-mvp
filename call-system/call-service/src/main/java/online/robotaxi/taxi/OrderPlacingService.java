package online.robotaxi.taxi;

import online.robotaxi.model.TaxiOrder;
import online.robotaxi.taxi.exception.RemoteException;

import java.util.function.Consumer;

public interface OrderPlacingService {
    void place(TaxiOrder savedOrder, Runnable placedCallback, Runnable failedToPlaceCallback);
}
