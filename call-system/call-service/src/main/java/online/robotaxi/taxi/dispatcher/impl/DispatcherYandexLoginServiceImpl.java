package online.robotaxi.taxi.dispatcher.impl;

import online.robotaxi.model.YandexPassport;
import online.robotaxi.repository.YandexPassportRepository;
import online.robotaxi.taxi.dispatcher.DispatcherLoginService;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Service;

import java.util.List;

import static java.lang.String.format;

@Service
public class DispatcherYandexLoginServiceImpl implements DispatcherLoginService {
    private static final Logger LOG = LoggerFactory.getLogger(DispatcherYandexLoginServiceImpl.class);

    private final YandexPassportRepository passportRepository;

    public DispatcherYandexLoginServiceImpl(YandexPassportRepository passportRepository) {
        this.passportRepository = passportRepository;
    }

    @Override
    public void login(String dispatcherName, WebDriver webDriver) {
        LOG.info(format("logining %s into yandex...", dispatcherName));
        webDriver.get("https://passport.yandex.ru/auth");
        new WebDriverWait(webDriver, 5)
                .until(ExpectedConditions.urlToBe("https://passport.yandex.ru/auth"));

        YandexPassport passport = findPassport();

        webDriver.findElement(By.name("login")).sendKeys(passport.getLogin());
        webDriver.findElement(By.name("passwd")).sendKeys(passport.getPass());

        WebElement loginBtn = webDriver.findElement(By.cssSelector(".passport-Button-Text"));
        loginBtn.click();

        new WebDriverWait(webDriver, 10)
                .until(ExpectedConditions.urlToBe("https://passport.yandex.ru/profile"));
        LOG.info(format("%s logged in yandex successfully", dispatcherName));
    }

    private YandexPassport findPassport() {
        List<YandexPassport> passports = passportRepository.findAll();
        if (passports.isEmpty()) {
            throw new IllegalStateException("no yandex passports found in repository");
        }
        return passports.get(0);
    }
}
