package online.robotaxi.taxi.impl;

import com.fasterxml.jackson.databind.ObjectMapper;
import online.robotaxi.freeswitch.InboundClient;
import online.robotaxi.model.Address;
import online.robotaxi.model.TaxiOrder;
import online.robotaxi.repository.TaxiOrderRepository;
import online.robotaxi.taxi.OrderPlacingListener;
import online.robotaxi.taxi.OrderPlacingService;
import online.robotaxi.taxi.OrderReceiveService;
import online.robotaxi.taxi.dto.AddressDto;
import online.robotaxi.taxi.dto.OrderDto;
import org.freeswitch.esl.client.IEslEventListener;
import org.freeswitch.esl.client.transport.event.EslEvent;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Service;

import javax.annotation.PostConstruct;
import java.io.IOException;
import java.time.ZoneId;
import java.time.ZonedDateTime;
import java.util.Arrays;
import java.util.Base64;
import java.util.List;
import java.util.Map;

import static java.lang.String.format;

@Service
public class OrderReceiveServiceImpl implements OrderReceiveService, OrderPlacingListener {
    private static final Logger LOG = LoggerFactory.getLogger(OrderReceiveServiceImpl.class);
    private static final String EVENT_HEADER = "taxiOrderService";
    private static final ObjectMapper OBJECT_MAPPER = new ObjectMapper();

    private final InboundClient client;
    private final TaxiOrderRepository taxiOrderRepository;
    private final OrderPlacingService orderPlacingService;

    public OrderReceiveServiceImpl(
            InboundClient client,
            TaxiOrderRepository taxiOrderRepository,
            OrderPlacingService orderPlacingService
    ) {
        this.client = client;
        this.taxiOrderRepository = taxiOrderRepository;
        this.orderPlacingService = orderPlacingService;
    }

    @SuppressWarnings("unused")
    @PostConstruct
    public void initFreeswitchListening() {
        client.addEventFilter("service", EVENT_HEADER);
        client.subscribe(new IEslEventListener() {
            @Override
            public void eventReceived(EslEvent event) {
                if (filterPassed(event)) {
                    onEventReceived(event);
                }
            }

            @Override
            public void backgroundJobResultReceived(EslEvent event) {

            }
        });
        LOG.info("subscribed for ESL");
    }

    private void onEventReceived(EslEvent event) {
        String sessionUid = event.getEventHeaders().get("sessionUuid");

        String messageType = event.getEventHeaders().get("messageType");

        switch (messageType) {
            case "ORDER_CREATED":
                if (event.getEventBodyLines().isEmpty()) {
                    LOG.error("order json expected in body, but body lines empty; %s", event.getEventHeaders());
                    fireFailedToPlaceOrder(sessionUid);
                    return;
                }

                String jsonBase64 = event.getEventBodyLines().get(0);
                byte[] orderBytes = Base64.getDecoder().decode(jsonBase64);
                OrderDto orderDto;
                try {
                    String orderJson = new String(orderBytes, "UTF-8");
                    orderDto = OBJECT_MAPPER.readValue(orderJson, OrderDto.class);
                } catch (IOException e) {
                    LOG.error(format(
                            "order json parsing failed; sessionUuid: %s, headers: %s, body: %s",
                            sessionUid,
                            event.getEventHeaders(),
                            event.getEventBodyLines()
                    ));
                    fireFailedToPlaceOrder(sessionUid);
                    return;
                }

                TaxiOrder savedOrder = taxiOrderRepository.save(fromDto(sessionUid, orderDto));
                fireOrderPlaced(sessionUid);
                orderPlacingService.place(
                        savedOrder,
                        () -> fireOrderPlaced(sessionUid),
                        () -> fireFailedToPlaceOrder(sessionUid)
                );
                break;
            default:
                throw new IllegalStateException(format(
                        "unexpected message type %s for sessionUid %s",
                        messageType,
                        sessionUid
                ));
        }
    }

    static TaxiOrder fromDto(String callSessionUuid, OrderDto orderDto) {
        Address arrivalAddress = fromDto(orderDto.getArrivalAddress());
        ZonedDateTime creationTime = ZonedDateTime.now(ZoneId.of(orderDto.getTimeZoneOffset()));
        ZonedDateTime arrivalTime = orderDto.findArrivalTime(creationTime);
        return new TaxiOrder(
                callSessionUuid,
                creationTime,
                orderDto.getPhone(),
                arrivalTime,
                arrivalAddress,
                orderDto.getComments(),
                orderDto.getSelectedOptions()
        );
    }

    private static Address fromDto(AddressDto dto) {
        return new Address(dto.getFullAddress(), dto.getCity(), dto.getStreet(), dto.getHouse());
    }

    private void fireOrderPlaced(String sessionUuid) {
        List<String> headers = Arrays.asList(
                format("sessionUuid:%s", sessionUuid),
                "messageType:ORDER_CREATE_RESULT",
                "result:SUCCESS"
        );
        client.sendEvent(headers);
    }

    private void fireFailedToPlaceOrder(String sessionUuid) {
        List<String> headers = Arrays.asList(
                format("sessionUuid:%s", sessionUuid),
                "messageType:ORDER_CREATE_RESULT",
                "result:FAILURE"
        );
        client.sendEvent(headers);
    }

    private boolean filterPassed(EslEvent event) {
        Map<String, String> headers = event.getEventHeaders();
        return headers.containsKey("service") && headers.get("service").equals(EVENT_HEADER);
    }

    @Override
    public void orderPlaced(TaxiOrder order) {
        LOG.info(format("firing order placed event for %s", order));
        List<String> headers = Arrays.asList(
                format("sessionUuid:%s", order.getCallSessionUuid()),
                "messageType:ORDER_PLACING_RESULT",
                "result:SUCCESS"
        );
        client.sendEvent(headers);
    }

    @Override
    public void orderPlacingFailed(TaxiOrder order) {
        LOG.info(format("firing order placing failed event for %s", order));
        List<String> headers = Arrays.asList(
                format("sessionUuid:%s", order.getCallSessionUuid()),
                "messageType:ORDER_PLACING_RESULT",
                "result:FAILURE"
        );
        client.sendEvent(headers);
    }
}
