package online.robotaxi.taxi.processing;

import online.robotaxi.model.TaxiOrder;
import online.robotaxi.repository.TaxiOrderRepository;
import online.robotaxi.taxi.exception.PageFormatChangedException;
import online.robotaxi.taxi.model.OrderTask;
import org.openqa.selenium.*;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;
import org.springframework.stereotype.Component;
import org.springframework.util.StringUtils;

import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Optional;

import static java.lang.String.format;

@Component
public class RefreshStateProcessor extends AbstractProcessor {
    // data-status value in order row in relation to logical State
    static final Map<String, TaxiOrder.State> STATE_MAPPING;

    static {
        STATE_MAPPING = new HashMap<>();

        STATE_MAPPING.put("0", TaxiOrder.State.CREATED);
        STATE_MAPPING.put("10", TaxiOrder.State.ARRIVING);
        STATE_MAPPING.put("40", TaxiOrder.State.DRIVING);
        STATE_MAPPING.put("70", TaxiOrder.State.CANCELLED);
    }

    protected RefreshStateProcessor(TaxiOrderRepository taxiOrderRepository) {
        super(taxiOrderRepository);
    }

    @Override
    protected TaxiOrder processInternal(
            WebDriver webDriver, OrderTask orderTask
    ) throws PageFormatChangedException, IllegalOrderState {
        JavascriptExecutor javascriptExecutor = (JavascriptExecutor) webDriver;
        String remoteUuid = orderTask.getOrder().getRemoteUuid();
        if (StringUtils.isEmpty(remoteUuid)) {
            throw new IllegalStateException(
                    format("order remote uuid should be saved before trying to refresh state, %s", orderTask.getOrder())
            );
        }

        webDriver.get("https://lk.taximeter.yandex.ru/dispatcher");
        javascriptExecutor.executeScript("document.getElementById('filter-search-query').selectedIndex=0");
        new WebDriverWait(webDriver, 5)
                .until(ExpectedConditions.attributeToBe(By.id("filter-search-query"), "value", "0"));
        WebElement searchInput = findElement(webDriver, By.id("filter-search"));
        searchInput.clear();
        searchInput.sendKeys(orderTask.getOrder().getRemoteId());
        new WebDriverWait(webDriver, 2)
                .until(ExpectedConditions.attributeToBe(
                        By.id("filter-search"),
                        "value",
                        orderTask.getOrder().getRemoteId()
                ));

        findElement(webDriver, By.id("btn-search")).click();
        // #table1 > div.datagrid-body.nonbounce > div > table > tbody > tr:nth-child(2)
        new WebDriverWait(webDriver, 5)
                .until(ExpectedConditions.presenceOfElementLocated(
                        By.cssSelector("[data-guid]")
                ));

        // search by presence of attribute
        List<WebElement> rows = webDriver.findElements(By.cssSelector("[data-guid]"));

        if (rows.size() == 0) {
            throw new IllegalOrderState(
                    format("order not found by guid %s", orderTask.getOrder().getRemoteUuid()),
                    orderTask
            );
        }

        Optional<WebElement> maybeRow;
        try {
            maybeRow = rows.stream()
                    .filter(r -> r.getAttribute("data-guid").equals(orderTask.getOrder().getRemoteUuid()))
                    .findFirst();
        } catch (StaleElementReferenceException e){
            rows = webDriver.findElements(By.cssSelector("[data-guid]"));
            maybeRow = rows.stream()
                    .filter(r -> r.getAttribute("data-guid").equals(orderTask.getOrder().getRemoteUuid()))
                    .findFirst();
        }

        if (!maybeRow.isPresent()) {
            throw new IllegalOrderState(
                    format("order not found by uuid %s", orderTask.getOrder().getRemoteUuid()),
                    orderTask
            );
        }
        WebElement row = maybeRow.get();
        String stateCode = row.getAttribute("data-status");
        if (StringUtils.isEmpty(stateCode)) {
            throw new IllegalOrderState(format("order status not found; row %s;", row), orderTask);
        }

        TaxiOrder.State orderState = RefreshStateProcessor.STATE_MAPPING.get(stateCode);

        orderTask.getOrder().setState(orderState);
        LOG.info(format("order state = %s found for %s", orderState, orderTask));
        return orderTask.getOrder();
    }

    @Override
    public OrderTask.Task processingTask() {
        return OrderTask.Task.REFRESH_STATE;
    }
}
