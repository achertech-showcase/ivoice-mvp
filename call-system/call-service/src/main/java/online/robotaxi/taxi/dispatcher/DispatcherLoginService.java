package online.robotaxi.taxi.dispatcher;

import org.openqa.selenium.WebDriver;

public interface DispatcherLoginService {
    void login(String name, WebDriver webDriver);
}
