package online.robotaxi.taxi.dto;

import com.fasterxml.jackson.annotation.JsonCreator;

public class AddressDto {
    private String fullAddress;
    private String city;
    private String street;
    private String house;

    @JsonCreator
    public AddressDto() {

    }

    public AddressDto(String fullAddress, String city, String street, String house) {
        this.fullAddress = fullAddress;
        this.city = city;
        this.street = street;
        this.house = house;
    }

    public String getFullAddress() {
        return fullAddress;
    }

    public void setFullAddress(String fullAddress) {
        this.fullAddress = fullAddress;
    }

    public String getCity() {
        return city;
    }

    public void setCity(String city) {
        this.city = city;
    }

    public String getStreet() {
        return street;
    }

    public void setStreet(String street) {
        this.street = street;
    }

    public String getHouse() {
        return house;
    }

    public void setHouse(String house) {
        this.house = house;
    }
}
