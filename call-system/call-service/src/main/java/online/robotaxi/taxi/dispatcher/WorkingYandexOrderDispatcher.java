package online.robotaxi.taxi.dispatcher;

import online.robotaxi.taxi.Processor;
import org.openqa.selenium.WebDriver;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Component;

import java.util.List;

@Component
public class WorkingYandexOrderDispatcher extends AbstractOrderSeleniumDispatcher implements ProcessingOrderDispatcher {
    public WorkingYandexOrderDispatcher(
            @Qualifier("workingOrder") WebDriver webDriver,
            DispatcherLoginService loginService,
            List<Processor> processors
    ) {
        super(webDriver, loginService, processors);
    }

    @Override
    protected String defaultPage() {
        return "https://lk.taximeter.yandex.ru/dispatcher";
    }

    @Override
    public String getName() {
        return "processingOrder";
    }
}
