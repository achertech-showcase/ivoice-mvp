package online.robotaxi.taxi.dispatcher;

import online.robotaxi.taxi.Processor;
import org.openqa.selenium.WebDriver;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Component;

import java.util.List;

@Component
public class PlacingYandexOrderDispatcher extends AbstractOrderSeleniumDispatcher implements PlacingOrderDispatcher {
    public PlacingYandexOrderDispatcher(
            @Qualifier("placingOrder") WebDriver webDriver,
            DispatcherLoginService loginService,
            List<Processor> processors
    ) {
        super(webDriver, loginService, processors);
    }

    @Override
    protected String defaultPage() {
        return "https://lk.taximeter.yandex.ru/dispatcher/new";
    }

    @Override
    public String getName() {
        return "placingOrder";
    }
}
