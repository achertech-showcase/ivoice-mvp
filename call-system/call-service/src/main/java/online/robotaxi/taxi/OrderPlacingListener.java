package online.robotaxi.taxi;

import online.robotaxi.model.TaxiOrder;

public interface OrderPlacingListener {
    void orderPlaced(TaxiOrder order);
    void orderPlacingFailed(TaxiOrder order);
}
