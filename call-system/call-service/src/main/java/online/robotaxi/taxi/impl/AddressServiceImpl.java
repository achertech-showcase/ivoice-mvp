package online.robotaxi.taxi.impl;

import online.robotaxi.config.RecordsProperty;
import online.robotaxi.freeswitch.InboundClient;
import online.robotaxi.map.Address;
import online.robotaxi.map.MapService;
import online.robotaxi.speech.AudioRecognitionService;
import online.robotaxi.speech.AudioSynthesisService;
import online.robotaxi.speech.RecognitionTopic;
import online.robotaxi.speech.VolumeService;
import online.robotaxi.speech.yandex.AudioFile;
import online.robotaxi.taxi.AddressService;
import org.freeswitch.esl.client.IEslEventListener;
import org.freeswitch.esl.client.transport.event.EslEvent;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Service;

import javax.annotation.PostConstruct;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.Arrays;
import java.util.List;
import java.util.Map;
import java.util.Optional;

import static java.lang.String.format;

@Service
public class AddressServiceImpl implements AddressService {
    private static final Logger LOG = LoggerFactory.getLogger(AddressServiceImpl.class);
    private static final String EVENT_HEADER = "addressService";
    private static final int VOLUME_INCREASE = 10;

    private final InboundClient client;
    private final AudioRecognitionService<AudioFile> recognitionService;
    private final AudioSynthesisService<AudioFile> speechSynthesisService;
    private final MapService mapService;
    private final VolumeService volumeService;

    private final Path recordsParent;

    public AddressServiceImpl(
            InboundClient inboundClient,
            AudioRecognitionService<AudioFile> recognitionService,
            AudioSynthesisService<AudioFile> speechSynthesisService,
            MapService mapService,
            VolumeService volumeService,
            RecordsProperty recordsProperty
    ) {
        this.client = inboundClient;
        this.recognitionService = recognitionService;
        this.speechSynthesisService = speechSynthesisService;
        this.mapService = mapService;
        this.volumeService = volumeService;
        this.recordsParent = Paths.get(recordsProperty.getRecordsParent());
        if (!Files.exists(recordsParent)) {
            throw new IllegalStateException(format("records parent path %s not exist", recordsParent));
        }
    }

    @SuppressWarnings("unused")
    @PostConstruct
    public void initFreeswitchListening() {
        client.addEventFilter("service", EVENT_HEADER);
        client.subscribe(new IEslEventListener() {
            @Override
            public void eventReceived(EslEvent event) {
                if (filterPassed(event)) {
                    onEventReceived(event);
                }
            }

            @Override
            public void backgroundJobResultReceived(EslEvent event) {

            }
        });
        LOG.info("subscribed for ESL");
    }

    private boolean filterPassed(EslEvent event) {
        Map<String, String> headers = event.getEventHeaders();
        return headers.containsKey("service") && headers.get("service").equals(EVENT_HEADER);
    }

    private void onEventReceived(EslEvent event) {
        String sessionUid = event.getEventHeaders().get("sessionUuid");

        String messageType = event.getEventHeaders().get("messageType");

        switch (messageType) {
            case "ADDRESS_RECORDED":
                String city = event.getEventHeaders().get("city");
                String record = event.getEventHeaders().get("record");
                onAddressRecorded(sessionUid, city, record);
                break;
            default:
                throw new IllegalStateException(format(
                        "unexpected message type %s for sessionUid %s",
                        messageType,
                        sessionUid
                ));
        }
    }

    private void onAddressRecorded(String sessionUuid, String city, String record) {
        Path recordFile = recordsParent.resolve(record);
        if (!Files.exists(recordFile)) {
            LOG.error(format("no record exist for %s", sessionUuid));
            fireRecognitionFailedEvent(sessionUuid);
            return;
        }

        AudioFile audio;
        try {
            audio = audioFile(recordFile);
        } catch (IOException e) {
            LOG.error(format("error while parsing audio: %s", e.getMessage()));
            fireRecognitionFailedEvent(sessionUuid);
            return;
        }

        Optional<String> maybeRecognition = recognitionService.recognize(audio, RecognitionTopic.ADDRESS);
        if (!maybeRecognition.isPresent()) {
            LOG.warn(format("no recognition for %s", sessionUuid));
            fireRecognitionFailedEvent(sessionUuid);
            return;
        }

        String recognition = maybeRecognition.get();
        LOG.info(format("recognition for %s = %s", sessionUuid, recognition));

        Optional<Address> maybeMapAddress = mapService.findMapAddress(city, recognition);
        if (!maybeMapAddress.isPresent()) {
            LOG.warn(format("address not found for recognition %s, sessionUuid = %s", recognition, sessionUuid));
            fireRecognitionFailedEvent(sessionUuid);
            return;
        }
        LOG.info(format(
                "recognized address %s for %s, session %s",
                maybeMapAddress.get(),
                recognition,
                sessionUuid
        ));

        Address address = maybeMapAddress.get();
        String addressValidateStr = address.getFullAddress();
        Optional<Path> maybeRecognitionRecord = recordRecognition(sessionUuid, addressValidateStr);
        if (!maybeRecognitionRecord.isPresent()) {
            fireRecognitionFailedEvent(sessionUuid);
            return;
        }
        Path increasedVolRecord = volumeService.increase(maybeRecognitionRecord.get(), VOLUME_INCREASE);
        fireRecognized(sessionUuid, increasedVolRecord.getFileName().toString(), address);
    }

    private void fireRecognized(String sessionUuid, String recognizedAudioFile, Address address) {
        List<String> headers = Arrays.asList(
                format("sessionUuid:%s", sessionUuid),
                "messageType:ADDRESS_RECOGNITION_RESULT",
                "result:SUCCESS",
                format("recognizedAddress:%s", recognizedAudioFile),
                format("fullAddress:%s", address.getFullAddress()),
                format("city:%s", address.getCity()),
                format("street:%s", address.getStreet()),
                format("house:%s", address.getHouse())
        );
        client.sendEvent(headers);
    }

    private void fireRecognitionFailedEvent(String sessionUuid) {
        List<String> headers = Arrays.asList(
                format("sessionUuid:%s", sessionUuid),
                "messageType:ADDRESS_RECOGNITION_RESULT",
                "result:FAILURE"
        );
        client.sendEvent(headers);
    }

    private Optional<Path> recordRecognition(String sessionUuid, String recognition) {
        Optional<AudioFile> maybeRecord = this.speechSynthesisService.synthesize(recognition);
        if (!maybeRecord.isPresent()) {
            LOG.error(format("failed to synthesize speech for recognition %s", recognition));
            return Optional.empty();
        }
        Path recordPath = recordsParent.resolve(format("addr_validation_%s.wav", sessionUuid));
        try {
            Files.write(recordPath, maybeRecord.get().getBytes());
            return Optional.of(recordPath);
        } catch (IOException e) {
            LOG.error(format("error writing recognition audio: %s; sessionUid = %s", e.getMessage(), sessionUuid));
            return Optional.empty();
        }
    }

    private AudioFile audioFile(Path recordFile) throws IOException {
        return new AudioFile(Files.readAllBytes(recordFile));
    }
}
