package online.robotaxi.taxi.impl;

import online.robotaxi.freeswitch.InboundClient;
import online.robotaxi.model.TaxiOrder;
import online.robotaxi.repository.TaxiOrderRepository;
import online.robotaxi.taxi.OrderCancelService;
import online.robotaxi.taxi.dispatcher.ProcessingOrderDispatcher;
import online.robotaxi.taxi.model.OrderTask;
import org.freeswitch.esl.client.IEslEventListener;
import org.freeswitch.esl.client.transport.event.EslEvent;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Service;

import javax.annotation.PostConstruct;
import java.util.*;

import static java.lang.String.format;

@Service
public class OrderCancelServiceImpl implements OrderCancelService {
    private static final Logger LOG = LoggerFactory.getLogger(OrderCancelServiceImpl.class);
    private static final String EVENT_HEADER = "orderCancelService";

    private final InboundClient client;
    private final TaxiOrderRepository orderRepository;
    private final ProcessingOrderDispatcher dispatcher;

    public OrderCancelServiceImpl(
            InboundClient client,
            TaxiOrderRepository orderRepository,
            ProcessingOrderDispatcher dispatcher
    ) {
        this.client = client;
        this.orderRepository = orderRepository;
        this.dispatcher = dispatcher;
    }

    @SuppressWarnings("unused")
    @PostConstruct
    public void initFreeswitchListening() {
        client.addEventFilter("service", EVENT_HEADER);
        client.subscribe(new IEslEventListener() {
            @Override
            public void eventReceived(EslEvent event) {
                if (filterPassed(event)) {
                    onEventReceived(event);
                }
            }

            @Override
            public void backgroundJobResultReceived(EslEvent event) {

            }
        });
        LOG.info("subscribed for ESL");
    }

    private void onEventReceived(EslEvent event) {
        String sessionUid = event.getEventHeaders().get("sessionUuid");

        String messageType = event.getEventHeaders().get("messageType");

        switch (messageType) {
            case "CANCEL_ORDER":
                String orderId = event.getEventHeaders().get("orderId");

                Optional<TaxiOrder> maybeOrder = orderRepository.findByRemoteId(orderId);
                if (!maybeOrder.isPresent()) {
                    throw new IllegalStateException(format(
                            "inexisting order requested: remoteId=%s, sessionUuid=%s",
                            orderId,
                            sessionUid
                    ));
                }
                TaxiOrder order = maybeOrder.get();
                OrderTask cancelTask = new OrderTask(
                        order,
                        OrderTask.Task.CANCEL,
                        Collections.singletonList(() -> fireCancelled(sessionUid)),
                        Collections.singletonList(() -> fireCancellationFailed(sessionUid))
                );
                dispatcher.queueTask(cancelTask);
                break;
            default:
                throw new IllegalStateException(format(
                        "unexpected message type %s for sessionUid %s",
                        messageType,
                        sessionUid
                ));
        }
    }

    private void fireCancelled(String sessionUuid) {
        List<String> headers = Arrays.asList(
                format("sessionUuid:%s", sessionUuid),
                "messageType:CANCEL_ORDER",
                "result:true"
        );
        client.sendEvent(headers);
    }

    private void fireCancellationFailed(String sessionUuid) {
        List<String> headers = Arrays.asList(
                format("sessionUuid:%s", sessionUuid),
                "messageType:CANCEL_ORDER",
                "result:false"
        );
        client.sendEvent(headers);
    }

    private boolean filterPassed(EslEvent event) {
        Map<String, String> headers = event.getEventHeaders();
        return headers.containsKey("service") && headers.get("service").equals(EVENT_HEADER);
    }

}
