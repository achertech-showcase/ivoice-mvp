package online.robotaxi.taxi.impl;

import online.robotaxi.freeswitch.InboundClient;
import online.robotaxi.model.TaxiOrder;
import online.robotaxi.repository.TaxiOrderRepository;
import online.robotaxi.taxi.OrderInProgressService;
import online.robotaxi.taxi.dispatcher.ProcessingOrderDispatcher;
import online.robotaxi.taxi.model.OrderTask;
import org.freeswitch.esl.client.IEslEventListener;
import org.freeswitch.esl.client.transport.event.EslEvent;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Service;
import org.springframework.util.StringUtils;

import javax.annotation.PostConstruct;
import java.util.*;

import static java.lang.String.format;
import static online.robotaxi.model.TaxiOrder.State.*;

@Service
public class OrderInProgressServiceImpl implements OrderInProgressService {
    private static final Logger LOG = LoggerFactory.getLogger(OrderInProgressServiceImpl.class);
    private static final String EVENT_HEADER = "taxiOrderInProgressService";

    private final InboundClient client;
    private final TaxiOrderRepository orderRepository;
    private final ProcessingOrderDispatcher dispatcher;

    public OrderInProgressServiceImpl(
            InboundClient client,
            TaxiOrderRepository orderRepository,
            ProcessingOrderDispatcher dispatcher
    ) {
        this.client = client;
        this.orderRepository = orderRepository;
        this.dispatcher = dispatcher;
    }

    @SuppressWarnings("unused")
    @PostConstruct
    public void initFreeswitchListening() {
        client.addEventFilter("service", EVENT_HEADER);
        client.subscribe(new IEslEventListener() {
            @Override
            public void eventReceived(EslEvent event) {
                if (filterPassed(event)) {
                    onEventReceived(event);
                }
            }

            @Override
            public void backgroundJobResultReceived(EslEvent event) {

            }
        });
        LOG.info("subscribed for ESL");
    }

    private void onEventReceived(EslEvent event) {
        String sessionUid = event.getEventHeaders().get("sessionUuid");

        String messageType = event.getEventHeaders().get("messageType");

        switch (messageType) {
            case "REQUEST_ORDER_IN_PROGRESS":
                String phone = event.getEventHeaders().get("phone");

                Optional<TaxiOrder> maybeOrder = orderRepository.findFirstByPhoneAndStateIn(
                        phone,
                        Arrays.asList(PLACED, CREATED, ARRIVING, DRIVING)
                );
                if (maybeOrder.isPresent()) {
                    fireOrderPresent(sessionUid, maybeOrder.get());
                } else {
                    fireOrderNotPresent(sessionUid);
                }
                break;
            case "REFRESH_ORDER_STATE":
                String orderId = event.getEventHeaders().get("orderId");
                if (StringUtils.isEmpty(orderId)) {
                    throw new IllegalStateException(format(
                            "order id empty on REFRESH_ORDER_STATE for session %s",
                            sessionUid
                    ));
                }
                LOG.info(format("refreshing order state for orderId %s", orderId));
                refreshOrderState(sessionUid, orderId);
                break;
            default:
                throw new IllegalStateException(format(
                        "unexpected message type %s for sessionUid %s",
                        messageType,
                        sessionUid
                ));
        }
    }

    private void refreshOrderState(String sessionUid, String orderId) {
        Optional<TaxiOrder> maybeOrder = orderRepository.findByRemoteId(orderId);
        if (!maybeOrder.isPresent()) {
            String error = format("refresh order with %s was requested, but order not found in db", orderId);
            LOG.error(error);
            throw new IllegalStateException(error);
        }

        TaxiOrder order = maybeOrder.get();

        List<Runnable> onRefreshStateSuccess = Collections.singletonList(
                () -> this.fireOrderState(sessionUid, orderRepository.findByRemoteId(orderId)
                        .orElseThrow(() -> new IllegalStateException(format(
                                "refresh order with %s was requested, but order not found in db",
                                orderId
                        ))).getState())
        );

        if (!StringUtils.isEmpty(order.getRemoteUuid())) {
            OrderTask findUuidAndState = new OrderTask(
                    order,
                    OrderTask.Task.REFRESH_STATE,
                    onRefreshStateSuccess,
                    Collections.singletonList(() -> new RuntimeException("not implemented"))
            );
            dispatcher.queueTask(findUuidAndState);
        } else {
            OrderTask findUuidAndState = new OrderTask(
                    order,
                    OrderTask.Task.FIND_UUID_AND_REFRESH_STATE,
                    onRefreshStateSuccess,
                    Collections.singletonList(() -> new RuntimeException("not implemented"))
            );
            dispatcher.queueTask(findUuidAndState);
        }
    }

    private void fireOrderState(String sessionUuid, TaxiOrder.State state) {
        List<String> headers = Arrays.asList(
                format("sessionUuid:%s", sessionUuid),
                "messageType:REFRESH_ORDER_STATE=",
                "result:true",
                format("state:%s", state)
        );
        client.sendEvent(headers);
    }

    private void fireOrderNotPresent(String sessionUuid) {
        List<String> headers = Arrays.asList(
                format("sessionUuid:%s", sessionUuid),
                "messageType:REQUEST_ORDER_IN_PROGRESS=",
                "result:false"
        );
        client.sendEvent(headers);
    }

    private void fireOrderPresent(String sessionUuid, TaxiOrder order) {
        if (StringUtils.isEmpty(order.getRemoteId())) {
            throw new IllegalStateException(format(
                    "order id must be defined when present order found; session = %s, %s",
                    sessionUuid,
                    order
            ));
        }
        List<String> headers = Arrays.asList(
                format("sessionUuid:%s", sessionUuid),
                "messageType:REQUEST_ORDER_IN_PROGRESS=",
                "result:true",
                format("orderId:%s", order.getRemoteId())
        );
        client.sendEvent(headers);
    }

    private boolean filterPassed(EslEvent event) {
        Map<String, String> headers = event.getEventHeaders();
        return headers.containsKey("service") && headers.get("service").equals(EVENT_HEADER);
    }

}
