package online.robotaxi.taxi.dto;

import com.fasterxml.jackson.annotation.JsonCreator;

public class DurationOffsetDto {
    private int value;
    private String unit; // name of ChroneUnit enum value

    @JsonCreator
    private DurationOffsetDto() {
    }

    public DurationOffsetDto(int value, String unit) {
        this.value = value;
        this.unit = unit;
    }

    public int getValue() {
        return value;
    }

    public String getUnit() {
        return unit;
    }
}
