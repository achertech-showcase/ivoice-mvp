package online.robotaxi.taxi.processing;

import online.robotaxi.model.TaxiOrder;
import online.robotaxi.repository.TaxiOrderRepository;
import online.robotaxi.taxi.exception.PageFormatChangedException;
import online.robotaxi.taxi.model.OrderTask;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;
import org.springframework.stereotype.Component;
import org.springframework.util.StringUtils;

import static java.lang.String.format;

@Component
public class CancelOrderProcessor extends AbstractProcessor {
    protected CancelOrderProcessor(TaxiOrderRepository taxiOrderRepository) {
        super(taxiOrderRepository);
    }

    @Override
    protected TaxiOrder processInternal(
            WebDriver webDriver,
            OrderTask task
    ) throws PageFormatChangedException, IllegalOrderState {
        TaxiOrder order = task.getOrder();
        if (StringUtils.isEmpty(order.getRemoteUuid())) {
            throw new AbstractProcessor.IllegalOrderState(
                    "order without uuid can't be cancelled: %s", task
            );
        }
        webDriver.get(
                format("https://lk.taximeter.yandex.ru/order/%s/canceled", order.getRemoteUuid())
        );
        findElement(webDriver, By.id("failed")).click();
        WebElement commentElement = findElement(webDriver, By.id("comment"));
        commentElement.sendKeys("отмена клиентом");

        new WebDriverWait(webDriver, 5)
                .until(ExpectedConditions.textToBePresentInElementValue(commentElement, "отмена клиентом"));

        findElement(webDriver, By.id("accept")).click();
        order.setState(TaxiOrder.State.CANCELLED);
        return order;
    }

    @Override
    public OrderTask.Task processingTask() {
        return OrderTask.Task.CANCEL;
    }
}
