package online.robotaxi.taxi.dispatcher;

import online.robotaxi.taxi.Processor;
import online.robotaxi.taxi.SeleniumDispatcher;
import online.robotaxi.taxi.model.OrderTask;
import org.openqa.selenium.WebDriver;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.scheduling.annotation.Scheduled;

import javax.annotation.PostConstruct;
import java.util.List;
import java.util.Map;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.LinkedTransferQueue;
import java.util.concurrent.TransferQueue;
import java.util.function.Function;
import java.util.stream.Collectors;

import static java.lang.String.format;
import static online.robotaxi.taxi.model.OrderTask.Task.REFRESH_DISPATCHER;

public abstract class AbstractOrderSeleniumDispatcher implements SeleniumDispatcher {
    protected final Logger LOG = LoggerFactory.getLogger(getClass());

    private final DispatcherLoginService loginService;
    private final WebDriver webDriver;
    private final Map<OrderTask.Task, Processor> processors;

    private final TransferQueue<OrderTask> taskQueue = new LinkedTransferQueue<>();
    private final ExecutorService queueProcessor = Executors.newSingleThreadExecutor();

    public AbstractOrderSeleniumDispatcher(
            WebDriver webDriver,
            DispatcherLoginService loginService,
            List<Processor> processors
    ) {
        this.webDriver = webDriver;
        this.loginService = loginService;
        this.processors = processors.stream().collect(Collectors.toMap(
                Processor::processingTask,
                Function.identity()
        ));
    }

    @PostConstruct
    public void loginAndInitProcessing() {
        loginService.login(getName(), webDriver);
        queueProcessor.submit(() -> {
            while (true) {
                try {
                    LOG.info("waiting for new task");
                    OrderTask task = taskQueue.take();
                    LOG.info(format("got task: %s", task));
                    process(task);
                } catch (InterruptedException e) {
                    LOG.error(format("unexpected interruption, %s", e.getMessage()));
                    return;
                }
            }
        });
    }

    @Scheduled(fixedDelay = 1000 * 60 * 10)
    public void refresh() {
        OrderTask task = new OrderTask(null, REFRESH_DISPATCHER, null, null);
        queueTask(task);
    }

    @Override
    public void queueTask(OrderTask task) {
        LOG.info(format("queue task: %s", task));
        taskQueue.add(task);
        if (taskQueue.size() > 5) {
            LOG.error(format("too much tasks in queue: %s", taskQueue.size()));
        }
    }

    @Override
    public WebDriver getWebDriver() {
        return webDriver;
    }

    @Override
    public void relogin() {
        LOG.info("relogining");
        loginService.login(getName(), getWebDriver());
    }

    private void process(OrderTask task) {
        if (task.getTask()==REFRESH_DISPATCHER){
            reloadDefaultPage();
            return;
        }
        if (!processors.containsKey(task.getTask())) {
            throw new IllegalStateException(format("processor %s not registered", task.getTask()));
        }
        processors.get(task.getTask()).process(this, task);
    }

    private void reloadDefaultPage() {
        LOG.info("reloading default page");
        webDriver.get(defaultPage());
    }

    protected abstract String defaultPage();
}
