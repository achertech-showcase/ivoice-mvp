package online.robotaxi.taxi.processing;

import com.google.common.base.MoreObjects;
import online.robotaxi.model.TaxiOrder;
import online.robotaxi.repository.TaxiOrderRepository;
import online.robotaxi.taxi.exception.PageFormatChangedException;
import online.robotaxi.taxi.model.OrderTask;
import org.openqa.selenium.By;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;
import org.springframework.stereotype.Component;
import org.springframework.util.StringUtils;

import java.time.format.DateTimeFormatter;
import java.util.List;
import java.util.Map;

import static java.lang.String.format;

@Component
public class PlaceOrderProcessor extends AbstractProcessor {
    public PlaceOrderProcessor(
            TaxiOrderRepository taxiOrderRepository
    ) {
        super(taxiOrderRepository);
    }

    @Override
    protected TaxiOrder processInternal(WebDriver webDriver, OrderTask task) throws PageFormatChangedException {
        JavascriptExecutor javascriptExecutor = (JavascriptExecutor) webDriver;
        if (!webDriver.getCurrentUrl().contains("https://lk.taximeter.yandex.ru/dispatcher/new")) {
            webDriver.get("https://lk.taximeter.yandex.ru/dispatcher/new");
            new WebDriverWait(webDriver, 5)
                    .until(ExpectedConditions.urlToBe("https://lk.taximeter.yandex.ru/dispatcher/new"));
        }

        // handle restore last order screen:
        List<WebElement> maybeRestore = webDriver.findElements(By.id("alert-restore-cancel"));
        if (maybeRestore.size() > 0) {
            WebElement cancelRestore = maybeRestore.get(0);
            if (cancelRestore.isEnabled() && cancelRestore.isDisplayed()) {
                cancelRestore.click();
                new WebDriverWait(webDriver, 5)
                        .until(ExpectedConditions.presenceOfElementLocated(By.id("number")));
            }
        } else {
            new WebDriverWait(webDriver, 5)
                    .until(ExpectedConditions.presenceOfElementLocated(By.id("number")));
        }

        TaxiOrder order = task.getOrder();
        FormattedOrder formattedOrder = new FormattedOrder(order);

        // update order id, just in case
//        findElement(By.cssSelector(".icon-repeat")).click();

        javascriptExecutor.executeScript(format("document.getElementById('date').value='%s'", formattedOrder.date));
        javascriptExecutor.executeScript(format("document.getElementById('time').value='%s'", formattedOrder.time));
        javascriptExecutor.executeScript(format("document.getElementById('phone1').value='%s'", formattedOrder.phone));
        // TODO remove test comments
        findElement(webDriver, By.id("description")).sendKeys("тест! не брать в работу!");
        findElement(webDriver, By.id("name")).sendKeys(order.getPhone());
        if (!StringUtils.isEmpty(order.getComments())) {
            webDriver.findElement(By.id("special-conditions")).sendKeys(order.getComments());
        }

        WebElement street = findElement(webDriver, By.id("AddressFromStreet"));
        street.sendKeys(order.getArrivalAddress().getStreet());

        WebElement house = findElement(webDriver, By.id("AddressFromHouse"));
        house.sendKeys(order.getArrivalAddress().getHouse());

        WebElement fullAddress = findElement(webDriver, By.id("AddressFromDescription"));
        fullAddress.sendKeys(order.getArrivalAddress().getFullAddress());

        // selected options originated from freeswitch script
        Map<String, Integer> selectedOptions = order.getSelectedOptions();
        selectedOptions.forEach((key, value) -> javascriptExecutor.executeScript(format(
                "document.getElementById('%s').selectedIndex=%s",
                key,
                value
        )));

        String orderId = findElement(webDriver, By.id("number")).getAttribute("value");

        findElement(webDriver, By.id("accept")).click();

        LOG.info(format("order placed: %s", orderId));

        return order.placed(orderId);
    }

    @Override
    public OrderTask.Task processingTask() {
        return OrderTask.Task.PLACE;
    }

    static class FormattedOrder {
        private static final DateTimeFormatter DATE_FORMATTER = DateTimeFormatter.ofPattern("yyyy-MM-dd");
        private static final DateTimeFormatter TIME_FORMATTER = DateTimeFormatter.ofPattern("HH:mm");
        private final String phone;
        private final String date;
        private final String time;

        FormattedOrder(TaxiOrder order) {
            if (order.getPhone().length() > 4) {
                this.phone = format("+%s", order.getPhone());
            } else {
                // debug!
                this.phone = "+79771368592";
            }
            if (!phone.startsWith("+7")) {
                throw new IllegalArgumentException(format("not +7 starting phone: %s", order));
            }

            date = DATE_FORMATTER.format(order.zonedArrivalDateTime());
            time = TIME_FORMATTER.format(order.zonedArrivalDateTime());
        }

        @Override
        public String toString() {
            return MoreObjects.toStringHelper(this)
                    .add("phone", phone)
                    .add("date", date)
                    .add("time", time)
                    .toString();
        }
    }
}
