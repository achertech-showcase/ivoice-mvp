package online.robotaxi.taxi;

import online.robotaxi.taxi.exception.PageFormatChangedException;
import online.robotaxi.taxi.model.OrderTask;
import org.openqa.selenium.By;
import org.openqa.selenium.NoSuchElementException;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;

public interface Processor {
    void process(SeleniumDispatcher dispatcher, OrderTask task);

    OrderTask.Task processingTask();

    /**
     * @return web element
     * @throws if page format changed
     */
    default WebElement findElement(WebDriver webDriver, By by) throws PageFormatChangedException {
        try {
            return webDriver.findElement(by);
        } catch (NoSuchElementException e) {
            throw new PageFormatChangedException(webDriver.getCurrentUrl(), by);
        }
    }
}
