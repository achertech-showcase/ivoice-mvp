package online.robotaxi.taxi.exception;

import org.openqa.selenium.By;

import static java.lang.String.format;

public class PageFormatChangedException extends RemoteException {
    private final String currentUrl;
    private final By elementSearchedBy;

    public PageFormatChangedException(String currentUrl, By elementSearchedBy) {
        super(format("element not found; url %s, searched by %s", currentUrl, elementSearchedBy));
        this.currentUrl = currentUrl;
        this.elementSearchedBy = elementSearchedBy;
    }
}
