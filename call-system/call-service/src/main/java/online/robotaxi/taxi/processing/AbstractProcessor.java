package online.robotaxi.taxi.processing;

import com.google.common.base.MoreObjects;
import online.robotaxi.model.TaxiOrder;
import online.robotaxi.repository.TaxiOrderRepository;
import online.robotaxi.taxi.Processor;
import online.robotaxi.taxi.SeleniumDispatcher;
import online.robotaxi.taxi.exception.PageFormatChangedException;
import online.robotaxi.taxi.model.OrderTask;
import org.openqa.selenium.NoSuchSessionException;
import org.openqa.selenium.WebDriver;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import static java.lang.String.format;

public abstract class AbstractProcessor implements Processor {
    protected final Logger LOG = LoggerFactory.getLogger(getClass());
    private final TaxiOrderRepository taxiOrderRepository;

    protected AbstractProcessor(TaxiOrderRepository taxiOrderRepository) {
        this.taxiOrderRepository = taxiOrderRepository;
    }

    @Override
    public void process(SeleniumDispatcher dispatcher, OrderTask task) {
        try {
            TaxiOrder order = task.getOrder();
            order.remoteTaskStarted(task.getTask());

            TaxiOrder updatedOrder = processInternal(dispatcher.getWebDriver(), task);
            taxiOrderRepository.save(updatedOrder.remoteTaskPerformedSuccessfully(task.getTask()));

            task.successfullyPerformed();
        } catch (PageFormatChangedException e) {
            taxiOrderRepository.save(task.getOrder().remoteTaskFailed(task.getTask(), e.getMessage()));
            task.failed();
            throw new IllegalStateException(e);
        } catch (NoSuchSessionException e){
            LOG.warn(format("no session exception, relogining: %s", e.getMessage()));
            dispatcher.relogin();
            process(dispatcher, task);
        } catch (Exception e) {
//            // TODO cancel order
            e.printStackTrace();
            LOG.error(format("exception while processing order task: %s; %s", task, e.getMessage()));
            taxiOrderRepository.save(task.getOrder().remoteTaskFailed(task.getTask(), e.getMessage()));
//            throw new RuntimeException("not implemented");
//            taxiOrderRepository.save(task.getOrder().remoteTaskFailed(task.getTask(), e.getMessage()));
//            task.incAttemptsMade();
//            if (task.hasMoreAttempts()){
//                LOG.info(format("next attempt for task %s", task));
//                dispatcher.queueTask(task);
//            } else {
//
//            }
            throw new RuntimeException("not implemented");
        }
    }

    /**
     * Exit from method means that task performed successfully; task success callbacks will be triggered
     * Method should only update order, but not save! saving performed in AbstractProcessor only
     *
     * @return updated order
     */
    protected abstract TaxiOrder processInternal(
            WebDriver webDriver,
            OrderTask orderTask
    ) throws PageFormatChangedException, IllegalOrderState;

    protected final class IllegalOrderState extends Exception {
        private final OrderTask orderTask;

        public IllegalOrderState(String message, OrderTask orderTask) {
            super(message);
            this.orderTask = orderTask;
        }

        @Override
        public String toString() {
            return MoreObjects.toStringHelper(this)
                    .add("orderTask", orderTask)
                    .toString();
        }
    }
}
