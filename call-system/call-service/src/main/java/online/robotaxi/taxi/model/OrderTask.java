package online.robotaxi.taxi.model;

import com.google.common.base.MoreObjects;
import online.robotaxi.model.TaxiOrder;
import org.apache.commons.lang3.ObjectUtils;

import java.util.Collections;
import java.util.List;

public final class OrderTask {
    private final TaxiOrder order;
    private final Task task;
    private final List<Runnable> successCallbacks;
    private final List<Runnable> failCallbacks;
    private final int repeatAttempts;
    private int attemptsMade = 0;

    public OrderTask(
            TaxiOrder order,
            Task task,
            List<Runnable> successCallbacks,
            List<Runnable> failCallbacks
    ) {
        this(order, task, successCallbacks, failCallbacks, 0);
    }

    @SuppressWarnings("unchecked")
    public OrderTask(
            TaxiOrder order,
            Task task,
            List<Runnable> successCallbacks,
            List<Runnable> failCallbacks,
            int repeatAttempts
    ) {
        this.order = order;
        this.task = task;
        this.successCallbacks = ObjectUtils.firstNonNull(successCallbacks, Collections.emptyList());
        this.failCallbacks = ObjectUtils.firstNonNull(failCallbacks, Collections.emptyList());
        this.repeatAttempts = repeatAttempts;
    }

    public TaxiOrder getOrder() {
        return order;
    }

    public Task getTask() {
        return task;
    }

    public void successfullyPerformed() {
        successCallbacks.forEach(Runnable::run);
    }

    public void failed() {
        failCallbacks.forEach(Runnable::run);
    }

    public int getRepeatAttempts() {
        return repeatAttempts;
    }

    public void incAttemptsMade() {
        this.attemptsMade++;
    }

    @Override
    public String toString() {
        return MoreObjects.toStringHelper(this)
                .add("order", order)
                .add("task", task)
                .toString();
    }

    public enum Task {
        PLACE,
        FIND_UUID_AND_REFRESH_STATE,
        REFRESH_STATE,
        CANCEL,
        REFRESH_DISPATCHER
    }
}


