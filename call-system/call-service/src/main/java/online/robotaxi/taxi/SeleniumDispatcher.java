package online.robotaxi.taxi;

import online.robotaxi.taxi.model.OrderTask;
import org.openqa.selenium.WebDriver;

/**
 * Emulates real dispatcher/
 * Implementation = open browser window;
 * Holds order task queue (orders processed in single thread per dispatcher)
 */
public interface SeleniumDispatcher {
    void queueTask(OrderTask task);

    /**
     * name to identify dispatcher in system
     */
    String getName();

    WebDriver getWebDriver();

    void relogin();
}
