package online.robotaxi.taxi.impl;

import online.robotaxi.model.TaxiOrder;
import online.robotaxi.taxi.OrderPlacingService;
import online.robotaxi.taxi.dispatcher.PlacingOrderDispatcher;
import online.robotaxi.taxi.dispatcher.ProcessingOrderDispatcher;
import online.robotaxi.taxi.model.OrderTask;
import org.springframework.stereotype.Service;

import java.util.Collections;
import java.util.List;

@Service
public class OrderPlacingServiceImpl implements OrderPlacingService {

    private final PlacingOrderDispatcher placingOrderDispatcher;
    private final ProcessingOrderDispatcher processingOrderDispatcher;

    public OrderPlacingServiceImpl(
            PlacingOrderDispatcher placingOrderDispatcher,
            ProcessingOrderDispatcher processingOrderDispatcher
    ) {
        this.placingOrderDispatcher = placingOrderDispatcher;
        this.processingOrderDispatcher = processingOrderDispatcher;
    }

    @Override
    public void place(
            TaxiOrder order, Runnable placedCallback, Runnable failedToPlaceCallback
    ) {
        OrderTask findUuidTask = new OrderTask(order, OrderTask.Task.FIND_UUID_AND_REFRESH_STATE, null, null);

//        List<Runnable> orderPlacedCallbacks = Arrays.asList(
//                placedCallback,
//                () -> processingOrderDispatcher.queueTask(findUuidTask)
//        );
        List<Runnable> orderPlacedCallbacks = Collections.singletonList(placedCallback);
        OrderTask task = new OrderTask(
                order,
                OrderTask.Task.PLACE,
                orderPlacedCallbacks,
                Collections.singletonList(failedToPlaceCallback)
        );
        placingOrderDispatcher.queueTask(task);
    }
}
