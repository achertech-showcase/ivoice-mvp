package online.robotaxi.taxi.dto;

import com.fasterxml.jackson.annotation.JsonCreator;

import java.time.ZonedDateTime;
import java.time.temporal.ChronoUnit;
import java.util.Map;

public class OrderDto {
    private String callSessionUuid;
    private String phone;
    private String timeZoneOffset;
    private DurationOffsetDto arrivalTimeOffset;
    private AddressDto arrivalAddress;
    private String comments;
    private Map<String, Integer> selectedOptions;

    @JsonCreator
    private OrderDto() {
    }

    public OrderDto(
            String phone,
            String timeZoneOffset,
            DurationOffsetDto arrivalTimeOffset,
            AddressDto arrivalAddress,
            String comments,
            Map<String, Integer> selectedOptions
    ) {
        this.phone = phone;
        this.timeZoneOffset = timeZoneOffset;
        this.arrivalTimeOffset = arrivalTimeOffset;
        this.arrivalAddress = arrivalAddress;
        this.comments = comments;
        this.selectedOptions = selectedOptions;
    }

    public AddressDto getArrivalAddress() {
        return arrivalAddress;
    }

    public void setArrivalAddress(AddressDto arrivalAddress) {
        this.arrivalAddress = arrivalAddress;
    }

    public String getComments() {
        return comments;
    }

    public void setComments(String comments) {
        this.comments = comments;
    }

    public String getPhone() {
        return phone;
    }

    public void setPhone(String phone) {
        this.phone = phone;
    }

    public String getTimeZoneOffset() {
        return timeZoneOffset;
    }

    public DurationOffsetDto getArrivalTimeOffset() {
        return arrivalTimeOffset;
    }

    public Map<String, Integer> getSelectedOptions() {
        return selectedOptions;
    }

    public ZonedDateTime findArrivalTime(ZonedDateTime orderCreationTime) {
        return orderCreationTime.plus(
                arrivalTimeOffset.getValue(),
                ChronoUnit.valueOf(arrivalTimeOffset.getUnit())
        );
    }
}
