package online.robotaxi.taxi.exception;

/**
 * Can't place order because of some remote issues that can't be handled automatically
 */
public class RemoteException extends Exception {
    public RemoteException(String message) {
        super(message);
    }
}
