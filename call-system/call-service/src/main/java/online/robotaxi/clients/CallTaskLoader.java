package online.robotaxi.clients;

import online.robotaxi.model.CallTask;
import online.robotaxi.repository.CallTaskRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Profile;
import org.springframework.stereotype.Component;

import javax.annotation.PostConstruct;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.HashSet;
import java.util.List;
import java.util.Optional;
import java.util.Set;
import java.util.stream.Collectors;
import java.util.stream.Stream;

import static java.lang.String.format;

//@Profile("phoneloader")
//@Component
public class CallTaskLoader {
    private static final String PHONES_PARENT = "/home/artyom/projects/my/robotaxi/clients/";

    private static final Set<Character> NOT_PHONE_CHARS;

    static {
        NOT_PHONE_CHARS = new HashSet<>();
        NOT_PHONE_CHARS.add('+');
        NOT_PHONE_CHARS.add('-');
        NOT_PHONE_CHARS.add('(');
        NOT_PHONE_CHARS.add(')');
    }

    private final String client = "drivers";
    private final String campaign = "voronezh1";
    private final String fullCampaign = client + campaign;

    private final CallTaskRepository callTaskRepository;

    @Autowired
    public CallTaskLoader(CallTaskRepository callTaskRepository) {
        this.callTaskRepository = callTaskRepository;
    }

    @PostConstruct
    public void init() {
        Path path = Paths.get(PHONES_PARENT).resolve(client).resolve(format("%s.csv", campaign));
        if (!Files.exists(path)) {
            throw new IllegalStateException();
        }

        callTaskRepository.deleteAllByCampaign(format("%s%s", client, campaign));
        List<String> phones;
        try (Stream<String> str = Files.lines(path)) {
            phones = str
                    .map(this::removeChars)
                    .map(this::phone)
                    .collect(Collectors.toList());

        } catch (IOException e) {
            throw new IllegalStateException(e);
        }

        Optional<String> maybeWrongPrefix = phones.stream().filter(p -> !p.startsWith("7")).findAny();
        if (maybeWrongPrefix.isPresent()) {
            throw new IllegalStateException();
        }
        callTaskRepository.deleteAllByCampaign(fullCampaign);

        phones.forEach(p -> {
            CallTask ct = new CallTask(fullCampaign, p);
            callTaskRepository.save(ct);
        });
    }


    private String removeChars(String orginal) {
        StringBuilder sb = new StringBuilder();
        char[] chars = orginal.toCharArray();
        for (int i = 0; i < orginal.length(); i++) {
            char c = chars[i];
            if (c >= 48 && c <= 57) {
                sb.append(c);
            }
        }
        return sb.toString();
    }

    private String phone(String str) {
        if (str.startsWith("385")) {
            return "7" + str;
        }
        if (str.length() < 8) {
            return "73852" + str;
        }

        if (str.startsWith("8")) {
            return "7" + str.substring(1, str.length());
        }
        if (str.startsWith("9")) {
            return "7" + str;
        }
        return str;
    }

}
