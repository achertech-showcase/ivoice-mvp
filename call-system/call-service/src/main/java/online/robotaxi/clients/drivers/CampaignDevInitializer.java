package online.robotaxi.clients.drivers;

import online.robotaxi.coldcall.repeat.RepeatOptions;
import online.robotaxi.coldcall.repeat.RetryWithIntervalOptions;
import online.robotaxi.model.Campaign;
import online.robotaxi.model.CampaignSchedule;
import online.robotaxi.model.Interval;
import online.robotaxi.repository.CampaignRepository;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import javax.annotation.PostConstruct;
import java.time.temporal.ChronoUnit;
import java.util.Collections;

//@Component(value = "drivers-dev-campaigns-initializer")
public class CampaignDevInitializer {
    private static final Logger LOG = LoggerFactory.getLogger(CampaignDevInitializer.class);
    private final CampaignRepository campaignRepository;

    @Autowired
    public CampaignDevInitializer(CampaignRepository campaignRepository) {
        this.campaignRepository = campaignRepository;
    }

    @PostConstruct
    public void init() {
        campaignRepository.deleteAllByName("drivers-dev");

        RepeatOptions repeatOptions = new RetryWithIntervalOptions(
                3,
                30,
                1,
                120,
                ChronoUnit.MINUTES
        );

        Interval interval = new Interval(10, 16);
        CampaignSchedule schedule = new CampaignSchedule(
                "+03",
                Collections.singletonList(interval),
                repeatOptions
        );
        Campaign campaign = new Campaign("drivers-dev", "drivers-dev", "ours", 20, schedule);
        campaignRepository.save(campaign);
    }
}
