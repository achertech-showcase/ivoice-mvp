package online.robotaxi.clients.kupofon;

import online.robotaxi.coldcall.repeat.RepeatOptions;
import online.robotaxi.coldcall.repeat.RetryWithIntervalOptions;
import online.robotaxi.model.Campaign;
import online.robotaxi.model.CampaignSchedule;
import online.robotaxi.model.Interval;
import online.robotaxi.repository.CampaignRepository;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import javax.annotation.PostConstruct;
import java.time.temporal.ChronoUnit;
import java.util.Collections;
import java.util.Optional;

//@Component
public class CampaignInitializer {
    private static final Logger LOG = LoggerFactory.getLogger(CampaignInitializer.class);
    private final CampaignRepository campaignRepository;

    @Autowired
    public CampaignInitializer(CampaignRepository campaignRepository) {
        this.campaignRepository = campaignRepository;
    }

//    @PostConstruct
//    public void init() {
//        Optional<Campaign> maybeCampaign = campaignRepository.findOneByName("kupofon2");
//        if (maybeCampaign.isPresent()) {
//            return;
//        }
//
//        // TODO return 4 attempts
//        RepeatOptions repeatOptions = new RetryWithIntervalOptions(4, 30, ChronoUnit.MINUTES);
//
//        Interval interval = new Interval(10, 16);
//        CampaignSchedule schedule = new CampaignSchedule(
//                "+03",
//                Collections.singletonList(interval),
//                repeatOptions
//        );
//        Campaign campaign = new Campaign("kupofon2", "kupofon", "kupofon", 20, schedule);
//        LOG.info("saving %s", campaign);
//        campaignRepository.save(campaign);
//    }
}
