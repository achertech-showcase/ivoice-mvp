package online.robotaxi.clients.nils;

import com.google.common.base.MoreObjects;
import online.robotaxi.coldcall.repeat.RepeatOptions;
import online.robotaxi.coldcall.repeat.RetryWithIntervalOptions;
import online.robotaxi.model.CallTask;
import online.robotaxi.model.Campaign;
import online.robotaxi.model.CampaignSchedule;
import online.robotaxi.model.Interval;
import online.robotaxi.repository.CallTaskRepository;
import online.robotaxi.repository.CampaignRepository;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.context.annotation.Profile;
import org.springframework.stereotype.Component;
import org.springframework.util.StringUtils;

import javax.annotation.PostConstruct;
import java.io.IOException;
import java.nio.charset.Charset;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.time.temporal.ChronoUnit;
import java.util.Collections;
import java.util.HashSet;
import java.util.List;
import java.util.Set;
import java.util.stream.Collectors;
import java.util.stream.Stream;

import static java.lang.String.format;

@Profile("campaignInit")
@Component
public class CampaignInitializer {
    private static final Logger LOG = LoggerFactory.getLogger(CampaignInitializer.class);
    private static final String PHONES_PARENT = "/home/artyom/projects/my/robotaxi/clients/";

    private static final Set<Character> NOT_PHONE_CHARS;

    static {
        NOT_PHONE_CHARS = new HashSet<>();
        NOT_PHONE_CHARS.add('+');
        NOT_PHONE_CHARS.add('-');
        NOT_PHONE_CHARS.add('(');
        NOT_PHONE_CHARS.add(')');
    }

    private final String phonesFile = "samara.csv";

    private final String client = "nils";

    private final String campaignMini = "samara11";
    private final String fullCampaignNameMini = format("%s_%s", client, campaignMini);

    private final String campaign = "samara12";
    private final String fullCampaignName = format("%s_%s", client, campaign);
    private final String script = "clients/nils/nils1";

    private final CallTaskRepository callTaskRepository;
    private final CampaignRepository campaignRepository;

    public CampaignInitializer(
            CallTaskRepository callTaskRepository,
            CampaignRepository campaignRepository
    ) {
        this.callTaskRepository = callTaskRepository;
        this.campaignRepository = campaignRepository;
    }

    @PostConstruct
    public void init() {
        initCampaign(fullCampaignNameMini);
        initCampaign(fullCampaignName);

        List<OriginalPhone> phones = parsePhones();
        initCallTasks(fullCampaignNameMini, phones, 0, 300);
        initCallTasks(fullCampaignName, phones, 300);

        LOG.info("campaigns init finished");
    }

    private List<OriginalPhone> parsePhones() {
        Path path = Paths.get(PHONES_PARENT).resolve(client).resolve(phonesFile);
        if (!Files.exists(path)) {
            throw new IllegalStateException();
        }

        callTaskRepository.deleteAllByCampaign(fullCampaignName);
        List<OriginalPhone> phones = parsePhones(path);

        String emptyPhones = phones.stream().filter(p -> p.phone.isEmpty()).map(p -> p.id).collect(Collectors.joining(
                "\r\n"));
        LOG.warn("empty phones: \r\n" + emptyPhones);

        return phones
                .stream()
                .filter(p -> !StringUtils.isEmpty(p.phone))
                .collect(Collectors.toList());
    }

    private void initCallTasks(String campaign, List<OriginalPhone> phones, int start) {
        initCallTasks(campaign, phones, start, phones.size());
    }

    private void initCallTasks(String campaign, List<OriginalPhone> phones, int start, int end) {
        List<OriginalPhone> phonesSublist = phones.subList(start, end);

        phonesSublist
                .stream()
                .forEach(p -> {
                    CallTask ct = new CallTask(campaign, p.phone);
                    ct.setOriginalDbId(p.id);
                    callTaskRepository.save(ct);
                });
    }

    private List<OriginalPhone> parsePhones(Path path) {
        try (Stream<String> strings = Files.lines(path, Charset.forName("Cp1251"))) {
            return strings
                    .map(OriginalPhone::new)
                    .collect(Collectors.toList());
        } catch (IOException e) {
            throw new IllegalStateException(e);
        }
    }

    private static class OriginalPhone {
        final String id;
        final String phone;

        public OriginalPhone(String originalString) {
            String[] split = originalString.split(";");
            this.id = removeChars(split[0]);
            String originalPhone = removeChars(split[12]);
            if (originalPhone.isEmpty()) {
                originalPhone = removeChars(split[13]);
            }
            if (originalPhone.isEmpty()) {
                originalPhone = removeChars(split[14]);
            }

            if (StringUtils.isEmpty(id)) {
                throw new IllegalArgumentException(format("id for: %s", this));
            }
//            if (StringUtils.isEmpty(originalPhone)) {
//                throw new IllegalArgumentException(format("phone for %s", this));
//            }

            this.phone = substitute8800(originalPhone);
            if (this.phone.isEmpty()) {
                throw new IllegalStateException("empty phone for" + id);
            }
            if (this.phone.length() < 9) {
                throw new IllegalStateException(format("too short phone %s for id %s", phone, id));
            }
        }

        @Override
        public String toString() {
            return MoreObjects.toStringHelper(this)
                    .add("id", id)
                    .add("phone", phone)
                    .toString();
        }
    }

    private static String substitute8800(String withoutChars) {
        if (withoutChars.startsWith("8800")) {
            return withoutChars.replaceFirst("8", "7");
        } else {
            return withoutChars;
        }
    }

    private void initCampaign(String campaignName) {
        campaignRepository.deleteAllByName(campaignName);

        RepeatOptions repeatOptions = new RetryWithIntervalOptions(
                3,
                30,
                1,
                120,
                ChronoUnit.MINUTES
        );

        Interval interval = new Interval(10, 16);
        CampaignSchedule schedule = new CampaignSchedule(
                "+03",
                Collections.singletonList(interval),
                repeatOptions
        );
        Campaign campaign = new Campaign(campaignName, script, client, 20, schedule);
        campaignRepository.save(campaign);

        callTaskRepository.deleteAllByCampaign(campaignName);
    }

    private static String removeChars(String orginal) {
        StringBuilder sb = new StringBuilder();
        char[] chars = orginal.toCharArray();
        for (int i = 0; i < orginal.length(); i++) {
            char c = chars[i];
            if (c >= 48 && c <= 57) {
                sb.append(c);
            }
        }
        return sb.toString();
    }

    private String phone(String str) {
        if (str.startsWith("385")) {
            return "7" + str;
        }
        if (str.length() < 8) {
            return "73852" + str;
        }

        if (str.startsWith("8")) {
            return "7" + str.substring(1, str.length());
        }
        if (str.startsWith("9")) {
            return "7" + str;
        }
        return str;
    }

}
