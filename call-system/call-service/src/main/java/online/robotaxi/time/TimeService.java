package online.robotaxi.time;

import java.time.Instant;
import java.time.LocalDateTime;
import java.time.ZoneId;

public interface TimeService {
    static long toEpochSeconds(LocalDateTime ldt) {
        return ldt.atZone(ZoneId.systemDefault()).toEpochSecond();
    }

    static long localDtNowSinceEpochSeconds() {
        return Instant.now().atZone(ZoneId.systemDefault()).toEpochSecond();
    }

    static LocalDateTime fromSecondsFromEpoch(long secondsSinceEpoch) {
        return Instant.ofEpochSecond(secondsSinceEpoch).atZone(ZoneId.systemDefault()).toLocalDateTime();
    }
}
