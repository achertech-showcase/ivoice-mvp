package online.robotaxi.time;

import java.time.LocalDateTime;

public interface SystemTimeProvider {
    LocalDateTime now();
}
