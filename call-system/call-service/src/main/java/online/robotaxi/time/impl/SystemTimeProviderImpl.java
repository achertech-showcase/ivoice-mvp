package online.robotaxi.time.impl;

import online.robotaxi.time.SystemTimeProvider;
import org.springframework.stereotype.Service;

import java.time.LocalDateTime;

@Service
public class SystemTimeProviderImpl implements SystemTimeProvider {
    @Override
    public LocalDateTime now() {
        return LocalDateTime.now();
    }
}
