
package online.robotaxi.map.yandex.dto;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;
import com.google.common.base.MoreObjects;

@JsonInclude(JsonInclude.Include.NON_NULL)
@JsonPropertyOrder({
        "PremiseNumber",
        "PostalCode"
})
public class Premise {

    @JsonProperty("PremiseNumber")
    private String premiseNumber;
    @JsonProperty("PostalCode")
    private PostalCode postalCode;

    @JsonProperty("PremiseNumber")
    public String getPremiseNumber() {
        return premiseNumber;
    }

    @JsonProperty("PremiseNumber")
    public void setPremiseNumber(String premiseNumber) {
        this.premiseNumber = premiseNumber;
    }

    @JsonProperty("PostalCode")
    public PostalCode getPostalCode() {
        return postalCode;
    }

    @JsonProperty("PostalCode")
    public void setPostalCode(PostalCode postalCode) {
        this.postalCode = postalCode;
    }

    @Override
    public String toString() {
        return MoreObjects.toStringHelper(this)
                .add("premiseNumber", premiseNumber)
                .add("postalCode", postalCode)
                .toString();
    }
}
