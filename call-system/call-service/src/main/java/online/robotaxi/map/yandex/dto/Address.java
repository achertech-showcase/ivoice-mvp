
package online.robotaxi.map.yandex.dto;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;
import com.google.common.base.MoreObjects;

import java.util.List;

@JsonInclude(JsonInclude.Include.NON_NULL)
@JsonPropertyOrder({
        "country_code",
        "postal_code",
        "formatted",
        "Components"
})
public class Address {

    @JsonProperty("country_code")
    private String countryCode;
    @JsonProperty("postal_code")
    private String postalCode;
    @JsonProperty("formatted")
    private String formatted;
    @JsonProperty("Components")
    private List<Component> components = null;

    @JsonProperty("country_code")
    public String getCountryCode() {
        return countryCode;
    }

    @JsonProperty("country_code")
    public void setCountryCode(String countryCode) {
        this.countryCode = countryCode;
    }

    @JsonProperty("postal_code")
    public String getPostalCode() {
        return postalCode;
    }

    @JsonProperty("postal_code")
    public void setPostalCode(String postalCode) {
        this.postalCode = postalCode;
    }

    @JsonProperty("formatted")
    public String getFormatted() {
        return formatted;
    }

    @JsonProperty("formatted")
    public void setFormatted(String formatted) {
        this.formatted = formatted;
    }

    @JsonProperty("Components")
    public List<Component> getComponents() {
        return components;
    }

    @JsonProperty("Components")
    public void setComponents(List<Component> components) {
        this.components = components;
    }

    @Override
    public String toString() {
        return MoreObjects.toStringHelper(this)
                .add("countryCode", countryCode)
                .add("postalCode", postalCode)
                .add("formatted", formatted)
                .add("components", components)
                .toString();
    }
}
