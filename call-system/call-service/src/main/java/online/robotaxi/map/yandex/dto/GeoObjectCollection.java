
package online.robotaxi.map.yandex.dto;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;
import com.google.common.base.MoreObjects;

import java.util.List;

@JsonInclude(JsonInclude.Include.NON_NULL)
@JsonPropertyOrder({
        "metaDataProperty",
        "featureMember"
})
public class GeoObjectCollection {

    @JsonProperty("metaDataProperty")
    private MetaDataProperty metaDataProperty;
    @JsonProperty("featureMember")
    private List<FeatureMember> featureMember = null;

    @JsonProperty("metaDataProperty")
    public MetaDataProperty getMetaDataProperty() {
        return metaDataProperty;
    }

    @JsonProperty("metaDataProperty")
    public void setMetaDataProperty(MetaDataProperty metaDataProperty) {
        this.metaDataProperty = metaDataProperty;
    }

    @JsonProperty("featureMember")
    public List<FeatureMember> getFeatureMember() {
        return featureMember;
    }

    @JsonProperty("featureMember")
    public void setFeatureMember(List<FeatureMember> featureMember) {
        this.featureMember = featureMember;
    }

    @Override
    public String toString() {
        return MoreObjects.toStringHelper(this)
                .add("metaDataProperty", metaDataProperty)
                .add("featureMember", featureMember)
                .toString();
    }
}
