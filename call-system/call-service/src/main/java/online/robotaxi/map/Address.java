package online.robotaxi.map;

import com.google.common.base.MoreObjects;

public final class Address {
    private final String country;
    private final String city;

    // not sure, maybe optional for cases like metro, maybe not
    private final String street;
    private final String house;

    private final String fullAddress;

    public Address(String country, String city, String street, String house, String fullAddress) {
        this.country = country;
        this.city = city;
        this.street = street;
        this.house = house;
        this.fullAddress = fullAddress;
    }

    public String getCountry() {
        return country;
    }

    public String getCity() {
        return city;
    }

    public String getStreet() {
        return street;
    }

    public String getHouse() {
        return house;
    }

    @Override
    public String toString() {
        return MoreObjects.toStringHelper(this)
                .add("country", country)
                .add("city", city)
                .add("street", street)
                .add("house", house)
                .add("fullAddress", fullAddress)
                .toString();
    }

    public String getFullAddress() {
        return fullAddress;
    }
}
