
package online.robotaxi.map.yandex.dto;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;
import com.google.common.base.MoreObjects;

@JsonInclude(JsonInclude.Include.NON_NULL)
@JsonPropertyOrder({
        "GeoObjectCollection"
})
public class Response {

    @JsonProperty("GeoObjectCollection")
    private GeoObjectCollection geoObjectCollection;

    @JsonProperty("GeoObjectCollection")
    public GeoObjectCollection getGeoObjectCollection() {
        return geoObjectCollection;
    }

    @JsonProperty("GeoObjectCollection")
    public void setGeoObjectCollection(GeoObjectCollection geoObjectCollection) {
        this.geoObjectCollection = geoObjectCollection;
    }

    @Override
    public String toString() {
        return MoreObjects.toStringHelper(this)
                .add("geoObjectCollection", geoObjectCollection)
                .toString();
    }
}
