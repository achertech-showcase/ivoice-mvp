
package online.robotaxi.map.yandex.dto;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;
import com.google.common.base.MoreObjects;

@JsonInclude(JsonInclude.Include.NON_NULL)
@JsonPropertyOrder({
        "lowerCorner",
        "upperCorner"
})
public class Envelope {

    @JsonProperty("lowerCorner")
    private String lowerCorner;
    @JsonProperty("upperCorner")
    private String upperCorner;

    @JsonProperty("lowerCorner")
    public String getLowerCorner() {
        return lowerCorner;
    }

    @JsonProperty("lowerCorner")
    public void setLowerCorner(String lowerCorner) {
        this.lowerCorner = lowerCorner;
    }

    @JsonProperty("upperCorner")
    public String getUpperCorner() {
        return upperCorner;
    }

    @JsonProperty("upperCorner")
    public void setUpperCorner(String upperCorner) {
        this.upperCorner = upperCorner;
    }

    @Override
    public String toString() {
        return MoreObjects.toStringHelper(this)
                .add("lowerCorner", lowerCorner)
                .add("upperCorner", upperCorner)
                .toString();
    }
}
