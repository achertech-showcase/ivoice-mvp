
package online.robotaxi.map.yandex.dto;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;
import com.google.common.base.MoreObjects;

@JsonInclude(JsonInclude.Include.NON_NULL)
@JsonPropertyOrder({
        "Envelope"
})
public class BoundedBy {

    @JsonProperty("Envelope")
    private Envelope envelope;

    @JsonProperty("Envelope")
    public Envelope getEnvelope() {
        return envelope;
    }

    @JsonProperty("Envelope")
    public void setEnvelope(Envelope envelope) {
        this.envelope = envelope;
    }

    @Override
    public String toString() {
        return MoreObjects.toStringHelper(this)
                .add("envelope", envelope)
                .toString();
    }
}
