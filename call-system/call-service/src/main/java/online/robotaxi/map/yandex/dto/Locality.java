
package online.robotaxi.map.yandex.dto;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;
import com.google.common.base.MoreObjects;

@JsonInclude(JsonInclude.Include.NON_NULL)
@JsonPropertyOrder({
        "LocalityName",
        "Thoroughfare"
})
public class Locality {

    @JsonProperty("LocalityName")
    private String localityName;
    @JsonProperty("Thoroughfare")
    private Thoroughfare thoroughfare;

    @JsonProperty("LocalityName")
    public String getLocalityName() {
        return localityName;
    }

    @JsonProperty("LocalityName")
    public void setLocalityName(String localityName) {
        this.localityName = localityName;
    }

    @JsonProperty("Thoroughfare")
    public Thoroughfare getThoroughfare() {
        return thoroughfare;
    }

    @JsonProperty("Thoroughfare")
    public void setThoroughfare(Thoroughfare thoroughfare) {
        this.thoroughfare = thoroughfare;
    }

    @Override
    public String toString() {
        return MoreObjects.toStringHelper(this)
                .add("localityName", localityName)
                .add("thoroughfare", thoroughfare)
                .toString();
    }
}
