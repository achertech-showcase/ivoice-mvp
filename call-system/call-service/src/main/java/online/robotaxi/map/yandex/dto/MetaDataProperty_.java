
package online.robotaxi.map.yandex.dto;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;
import com.google.common.base.MoreObjects;

@JsonInclude(JsonInclude.Include.NON_NULL)
@JsonPropertyOrder({
        "GeocoderMetaData"
})
public class MetaDataProperty_ {

    @JsonProperty("GeocoderMetaData")
    private GeocoderMetaData geocoderMetaData;

    @JsonProperty("GeocoderMetaData")
    public GeocoderMetaData getGeocoderMetaData() {
        return geocoderMetaData;
    }

    @JsonProperty("GeocoderMetaData")
    public void setGeocoderMetaData(GeocoderMetaData geocoderMetaData) {
        this.geocoderMetaData = geocoderMetaData;
    }

    @Override
    public String toString() {
        return MoreObjects.toStringHelper(this)
                .add("geocoderMetaData", geocoderMetaData)
                .toString();
    }
}
