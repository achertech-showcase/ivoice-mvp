package online.robotaxi.map;

import java.util.Optional;

public interface MapService {
    /**
     * @return empty if address not found, or ambiguous results
     */
    Optional<Address> findMapAddress(String city, String addressQuery);
}
