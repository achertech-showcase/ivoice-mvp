
package online.robotaxi.map.yandex.dto;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;
import com.google.common.base.MoreObjects;

@JsonInclude(JsonInclude.Include.NON_NULL)
@JsonPropertyOrder({
        "PostalCodeNumber"
})
public class PostalCode {

    @JsonProperty("PostalCodeNumber")
    private String postalCodeNumber;

    @JsonProperty("PostalCodeNumber")
    public String getPostalCodeNumber() {
        return postalCodeNumber;
    }

    @JsonProperty("PostalCodeNumber")
    public void setPostalCodeNumber(String postalCodeNumber) {
        this.postalCodeNumber = postalCodeNumber;
    }

    @Override
    public String toString() {
        return MoreObjects.toStringHelper(this)
                .add("postalCodeNumber", postalCodeNumber)
                .toString();
    }
}
