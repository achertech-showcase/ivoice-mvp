package online.robotaxi.map.yandex;

import online.robotaxi.config.YandexMapsProperties;
import online.robotaxi.map.Address;
import online.robotaxi.map.MapService;
import online.robotaxi.map.yandex.dto.*;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;
import org.springframework.web.client.RestClientException;
import org.springframework.web.client.RestTemplate;
import org.springframework.web.util.UriComponentsBuilder;

import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Optional;
import java.util.function.Function;
import java.util.stream.Collectors;

import static java.lang.String.format;

@Service
public class MapServiceYandexImpl implements MapService {
    private static final Logger LOG = LoggerFactory.getLogger(MapServiceYandexImpl.class);

    private final RestTemplate restClient;
    private final String searchMethodUri;

    @Autowired
    MapServiceYandexImpl(RestTemplate restClient, YandexMapsProperties properties) {
        this.restClient = restClient;

        searchMethodUri = properties.getUrl() + "/";
    }

    @Override
    public Optional<online.robotaxi.map.Address> findMapAddress(String city, String addressQuery) {
        String geocodeWithCity = format("%s,%s", city, addressQuery);
        Optional<YandexMapSearchResponseDto> maybeResponse = apiSearch(geocodeWithCity);
        if (!maybeResponse.isPresent()) {
            return Optional.empty();
        }
        YandexMapSearchResponseDto response = maybeResponse.get();
        LOG.debug(format("%s for address %s", response, addressQuery));
        return mapAddress(response);
    }

    private Optional<Address> mapAddress(YandexMapSearchResponseDto searchResult) {
        List<FeatureMember> featured = searchResult.getResponse().getGeoObjectCollection().getFeatureMember();
        if (featured.isEmpty()) {
            return Optional.empty();
        }
        if (featured.size() > 1) {
            LOG.warn(format("more then 1 featured address: %s", searchResult));
        }
        FeatureMember firstFeatured = featured.get(0);
        GeocoderMetaData meta = firstFeatured.getGeoObject().getMetaDataProperty().getGeocoderMetaData();
        if (!meta.getPrecision().equals("exact")) {
            LOG.warn(format("not exact precision: %s", searchResult));
            return Optional.empty();
        }
        Country country = meta.getAddressDetails().getCountry();
        boolean russia = country.getCountryNameCode().equals("RU") && country.getCountryName().equals("Россия");
        if (!russia) {
            LOG.warn(format("not russia: %s", searchResult));
            return Optional.empty();
        }

        Map<AddressComponent, Component> components = meta.getAddress().getComponents().stream()
                .filter(c -> AddressComponent.contains(c.getKind()))
                .collect(
                        Collectors.toMap(
                                c -> AddressComponent.from(c.getKind()),
                                Function.identity()
                        )
                );

        if (!components.containsKey(AddressComponent.CITY)) {
            LOG.warn(format("city not found %s", searchResult));
            return Optional.empty();
        }

        Address address = new Address(
                country.getCountryName(),
                components.get(AddressComponent.CITY).getName(),
                components.get(AddressComponent.STREET).getName(),
                components.get(AddressComponent.HOUSE).getName(),
                meta.getAddress().getFormatted()
        );
        return Optional.of(address);
    }

    private Optional<YandexMapSearchResponseDto> apiSearch(String addressQuery) {
        try {
            UriComponentsBuilder builder = UriComponentsBuilder.fromUriString(searchMethodUri)
                    .queryParam("geocode", addressQuery)
                    .queryParam("format", "json")
                    .queryParam("lang", "ru_RU");

            ResponseEntity<YandexMapSearchResponseDto> responseEntity = restClient.getForEntity(
                    builder.build().encode().toUri(),
                    YandexMapSearchResponseDto.class
            );
            if (responseEntity.getStatusCode() != HttpStatus.OK) {
                LOG.error(format("%s for address %s", responseEntity, addressQuery));
                return Optional.empty();
            }
            return Optional.of(responseEntity.getBody());
        } catch (RestClientException e) {
            LOG.error(e.getMessage());
            return Optional.empty();
        }
    }

    private enum AddressComponent {
        CITY("locality"),
        STREET("street"),
        HOUSE("house");

        private static Map<String, AddressComponent> COMPONENT_BY_KIND;

        static {
            COMPONENT_BY_KIND = new HashMap<>();
            COMPONENT_BY_KIND.put(CITY.kind, CITY);
            COMPONENT_BY_KIND.put(STREET.kind, STREET);
            COMPONENT_BY_KIND.put(HOUSE.kind, HOUSE);
            if (COMPONENT_BY_KIND.size() != AddressComponent.values().length) {
                throw new IllegalStateException();
            }
        }

        private final String kind;

        AddressComponent(String kind) {
            this.kind = kind;
        }

        static boolean contains(String kind) {
            return COMPONENT_BY_KIND.keySet().contains(kind);
        }

        static AddressComponent from(String kind) {
            return COMPONENT_BY_KIND.get(kind);
        }
    }
}
