
package online.robotaxi.map.yandex.dto;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;
import com.google.common.base.MoreObjects;

@JsonInclude(JsonInclude.Include.NON_NULL)
@JsonPropertyOrder({
        "GeoObject"
})
public class FeatureMember {

    @JsonProperty("GeoObject")
    private GeoObject geoObject;

    @JsonProperty("GeoObject")
    public GeoObject getGeoObject() {
        return geoObject;
    }

    @JsonProperty("GeoObject")
    public void setGeoObject(GeoObject geoObject) {
        this.geoObject = geoObject;
    }

    @Override
    public String toString() {
        return MoreObjects.toStringHelper(this)
                .add("geoObject", geoObject)
                .toString();
    }
}
