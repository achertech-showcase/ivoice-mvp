
package online.robotaxi.map.yandex.dto;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;
import com.google.common.base.MoreObjects;

@JsonInclude(JsonInclude.Include.NON_NULL)
@JsonPropertyOrder({
        "request",
        "found",
        "results"
})
public class GeocoderResponseMetaData {

    @JsonProperty("request")
    private String request;
    @JsonProperty("found")
    private String found;
    @JsonProperty("results")
    private String results;

    @JsonProperty("request")
    public String getRequest() {
        return request;
    }

    @JsonProperty("request")
    public void setRequest(String request) {
        this.request = request;
    }

    @JsonProperty("found")
    public String getFound() {
        return found;
    }

    @JsonProperty("found")
    public void setFound(String found) {
        this.found = found;
    }

    @JsonProperty("results")
    public String getResults() {
        return results;
    }

    @JsonProperty("results")
    public void setResults(String results) {
        this.results = results;
    }

    @Override
    public String toString() {
        return MoreObjects.toStringHelper(this)
                .add("request", request)
                .add("found", found)
                .add("results", results)
                .toString();
    }
}
