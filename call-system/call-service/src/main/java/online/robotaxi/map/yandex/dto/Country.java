
package online.robotaxi.map.yandex.dto;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;
import com.google.common.base.MoreObjects;

@JsonInclude(JsonInclude.Include.NON_NULL)
@JsonPropertyOrder({
        "AddressLine",
        "CountryNameCode",
        "CountryName",
        "AdministrativeArea"
})
public class Country {

    @JsonProperty("AddressLine")
    private String addressLine;
    @JsonProperty("CountryNameCode")
    private String countryNameCode;
    @JsonProperty("CountryName")
    private String countryName;
    @JsonProperty("AdministrativeArea")
    private AdministrativeArea administrativeArea;

    @JsonProperty("AddressLine")
    public String getAddressLine() {
        return addressLine;
    }

    @JsonProperty("AddressLine")
    public void setAddressLine(String addressLine) {
        this.addressLine = addressLine;
    }

    @JsonProperty("CountryNameCode")
    public String getCountryNameCode() {
        return countryNameCode;
    }

    @JsonProperty("CountryNameCode")
    public void setCountryNameCode(String countryNameCode) {
        this.countryNameCode = countryNameCode;
    }

    @JsonProperty("CountryName")
    public String getCountryName() {
        return countryName;
    }

    @JsonProperty("CountryName")
    public void setCountryName(String countryName) {
        this.countryName = countryName;
    }

    @JsonProperty("AdministrativeArea")
    public AdministrativeArea getAdministrativeArea() {
        return administrativeArea;
    }

    @JsonProperty("AdministrativeArea")
    public void setAdministrativeArea(AdministrativeArea administrativeArea) {
        this.administrativeArea = administrativeArea;
    }

    @Override
    public String toString() {
        return MoreObjects.toStringHelper(this)
                .add("addressLine", addressLine)
                .add("countryNameCode", countryNameCode)
                .add("countryName", countryName)
                .add("administrativeArea", administrativeArea)
                .toString();
    }
}
