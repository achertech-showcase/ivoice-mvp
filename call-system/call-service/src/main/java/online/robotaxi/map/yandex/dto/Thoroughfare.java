
package online.robotaxi.map.yandex.dto;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;
import com.google.common.base.MoreObjects;

@JsonInclude(JsonInclude.Include.NON_NULL)
@JsonPropertyOrder({
        "ThoroughfareName",
        "Premise"
})
public class Thoroughfare {

    @JsonProperty("ThoroughfareName")
    private String thoroughfareName;
    @JsonProperty("Premise")
    private Premise premise;

    @JsonProperty("ThoroughfareName")
    public String getThoroughfareName() {
        return thoroughfareName;
    }

    @JsonProperty("ThoroughfareName")
    public void setThoroughfareName(String thoroughfareName) {
        this.thoroughfareName = thoroughfareName;
    }

    @JsonProperty("Premise")
    public Premise getPremise() {
        return premise;
    }

    @JsonProperty("Premise")
    public void setPremise(Premise premise) {
        this.premise = premise;
    }

    @Override
    public String toString() {
        return MoreObjects.toStringHelper(this)
                .add("thoroughfareName", thoroughfareName)
                .add("premise", premise)
                .toString();
    }
}
