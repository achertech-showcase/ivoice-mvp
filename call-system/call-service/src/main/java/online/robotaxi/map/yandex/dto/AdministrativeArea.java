
package online.robotaxi.map.yandex.dto;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;
import com.google.common.base.MoreObjects;

@JsonInclude(JsonInclude.Include.NON_NULL)
@JsonPropertyOrder({
        "AdministrativeAreaName",
        "Locality"
})
public class AdministrativeArea {

    @JsonProperty("AdministrativeAreaName")
    private String administrativeAreaName;
    @JsonProperty("Locality")
    private Locality locality;

    @JsonProperty("AdministrativeAreaName")
    public String getAdministrativeAreaName() {
        return administrativeAreaName;
    }

    @JsonProperty("AdministrativeAreaName")
    public void setAdministrativeAreaName(String administrativeAreaName) {
        this.administrativeAreaName = administrativeAreaName;
    }

    @JsonProperty("Locality")
    public Locality getLocality() {
        return locality;
    }

    @JsonProperty("Locality")
    public void setLocality(Locality locality) {
        this.locality = locality;
    }

    @Override
    public String toString() {
        return MoreObjects.toStringHelper(this)
                .add("administrativeAreaName", administrativeAreaName)
                .add("locality", locality)
                .toString();
    }
}
