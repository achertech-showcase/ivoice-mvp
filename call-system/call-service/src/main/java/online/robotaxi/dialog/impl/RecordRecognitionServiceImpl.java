package online.robotaxi.dialog.impl;

import online.robotaxi.config.RecordsProperty;
import online.robotaxi.dialog.RecordRecognitionService;
import online.robotaxi.freeswitch.InboundClient;
import online.robotaxi.speech.AudioRecognitionService;
import online.robotaxi.speech.RecognitionTopic;
import online.robotaxi.speech.yandex.AudioFile;
import org.freeswitch.esl.client.IEslEventListener;
import org.freeswitch.esl.client.transport.event.EslEvent;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Service;

import javax.annotation.PostConstruct;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.Arrays;
import java.util.List;
import java.util.Map;
import java.util.Optional;

import static java.lang.String.format;

@Service
public class RecordRecognitionServiceImpl implements RecordRecognitionService {
    private static final Logger LOG = LoggerFactory.getLogger(RecordRecognitionServiceImpl.class);
    private static final String EVENT_HEADER = "recordRecognitionService";

    private final InboundClient client;
    private final AudioRecognitionService<AudioFile> recognitionService;

    private final Path recordsParent;

    public RecordRecognitionServiceImpl(
            InboundClient client,
            AudioRecognitionService<AudioFile> recognitionService,
            RecordsProperty recordsProperty
    ) {
        this.client = client;
        this.recognitionService = recognitionService;

        this.recordsParent = Paths.get(recordsProperty.getRecordsParent());
        if (!Files.exists(recordsParent)) {
            throw new IllegalStateException(format("records parent path %s not exist", recordsParent));
        }
    }

    @SuppressWarnings("unused")
    @PostConstruct
    public void initFreeswitchListening() {
        client.addEventFilter("service", EVENT_HEADER);
        client.subscribe(new IEslEventListener() {
            @Override
            public void eventReceived(EslEvent event) {
                if (filterPassed(event)) {
                    onEventReceived(event);
                }
            }

            @Override
            public void backgroundJobResultReceived(EslEvent event) {

            }
        });
        LOG.info("subscribed for ESL");
    }

    private void onEventReceived(EslEvent event) {
        String sessionUid = event.getEventHeaders().get("sessionUuid");

        String messageType = event.getEventHeaders().get("messageType");
        String name = event.getEventHeaders().get("name");

        switch (messageType) {
            case "CUSTOM_RECORD":
                String record = event.getEventHeaders().get("record");
                onRecordedToRecognize(sessionUid, name, record);
                break;
            default:
                throw new IllegalStateException(format(
                        "unexpected message type %s for sessionUid %s",
                        messageType,
                        sessionUid
                ));
        }
    }

    private void onRecordedToRecognize(String sessionUuid, String recognitionName, String recordFileName) {
        Path recordFile = recordsParent.resolve(recordFileName);
        if (!Files.exists(recordFile)) {
            LOG.error(format("no record exist for %s", sessionUuid));
            fireRecognitionFailedEvent(sessionUuid);
            return;
        }

        AudioFile audio;
        try {
            audio = audioFile(recordFile);
        } catch (IOException e) {
            LOG.error(format("error while parsing audio: %s", e.getMessage()));
            fireRecognitionFailedEvent(sessionUuid);
            return;
        }

        Optional<String> maybeRecognition = recognitionService.recognize(audio, RecognitionTopic.ADDRESS);
        if (!maybeRecognition.isPresent()) {
            LOG.warn(format("no recognition for %s, %s", sessionUuid, recognitionName));
            fireRecognitionFailedEvent(sessionUuid);
            return;
        }

        String recognition = maybeRecognition.get();
        LOG.info(format("recognition for %s, %s = %s", sessionUuid, recognitionName, recognition));
        fireRecognized(sessionUuid, recognition);
    }

    private boolean filterPassed(EslEvent event) {
        Map<String, String> headers = event.getEventHeaders();
        return headers.containsKey("service") && headers.get("service").equals(EVENT_HEADER);
    }

    private void fireRecognized(String sessionUuid, String recognition) {
        List<String> headers = Arrays.asList(
                format("sessionUuid:%s", sessionUuid),
                "messageType:RECORD_RECOGNITION_RESULT",
                "result:SUCCESS",
                format("recognition:%s", recognition)
        );
        client.sendEvent(headers);
    }


    private void fireRecognitionFailedEvent(String sessionUuid) {
        List<String> headers = Arrays.asList(
                format("sessionUuid:%s", sessionUuid),
                "messageType:RECORD_RECOGNITION_RESULT",
                "result:FAILURE"
        );
        client.sendEvent(headers);
    }

    private AudioFile audioFile(Path recordFile) throws IOException {
        return new AudioFile(Files.readAllBytes(recordFile));
    }
}
