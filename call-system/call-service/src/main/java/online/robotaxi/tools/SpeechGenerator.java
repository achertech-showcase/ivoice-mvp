package online.robotaxi.tools;

import online.robotaxi.config.YandexSpeechProperties;
import online.robotaxi.speech.yandex.AudioFile;
import online.robotaxi.speech.yandex.YandexAudioSynthesisService;
import org.springframework.web.client.RestTemplate;

import java.io.IOException;
import java.nio.file.*;
import java.util.HashMap;
import java.util.Map;
import java.util.Optional;

import static java.lang.String.format;

public class SpeechGenerator {
    private static final Map<String, String> PHRAZES;

    private static final Path SOUNDS_PARENT = Paths.get("call-system/sound/to-convert/");
    private static final Path SCRIPT_SOUNDS = SOUNDS_PARENT.resolve("taxi");
    private static final Path LOUD_SOUNDS = SCRIPT_SOUNDS.resolve("loud");

    private static String parentFolder;

    // taxi
    static {
        parentFolder = "taxi";

        PHRAZES = new HashMap<>();
//        PHRAZES.put(
//                "1hello",
//                "Здравствуйте! Вы позвонили в робо-такси."
//        );
//
//        // tariff
//        PHRAZES.put("21_tariff_request", "Выберите тариф:");
//        PHRAZES.put("21_tariff_options", "эконом, или бибика?");
//        PHRAZES.put("21_tariff_option_validate_econom", "выбран тариф эконом");
//        PHRAZES.put("21_tariff_option_validate_bibika", "выбран тариф бибика");
//
//        // day
//        PHRAZES.put("22_day_request", "На какой день оформляем заказ?");
//        PHRAZES.put("22_day_options", "сегодня, или завтра?");
//        PHRAZES.put("22_day_option_validation_today", "заказ оформляем на сегодня.");
//        PHRAZES.put("22_day_option_validation_tomorrow", "заказ оформляем на завтра.");
//
//        // time
//        PHRAZES.put(
//                "23_time_request",
//                "Назовите время подачи в формате часы, затем минуты, например двадцать два тридцать."
//        );
//        PHRAZES.put("23_time_validation", "Время подачи:");
//
//        // arrival address
//        PHRAZES.put("23_request_address", "Назовите адрес подачи автомобиля.");
//        PHRAZES.put("23_repeat_addr", "Повторите пожалуйста адрес подачи.");
//        PHRAZES.put("23_arrival_on", "Машина будет подана по адресу.");
//
//        // payment
//        PHRAZES.put("25_payment_request", "Выберите способ оплаты:");
//        PHRAZES.put("9_payment_options", "наличными, или картой?");
//        PHRAZES.put("9_payment_option_validation_nal", "оплата наличными");
//        PHRAZES.put("9_payment_option_validation_card", "оплата картой");

        // helping phrazes
        PHRAZES.put("0_is_correct", "Всё верно? Да. Нет.");
//        PHRAZES.put("0_no_if_wrong", "Если я ошиблась, скажите нет.");
//        PHRAZES.put("0_please_repeat_universal", "Не могли бы вы повторить?");
    }

    private static YandexAudioSynthesisService service;

    public static void main(String[] args) {
        YandexSpeechProperties properties = new YandexSpeechProperties();
        properties.setSynthesisMethod("https://tts.voicetech.yandex.net/generate");
        properties.setKey("39d831ea-7cb2-492b-af66-2c7ce45179f3");
        properties.setClientUuid("6c54f3b745ad4335b347-0307633fa1d6");
        properties.setSynthesisQuality("lo");
        properties.setSpeaker("oksana");
        properties.setFormat("wav");
        service = new YandexAudioSynthesisService(
                new RestTemplate(), properties

        );

        try {
            Files.createDirectories(SCRIPT_SOUNDS);
            Files.createDirectories(LOUD_SOUNDS);
        } catch (IOException e) {
            e.printStackTrace();
            throw new IllegalStateException(e);
        }

//        PHRAZES.forEach(SpeechGenerator::synthesize);

        // test - short phrazes
//        synthesize(
//                "29_do_not_understand_address",
//                "Извините, я не поняла адрес."
//        );
//        synthesize(
//                "24_request_arrival_address_detailed",
//                "Пожалуйста, громко и четко повторите адрес в формате: улица - дом."
//        );

//        synthesize(
//                "81_request_comments",
//                "Есть ли у вас дополнительные пожелания к заказу? Говорите после сигнала."
//        );

//        synthesize(
//                "0_thanks",
//                "Спасибо!"
//        );

//        synthesize("1_conditions", "Заказы принимаются только с оплатой наличными и на ближайшее время.");

//        synthesize(
//                "9_order_created_thanks",
//                "Ваш заказ успешно оформлен! Чтобы уточнить статус заказа, или отменить заказ, перезвоните по этому же номеру. Спасибо что воспользовались нашими услугами, всего вам доброго!"
//        );

//        synthesize(
//                "9_placing_order",
//                "Оформляю заказ. пожалуйста ожидайте!"
//        );

//        synthesize(
//                "11_order_in_state_await_driver_accept",
//                "Ваш заказ принят, водитель пока не назначен."
//        );
//
//        synthesize(
//                "0_request_cancellation",
//                "Если вы хотите отменить заказ, скажите отмена."
//        );
//
//        synthesize(
//                "0_approve_cancellation",
//                "Пожалуйста, подтвердите отмену заказа. Отменяем? Ответьте, да. или нет."
//        );

//        synthesize(
//                "5_request_order_status",
//                "Уточняю статус вашего заказа! Пожалуйста, ожидайте."
//        );
//        synthesize(
//                "0_thanks_waiting",
//                "Спасибо за ожидание!"
//        );
//        synthesize(
//                "11_order_cancelled_start_new_order",
//                "Ваш последний заказ отменён! Оформляю новый заказ!"
//        );
//        synthesize(
//                "11_order_state_driver_arriving",
//                "Водитель назначен, автомобиль на подаче."
//        );
//        synthesize(
//                "11_order_state_driving",
//                "Вы в пути."
//        );

//        synthesize(
//                "11_menu",
//                "Если вы хотите отменить заказ, скажите отмена! Если вы хотите связаться с диспетчером, скажите диспетчер, или оставайтесь на линии"
//        );

//        synthesize(
//                "0_repeat_only",
//                "Не могли бы вы повторить?"
//        );

//        synthesize(
//                "12_cancelling",
//                "Отменяю заказ!"
//        );
//        synthesize(
//                "12_cancelling_success",
//                "Заказ успешно отменен!"
//        );
//        synthesize(
//                "12_connecting_dispatcher",
//                "Соединяю с диспетчером!"
//        );

        synthesize(
                "0_common_thanks_buy",
                "Спасибо что воспользовались нашими услугами. Всего вам доброго!"
        );

//        synthesize(
//                "0_order_cancelled",
//                "Ваш заказ отменен. Спасибо что воспользовались нашими услугами. Всего вам доброго!"
//        );

//        synthesize(
//                "9_sorry_cant_proceed",
//                "К сожалению, заказ оформить не удалось. Благодарим вас за терпение и просим прощения за доставленные неудобства. Попробуйте перезвонить чуть позже. Всего вам доброго!"
//        );

//        synthesize(
//                "2_details_question_short",
//                "Уточняю детали."
//        );
//        synthesize(
//                "0_please_repeat",
//                "Не могли бы вы повторить, нет или да?"
//        );
//        synthesize(
//                "0_yes_no_long",
//                "Ответьте пожалуйста, нет или да?"
//        );

//        synthesize(
//                "1_greet_request",
//                "Добрый день! Меня зовут Даша. Я робот. Компания нильс предлагает вам услуги по уничтожению насекомых и грызунов, услуги по фумигации и биозащите, а также огнезащитную обработку дерева и металла. Интересно ли вам данное предложение?"
//        );
//        synthesize(
//                "2_request_contacts",
//                "Наш специалист готов связаться с вами в удобное для вас время и рассказать подробности. Скажите пожалуйста, как, и когда лучше всего связаться с вами?"
//        );
//        synthesize(
//                "0_success_short_spec_will_call",
//                "Спасибо за уделённое время, наш специалист свяжется с вами! Хорошего вам дня!"
//        );

//        synthesize(
//                "1_greet_request_long",
//                "Добрый день! Меня зовут Даша. Я робот. Я хочу предложить вам подработку на личном автомобиле в сервисе Яндекс Такси. Вы сможете зарабатывать от двухсот до четырёхсот рублей за час работы. Вы будете получать дополнительно тысячу рублей за каждые 100 выполненных поездок. Скажите, готовы ли вы рассмотреть наше предложение?"
//        );
//
//        synthesize(
//                "2_details_question",
//                "Благодарю за ответ! Скажите пожалуйста, есть ли у вас личный автомобиль, иномарка не старше десяти лет или отечественный не старше пяти лет, с отсутствием видимых повреждений на кузове?"
//        );
//
//        synthesize(
//                "3_thanks_success",
//                "Спасибо за ваш ответ, в ближайшее время с вами свяжется наш менеджер. Приготовьте пожалуйста документа на автомобиль и водительские права. Вам также будет необходим телефон или планшет на базе андроид. Хорошего вам дня!"
//        );
//
//        synthesize(
//                "4_thanks_fail",
//                "Спасибо за ваш ответ, извините за беспокойство. Хорошего вам дня!"
//        );

    }

//    private static void synthesize(String phrazeName) {
//        String phraze = PHRAZES.get(phrazeName);
//        Optional<AudioFile> audioFile = service.synthesize(phraze);
//        try {
//            Path path = Paths.get(format("call-system/sound/to-convert/%s/%s.wav", parentFolder, phrazeName));
//            //noinspection ConstantConditions
//            Files.write(
//                    path,
//                    audioFile.get().getBytes(),
//                    StandardOpenOption.CREATE
//            );
//            increaseVolume(path, 10);
//        } catch (IOException e) {
//            throw new IllegalStateException(e);
//        }
//    }

    private static void increaseVolume(Path path, int gain) {
        try {
            Path name = path.getFileName();
            String nameWithoutExtension = com.google.common.io.Files.getNameWithoutExtension(name.toString());
//            Path gainPath = path.getParent().resolve(LOUD_SOUNDS).resolve(name);
            Path gainPath = path.getParent().resolve(nameWithoutExtension + "_loud.wav");
            Process exec = Runtime.getRuntime().exec(format("sox %s %s gain -n %s", path, gainPath, gain));
            try {
                exec.waitFor();
            } catch (InterruptedException e) {
                throw new IllegalStateException(e);
            }
            Files.move(
                    gainPath,
                    LOUD_SOUNDS.resolve(nameWithoutExtension + ".wav"),
                    StandardCopyOption.REPLACE_EXISTING
            );
        } catch (IOException e) {
            e.printStackTrace();
            throw new IllegalStateException("e");
        }
    }

    private static void synthesize(String phrazeName, String phraze) {
        Optional<AudioFile> audioFile = service.synthesize(phraze);
        try {
            Path path = SCRIPT_SOUNDS.resolve(phrazeName + ".wav");
            //noinspection ConstantConditions
            Files.write(
                    path,
                    audioFile.get().getBytes(),
                    StandardOpenOption.CREATE
            );
            increaseVolume(path, 10);
        } catch (IOException e) {
            throw new IllegalStateException(e);
        }
    }
}
