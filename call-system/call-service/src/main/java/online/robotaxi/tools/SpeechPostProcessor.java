package online.robotaxi.tools;

import ie.corballis.sox.SoXEffect;
import ie.corballis.sox.WrongParametersException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.IOException;
import java.nio.file.DirectoryStream;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.HashMap;
import java.util.Map;

import static java.lang.String.format;

/**
 * hasten speech, higher volume;
 * need sox to be installed in host system
 */
public class SpeechPostProcessor {
    private static final Logger LOG = LoggerFactory.getLogger(SpeechPostProcessor.class);

    // folders mapped for running from ide, with parent project imported
    private static final Path ORIGINAL_AUDIOS = Paths.get("call-system/sound/to-convert/clients");
    private static final Path TARGET_AUDIOS = Paths.get("call-system/sound/sounds");

    // this is always same, for best quality in asterisk audio

    private static final Map<SpeakerParameters, Object> AMAZON_MAXIM_PARAMETERS;
    private static final Map<SpeakerParameters, Object> YANDEX_OKSANA_PARAMETERS;

    static {
        AMAZON_MAXIM_PARAMETERS = new HashMap<>();

        AMAZON_MAXIM_PARAMETERS.put(SpeakerParameters.ORIGINAL_FORMAT, "ogg");
        AMAZON_MAXIM_PARAMETERS.put(SpeakerParameters.ORIGINAL_RATE, "16k");
//        AMAZON_MAXIM_PARAMETERS.put(SpeakerParameters.VOLUME_ADJUSTMENT, "");
        Map<SoXEffect, String> effects = new HashMap<>();
        effects.put(SoXEffect.SPEED, "1.08");
        effects.put(SoXEffect.LOUDNESS, "1.3");
        AMAZON_MAXIM_PARAMETERS.put(SpeakerParameters.SOX_EFFECTS, effects);
        AMAZON_MAXIM_PARAMETERS.put(SpeakerParameters.TARGET_FORMAT, "sln16");
    }

    static {
        YANDEX_OKSANA_PARAMETERS = new HashMap<>();

        YANDEX_OKSANA_PARAMETERS.put(SpeakerParameters.ORIGINAL_FORMAT, "opus");
        YANDEX_OKSANA_PARAMETERS.put(SpeakerParameters.ORIGINAL_RATE, "48k");
    }

    //    private static final String CLIENT = "parazitamnet";
    private static final String CLIENT = "kupofon";
    private static final boolean makeBck = false;

    public static void main(String[] args) throws IOException, WrongParametersException {
        processAllAudios(CLIENT, AMAZON_MAXIM_PARAMETERS);
    }

    private static void processAllAudios(
            String client,
            Map<SpeakerParameters, Object> parameters
    ) throws IOException, WrongParametersException {
        Path originalAudios = ORIGINAL_AUDIOS.resolve(client);
        Path targetParent = TARGET_AUDIOS.resolve(client);

        if (!Files.exists(originalAudios)) {
            throw new IllegalStateException();
        }
        if (makeBck && Files.exists(targetParent)) {
            bck(targetParent);
        }
        if (Files.exists(targetParent)) {
            try (DirectoryStream<Path> stream = Files.newDirectoryStream(targetParent)) {
                for (Path f : stream) {
                    if (!Files.isDirectory(f)) {
                        Files.delete(f);
                    }
                }
            }
        }
        if (!Files.exists(targetParent)) {
            Files.createDirectory(targetParent);
        }

        try (DirectoryStream<Path> stream = Files.newDirectoryStream(originalAudios)) {
            for (Path f : stream) {
                if (!Files.isDirectory(f)) {
                    String extension = com.google.common.io.Files.getFileExtension(f.getFileName().toString());
                    if (extension.equals(parameters.get(SpeakerParameters.ORIGINAL_FORMAT))) {
                        processAudio(f, targetParent, parameters);
                    }
                }
            }
        }
    }

    private static void bck(Path targetParent) throws IOException {
        Path bckTarget = targetParent.resolve("bck");
        if (!Files.exists(bckTarget)) {
            Files.createDirectory(bckTarget);
        }

        try (DirectoryStream<Path> stream = Files.newDirectoryStream(targetParent)) {
            for (Path f : stream) {
                if (!f.getFileName().toString().equals("bck")) {
                    Files.move(f, bckTarget.resolve(f.getFileName()));
                }
            }
        }
    }

    private static void processAudio(
            Path original,
            Path targetParent,
            Map<SpeakerParameters, Object> parameters
    ) throws WrongParametersException, IOException {
        String fileNameWithoutExtension = com.google.common.io.Files
                .getNameWithoutExtension(original.getFileName().toString());
        Path target = targetParent.resolve(format("%s.sln", fileNameWithoutExtension));
        LOG.info(format("%s -> %s", original, target));

        StringBuilder cmd = new StringBuilder("sox ");
        if (parameters.containsKey(SpeakerParameters.VOLUME_ADJUSTMENT)) {
            String volumeAdjustmentValue = (String) parameters.get(SpeakerParameters.VOLUME_ADJUSTMENT);
            cmd.append("-v ").append(volumeAdjustmentValue).append(" ");
        }
        cmd.append(original.toAbsolutePath()).append(" ");
        cmd.append("-t raw").append(" ");
        cmd.append("--rate ").append(parameters.get(SpeakerParameters.ORIGINAL_RATE)).append(" ");
        cmd.append(" -e signed-integer -b 16 -c 1").append(" ");
        cmd.append(target.toAbsolutePath()).append(" ");
        if (parameters.containsKey(SpeakerParameters.SOX_EFFECTS)) {
            //noinspection unchecked
            Map<SoXEffect, String> effects = (Map<SoXEffect, String>) parameters.get(SpeakerParameters.SOX_EFFECTS);
            for (SoXEffect effect : effects.keySet()) {
                cmd.append(effect.name()).append(" ").append(effects.get(effect)).append(" ");
            }
        }

        LOG.info(cmd.toString());
        Process exec = Runtime.getRuntime().exec(cmd.toString());
        try {
            exec.waitFor();
        } catch (InterruptedException e) {
            throw new IllegalStateException(e);
        }
//        sox.execute();

        Path sln16target = targetParent.resolve(format(
                "%s.%s",
                fileNameWithoutExtension,
                parameters.get(SpeakerParameters.TARGET_FORMAT)
        ));
        Files.move(target, sln16target);

    }

    private enum SpeakerParameters {
        ORIGINAL_FORMAT,
        ORIGINAL_RATE, // khz
        VOLUME_ADJUSTMENT,
        SOX_EFFECTS,
        TARGET_FORMAT
    }

}
