package online.robotaxi.tools;

import online.robotaxi.config.YandexSpeechProperties;
import online.robotaxi.speech.yandex.AudioFile;
import online.robotaxi.speech.yandex.YandexAudioSynthesisService;
import org.springframework.web.client.RestTemplate;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.nio.file.StandardOpenOption;
import java.util.Optional;
import java.util.stream.Stream;

import static java.lang.String.format;

/**
 * generates all phrazes for client
 */
public class PacketSpeechGenerator {
    // will work if import parent project in idea
    private static final Path TO_CONVERT_RELATIVE_PATH = Paths.get("call-system/sound/to-convert");
    private static final Path CLIENTS_PATH = TO_CONVERT_RELATIVE_PATH.resolve("clients");

    private static YandexAudioSynthesisService service;

    public static void main(String[] args) {
        YandexSpeechProperties properties = new YandexSpeechProperties();
        properties.setSynthesisMethod("https://tts.voicetech.yandex.net/generate");
        properties.setKey("4a5fd461-c41c-47b8-8b95-ea4e26c190b1");
        properties.setSynthesisQuality("hi");
        properties.setSpeaker("oksana");
        properties.setFormat("opus");
        service = new YandexAudioSynthesisService(
                new RestTemplate(), properties

        );
        // correcsponds to sound/to-convert/client/<client>.txt
        String clientName = "parazitamnet";
        if (!Files.exists(CLIENTS_PATH.resolve(clientName))) {
            try {
                Files.createDirectory(CLIENTS_PATH.resolve(clientName));
            } catch (IOException e) {
                throw new IllegalStateException(e);
            }
        }
        parsePhrazes(clientName)
                .forEach(p -> synthesize(clientName, p, properties.getFormat()));
    }

    private static Stream<PhrazeAndText> parsePhrazes(String clientName) {
        Path phrazesFile = CLIENTS_PATH.resolve(format("%s.txt", clientName));

        try {
            return Files.lines(phrazesFile)
                    .map(p -> {
                        int firstSpaceId = p.indexOf(" ");
                        String phrazeName = p.substring(0, firstSpaceId);
                        String text = p.substring(firstSpaceId, p.length());
                        return new PhrazeAndText(phrazeName, text);
                    });
        } catch (IOException e) {
            throw new IllegalStateException(e);
        }
    }

    private static void synthesize(String client, PhrazeAndText phrazeAndText, String format) {
        Optional<AudioFile> audioFile = service.synthesize(phrazeAndText.text);
        try {
            //noinspection ConstantConditions
            Files.write(
                    CLIENTS_PATH.resolve(client).resolve(format("%s.%s", phrazeAndText.phraze, format)),
                    audioFile.get().getBytes(),
                    StandardOpenOption.CREATE
            );
        } catch (IOException e) {
            throw new IllegalStateException(e);
        }
    }

    private static final class PhrazeAndText {
        private final String phraze;
        private final String text;

        PhrazeAndText(String phraze, String text) {
            this.phraze = phraze;
            this.text = text;
        }

        public String getPhraze() {
            return phraze;
        }

        public String getText() {
            return text;
        }
    }
}
