package online.robotaxi.model;

import org.junit.Test;

import java.time.LocalDateTime;
import java.time.ZoneId;
import java.time.ZonedDateTime;

import static org.junit.Assert.assertEquals;

public class TaxiOrderTest {

    @Test
    public void thatArrivalTimeFieldsSetupProperly() {
        // 10.10.2010 10:30 UTC+3 (moscow)
        ZonedDateTime dateTime = ZonedDateTime.of(2010, 10, 10, 10, 30, 0, 0, ZoneId.of("+3"));

        TaxiOrder order = new TaxiOrder(
                "session",
                LocalDateTime.now().atZone(ZoneId.of("+3")),
                "phone",
                dateTime,
                null,
                null,
                null
        );
        ZonedDateTime recreatedDateTime = order.zonedArrivalDateTime();
        assertEquals(dateTime, recreatedDateTime);
    }
}