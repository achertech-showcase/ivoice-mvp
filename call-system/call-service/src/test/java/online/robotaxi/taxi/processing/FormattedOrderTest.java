package online.robotaxi.taxi.processing;

import online.robotaxi.model.Address;
import online.robotaxi.model.TaxiOrder;
import org.junit.Test;

import java.time.ZoneId;
import java.time.ZonedDateTime;
import java.time.temporal.ChronoUnit;

import static java.lang.System.out;

public class FormattedOrderTest {
    @Test
    public void thatFormattedOrderCreatedCorrectly() {
        ZonedDateTime nowPlus4 = ZonedDateTime.now(ZoneId.of("+4"));
        ZonedDateTime arrival = nowPlus4.plus(1, ChronoUnit.HOURS);
        Address arrivalAddr = new Address("full address", "city", "street", "house");
        TaxiOrder order = new TaxiOrder("session", nowPlus4, "71231231212", arrival, arrivalAddr, "comment", null);
        PlaceOrderProcessor.FormattedOrder fo = new PlaceOrderProcessor.FormattedOrder(order);
        out.println(fo);
    }
}