package online.robotaxi.taxi.dto;

import org.junit.Test;

import java.time.LocalDateTime;
import java.time.ZoneId;
import java.time.ZonedDateTime;

import static java.lang.System.out;

public class OrderDtoTest {
    @Test
    public void findArrivalTime() throws Exception {
        OrderDto dto = new OrderDto(
                "phone",
                "+7",
                new DurationOffsetDto(30, "MINUTES"),
                new AddressDto(),
                null,
                null
        );
        ZonedDateTime creation = LocalDateTime.now().atZone(ZoneId.of("+3"));
        out.println("creation: " + creation);
        ZonedDateTime arrivalTime = dto.findArrivalTime(creation);
        out.println("arrival: " + arrivalTime);
    }

}