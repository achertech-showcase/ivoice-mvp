package online.robotaxi.taxi.dto;

import com.fasterxml.jackson.databind.ObjectMapper;
import org.junit.Test;

import java.io.IOException;
import java.util.Base64;

public class FailedOrderParsingTest {
    @Test
    public void test() {
        String jsonBase64 = "eyJ0aW1lWm9uZU9mZnNldCI6IkdNVCs3IiwiYXJyaXZhbFRpbWVPZmZzZXQiOnsidmFsdWUiOjEsInVuaXQiOiJEQVlTIn0sInNlbGVjdGVkT3B0aW9ucyI6eyJydWxlIjozLCJ0YXJpZmYiOjF9LCJwaG9uZSI6IjEwMDYiLCJpZCI6IjE3NzMzOSIsImFycml2YWxBZGRyZXNzIjp7ImZ1bGxBZGRyZXNzIjoi0JrQtdC80LXRgNC+0LLQviwg0YPQu9C40YbQsCDQm9C10L3QuNC90LAsIDEiLCJjaXR5Ijoi0JrQtdC80LXRgNC+0LLQviIsInN0cmVldCI6ItGD0LvQuNGG0LAg0JvQtdC90LjQvdCwIiwiaG91c2UiOiIxIn0sImNvbW1lbnRzIjoi0LLQvtC30YzQvNGDINC00LXRgtC10LkifQ==";
        byte[] orderBytes = Base64.getDecoder().decode(jsonBase64);
        OrderDto orderDto;
        try {
            String orderJson = new String(orderBytes, "UTF-8");
            orderDto = new ObjectMapper().readValue(orderJson, OrderDto.class);
        } catch (IOException e) {
        }
    }
}
