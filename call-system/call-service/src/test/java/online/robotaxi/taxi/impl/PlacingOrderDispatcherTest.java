package online.robotaxi.taxi.impl;

import online.robotaxi.config.WebdriverConfig;
import online.robotaxi.config.WebdriverProperties;
import online.robotaxi.model.Address;
import online.robotaxi.model.TaxiOrder;
import online.robotaxi.model.YandexPassport;
import online.robotaxi.repository.TaxiOrderRepository;
import online.robotaxi.repository.YandexPassportRepository;
import online.robotaxi.taxi.dispatcher.DispatcherLoginService;
import online.robotaxi.taxi.dispatcher.PlacingYandexOrderDispatcher;
import online.robotaxi.taxi.dispatcher.impl.DispatcherYandexLoginServiceImpl;
import online.robotaxi.taxi.exception.PageFormatChangedException;
import online.robotaxi.taxi.model.OrderTask;
import online.robotaxi.taxi.processing.PlaceOrderProcessor;
import org.junit.Before;
import org.junit.Ignore;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.openqa.selenium.WebDriver;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.boot.context.properties.ConfigurationPropertiesBindingPostProcessor;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.stereotype.Component;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.TestPropertySource;
import org.springframework.test.context.junit4.SpringRunner;

import java.time.ZoneId;
import java.time.ZonedDateTime;
import java.time.temporal.ChronoUnit;
import java.util.Collections;
import java.util.HashMap;
import java.util.Map;
import java.util.concurrent.CountDownLatch;

import static java.lang.System.out;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;

@Ignore
@ContextConfiguration(
        classes = {
                PlacingOrderDispatcherTest.Config.class,
                WebdriverProperties.class,
                WebdriverConfig.class
        }
)
@TestPropertySource(locations = {"classpath:application.properties", "classpath:/secrets/application-secrets.properties"})
@RunWith(SpringRunner.class)
public class PlacingOrderDispatcherTest {

    @Autowired
    private PlacingOrderDispatcherTest.Config.PassportSecretProperties passportSecretProperties;
    @Autowired
    @Qualifier("placingOrder")
    private WebDriver driver;
    @Autowired
    private WebdriverProperties webdriverProperties;

    private YandexPassportRepository passportRepository;
    private TaxiOrderRepository taxiOrderRepository;
    private DispatcherLoginService loginService;
    private PlaceOrderProcessor processor;

    private PlacingYandexOrderDispatcher service;

    @Before
    public void setUp() {
        passportRepository = mock(YandexPassportRepository.class);
        taxiOrderRepository = mock(TaxiOrderRepository.class);

        when(passportRepository.findAll()).thenReturn(
                Collections.singletonList(
                        new YandexPassport(
                                "my",
                                passportSecretProperties.getLogin(),
                                passportSecretProperties.getPassword()
                        )
                )
        );
        loginService = new DispatcherYandexLoginServiceImpl(passportRepository);
        processor = new PlaceOrderProcessor(taxiOrderRepository);
        service = new PlacingYandexOrderDispatcher(
                driver, loginService, Collections.singletonList(processor)
        );
    }

    @Test
    public void test() throws PageFormatChangedException {
        CountDownLatch latch = new CountDownLatch(1);
        service.loginAndInitProcessing();
        for (int i = 0; i < 1; i++) {
            OrderTask task = new OrderTask(
                    createOrder(),
                    OrderTask.Task.PLACE,
                    Collections.singletonList(() -> {
                        out.println("placed");
                        latch.countDown();
                    }),
                    Collections.singletonList(() -> {
                        out.println("failed");
                        latch.countDown();
                    })
            );
            service.queueTask(task);
            try {
                latch.await();
            } catch (InterruptedException e) {
                throw new IllegalStateException(e);
            }
        }

    }

    private TaxiOrder createOrder() {
        Address address = new Address(
                "Кемерово, улица Ленина, 1",
                "Кемерово",
                "Ленина",
                "1"
        );
        Map<String, Integer> selectedOptions = new HashMap<>();
        selectedOptions.put("rule", 3);
        selectedOptions.put("tariff", 1);
        return new TaxiOrder(
                "session-uuid",
                ZonedDateTime.now(ZoneId.of("+3")),
                "79771368592",
                ZonedDateTime.now(ZoneId.of("+3")).plus(1, ChronoUnit.DAYS),
                address,
                "тестовый заказ! не брать!",
                selectedOptions
        );
    }

    @Configuration
    static class Config {
        @Bean
        public static ConfigurationPropertiesBindingPostProcessor propertiesProcessor() {
            return new ConfigurationPropertiesBindingPostProcessor();
        }

        @Component
        @ConfigurationProperties(prefix = "yandexpassport")
        static class PassportSecretProperties {
            private String login;
            private String password;

            public String getLogin() {
                return login;
            }

            public void setLogin(String login) {
                this.login = login;
            }

            public String getPassword() {
                return password;
            }

            public void setPassword(String password) {
                this.password = password;
            }
        }
    }


}