package online.robotaxi.taxi.impl;

import online.robotaxi.model.TaxiOrder;
import online.robotaxi.taxi.dto.AddressDto;
import online.robotaxi.taxi.dto.DurationOffsetDto;
import online.robotaxi.taxi.dto.OrderDto;
import org.junit.Test;

import static java.lang.System.out;

public class OrderReceiveServiceImplTest {

    @Test
    public void thatOrderFromDtoCreatedCorrectly() {
        OrderDto orderDto = new OrderDto(
                "71231231212",
                "GMT+7", // offset from local time
                new DurationOffsetDto(30, "MINUTES"),
                new AddressDto("full address", "city", "street", "house"),
                "comment",
                null
        );
        TaxiOrder order = OrderReceiveServiceImpl.fromDto("session", orderDto);
        out.println(order);
    }
}