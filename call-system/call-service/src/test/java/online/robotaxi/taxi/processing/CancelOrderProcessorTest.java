package online.robotaxi.taxi.processing;

import online.robotaxi.config.WebdriverConfig;
import online.robotaxi.config.WebdriverProperties;
import online.robotaxi.model.TaxiOrder;
import online.robotaxi.model.YandexPassport;
import online.robotaxi.repository.TaxiOrderRepository;
import online.robotaxi.repository.YandexPassportRepository;
import online.robotaxi.taxi.SeleniumDispatcher;
import online.robotaxi.taxi.dispatcher.DispatcherLoginService;
import online.robotaxi.taxi.dispatcher.impl.DispatcherYandexLoginServiceImpl;
import online.robotaxi.taxi.model.OrderTask;
import org.junit.Before;
import org.junit.Ignore;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.openqa.selenium.WebDriver;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.boot.context.properties.ConfigurationPropertiesBindingPostProcessor;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.stereotype.Component;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.TestPropertySource;
import org.springframework.test.context.junit4.SpringRunner;

import java.time.ZonedDateTime;
import java.util.Collections;

import static java.lang.System.out;
import static org.junit.Assert.assertEquals;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;

@Ignore
@ContextConfiguration(
        classes = {
                CancelOrderProcessorTest.Config.class,
                WebdriverProperties.class,
                WebdriverConfig.class
        }
)
@TestPropertySource(locations = {"classpath:application.properties", "classpath:/secrets/application-secrets.properties"})
@RunWith(SpringRunner.class)
public class CancelOrderProcessorTest {
    @Autowired
    private CancelOrderProcessorTest.Config.PassportSecretProperties passportSecretProperties;
    @Autowired
    @Qualifier("workingOrder")
    private WebDriver webDriver;

    private DispatcherLoginService loginService;
    private YandexPassportRepository passportRepository;

    TaxiOrderRepository repository;
    CancelOrderProcessor processor;
    SeleniumDispatcher dispatcher;

    @Before
    public void setUp() {
        repository = mock(TaxiOrderRepository.class);
        processor = new CancelOrderProcessor(repository);

        passportRepository = mock(YandexPassportRepository.class);
        when(passportRepository.findAll()).thenReturn(
                Collections.singletonList(
                        new YandexPassport(
                                "my",
                                passportSecretProperties.getLogin(),
                                passportSecretProperties.getPassword()
                        )
                )
        );

        loginService = new DispatcherYandexLoginServiceImpl(passportRepository);
        dispatcher = mock(SeleniumDispatcher.class);
        when(dispatcher.getWebDriver()).thenReturn(webDriver);
    }

    @Test
    public void test() {
        loginService.login("workingOrderDispatcher", webDriver);
        TaxiOrder order = new TaxiOrder(
                null,
                ZonedDateTime.now(),
                "79771368592",
                ZonedDateTime.now(),
                null,
                null,
                null
        );
        order.placed("171930");
        order.setRemoteUuid("ad74c5117f834bbda74c7bc5e8a52d35");
        OrderTask task = new OrderTask(
                order,
                OrderTask.Task.CANCEL,
                Collections.singletonList(() -> out.println("processor returned, success")),
                null
        );
        processor.process(dispatcher, task);
        assertEquals(2, order.getRemoteTaskLogs().size());
    }

    @Configuration
    static class Config {
        @Bean
        public static ConfigurationPropertiesBindingPostProcessor propertiesProcessor() {
            return new ConfigurationPropertiesBindingPostProcessor();
        }

        @Component
        @ConfigurationProperties(prefix = "yandexpassport")
        static class PassportSecretProperties {
            private String login;
            private String password;

            public String getLogin() {
                return login;
            }

            public void setLogin(String login) {
                this.login = login;
            }

            public String getPassword() {
                return password;
            }

            public void setPassword(String password) {
                this.password = password;
            }
        }
    }
}