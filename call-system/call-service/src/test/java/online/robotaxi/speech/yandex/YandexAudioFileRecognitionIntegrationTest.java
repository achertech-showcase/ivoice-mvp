package online.robotaxi.speech.yandex;

import online.robotaxi.config.YandexSpeechProperties;
import online.robotaxi.speech.RecognitionTopic;
import org.junit.Ignore;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.context.properties.ConfigurationPropertiesBindingPostProcessor;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.TestPropertySource;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.web.client.RestTemplate;

import java.net.URL;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.Optional;

import static java.lang.System.out;

@Ignore // run only manually!
@ContextConfiguration(
        classes = {
                YandexAudioFileRecognitionIntegrationTest.Config.class,
                YandexSpeechProperties.class,
                YandexAudioFileRecognition.class
        }
)
@TestPropertySource(locations = {"classpath:application.properties"})
@RunWith(SpringRunner.class)
@SuppressWarnings("SameParameterValue")
public class YandexAudioFileRecognitionIntegrationTest {
    @Autowired
    private YandexAudioFileRecognition recognitionClient;

    @Test
    public void recognize() throws Exception {
//        AudioFile audioFile = load("/home/artyom/projects/my/robotaxi/call-system/sound/to-convert/taxi/loud/0_approve_cancellation.wav");
        AudioFile audioFile = new AudioFile(Files.readAllBytes(Paths.get(
                "/home/artyom/projects/my/robotaxi/call-system/sound/to-convert/taxi/loud/0_approve_cancellation.wav")));
        Optional<String> recognition = recognitionClient.recognize(audioFile, RecognitionTopic.ADDRESS);
        if (recognition.isPresent()) {
            out.printf("recognized: %s\n", recognition.get());
        } else {
            out.println("result empty");
        }
    }

    private AudioFile load(String resource) {
        URL url = YandexAudioFileRecognitionTest.class.getResource(resource);
        try {
            Path resPath = Paths.get(url.toURI());
            return new AudioFile(Files.readAllBytes(resPath));
        } catch (Exception e) {
            throw new IllegalStateException(e);
        }
    }

    /**
     * needed for property value injection injection to work
     */
    @Configuration
    static class Config {
        @Bean
        public static ConfigurationPropertiesBindingPostProcessor propertiesProcessor() {
            return new ConfigurationPropertiesBindingPostProcessor();
        }

        @Bean
        public RestTemplate restTemplate() {
            return new RestTemplate();
        }
    }

}