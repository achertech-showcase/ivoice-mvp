package online.robotaxi.speech.yandex;

import online.robotaxi.config.YandexSpeechProperties;
import org.junit.Ignore;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.context.properties.ConfigurationPropertiesBindingPostProcessor;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.TestPropertySource;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.web.client.RestTemplate;

import java.nio.file.Files;
import java.nio.file.Paths;
import java.nio.file.StandardOpenOption;
import java.util.Optional;

@Ignore // run only manually!
@ContextConfiguration(
        classes = {
                YandexAudioSynthesisServiceIntegrationTest.Config.class,
                YandexSpeechProperties.class,
                YandexAudioSynthesisService.class
        }
)
@TestPropertySource(locations = {"classpath:application.properties"})
@RunWith(SpringRunner.class)
@SuppressWarnings("SameParameterValue")
public class YandexAudioSynthesisServiceIntegrationTest {
    @Autowired
    private YandexAudioSynthesisService synthesisService;

    @Test
    public void synthesize() throws Exception {
        Optional<AudioFile> audioFile = synthesisService.synthesize("Привет, проверка связи!");
        Files.write(Paths.get("test.wav"),audioFile.get().getBytes(),StandardOpenOption.CREATE);
    }

    /**
     * needed for property value injection injection to work
     */
    @Configuration
    static class Config {
        @Bean
        public static ConfigurationPropertiesBindingPostProcessor propertiesProcessor() {
            return new ConfigurationPropertiesBindingPostProcessor();
        }

        @Bean
        public RestTemplate restTemplate() {
            return new RestTemplate();
        }
    }
}