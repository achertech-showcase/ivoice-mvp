package online.robotaxi.speech.yandex;

import online.robotaxi.config.YandexSpeechProperties;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import org.mockito.runners.MockitoJUnitRunner;
import org.springframework.web.client.RestTemplate;

import java.io.ByteArrayInputStream;
import java.io.InputStream;
import java.net.URL;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.Arrays;

import static org.junit.Assert.assertEquals;
import static org.mockito.Mockito.when;

@SuppressWarnings("ConstantConditions")
@RunWith(MockitoJUnitRunner.class)
public class YandexAudioFileRecognitionTest {
    private YandexAudioFileRecognition recognitionClient;
    @Mock
    private RestTemplate restTemplate;
    @Mock
    private YandexSpeechProperties config;

    @Before
    public void setUp() {
        when(config.getClientUuid()).thenReturn("clientUuid");
        when(config.getKey()).thenReturn("key");
        recognitionClient = new YandexAudioFileRecognition(restTemplate, config);
    }

    @Test
    public void thatSuccessfulRecognitionParsedProperly() throws Exception {
        InputStream responseStream = loadResource("/yandex/recognition-successful.xml");
        RecognitionResultsDto resultsDto = recognitionClient.parseResponse(responseStream).get();

        RecognitionResultsDto.Variant v1 = new RecognitionResultsDto.Variant(0.89f, "твой номер 212-85-06");
        RecognitionResultsDto.Variant v2 = new RecognitionResultsDto.Variant(0, "твой номер 213-85-06");
        RecognitionResultsDto expected = RecognitionResultsDto.successfull(Arrays.asList(v1, v2));
        assertEquals(expected, resultsDto);
    }

    @Test
    public void thatUnsuccessfulRecognitionResultParsedProperly() throws Exception {
        InputStream responseStream = loadResource("/yandex/recognition-unsuccessful.xml");
        RecognitionResultsDto result = recognitionClient.parseResponse(responseStream).get();
        RecognitionResultsDto expected = RecognitionResultsDto.unsuccessfull();
        assertEquals(expected, result);
    }

    private static InputStream loadResource(String resource) {
        URL url = YandexAudioFileRecognitionTest.class.getResource(resource);
        try {
            Path resPath = Paths.get(url.toURI());
            return new ByteArrayInputStream(Files.readAllBytes(resPath));
        } catch (Exception e) {
            throw new IllegalStateException(e);
        }

    }
}
