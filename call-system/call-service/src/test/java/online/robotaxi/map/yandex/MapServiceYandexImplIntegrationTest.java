package online.robotaxi.map.yandex;

import online.robotaxi.config.YandexMapsProperties;
import online.robotaxi.map.Address;
import org.junit.Ignore;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.context.properties.ConfigurationPropertiesBindingPostProcessor;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.TestPropertySource;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.web.client.RestTemplate;

import java.util.Optional;

import static java.lang.System.out;
import static org.junit.Assert.assertTrue;

@Ignore // run only manually!
@ContextConfiguration(
        classes = {
                MapServiceYandexImplIntegrationTest.Config.class,
                YandexMapsProperties.class,
                MapServiceYandexImpl.class
        }
)
@TestPropertySource(locations = {"classpath:application.properties"})
@RunWith(SpringRunner.class)
@SuppressWarnings("SameParameterValue")
public class MapServiceYandexImplIntegrationTest {
    @Autowired
    private MapServiceYandexImpl addressCheckServiceOpenStreet;

    @Test
    public void checkExisting() throws Exception {
        Optional<Address> address = addressCheckServiceOpenStreet.findMapAddress(
                "Москва",
                "улица бориса жигуленкова дом 25 корпус 4");
        assertTrue(address.isPresent());
        out.println(address);
    }

    @Test
    public void checkDuplicateAddress() throws Exception {
        Optional<Address> address = addressCheckServiceOpenStreet.findMapAddress(
                "Москва",
                "Москва улица бориса жигуленкова дом 25 корпус 4");
        assertTrue(address.isPresent());
        out.println(address);
    }

    @Test
    public void checkNotHouse() throws Exception {
        Optional<Address> address = addressCheckServiceOpenStreet.findMapAddress(
                "Москва",
                "метро семеновская");
        assertTrue(address.isPresent());
        out.println(address);
    }

    @Test
    public void checkNotExisting() throws Exception {
        Optional<Address> address = addressCheckServiceOpenStreet.findMapAddress(
                "Крыжополь",
                "нет такого адреса бла бла бла");
        assertTrue(!address.isPresent());
    }

    /**
     * needed for property value injection injection to work
     */
    @Configuration
    static class Config {
        @Bean
        public static ConfigurationPropertiesBindingPostProcessor propertiesProcessor() {
            return new ConfigurationPropertiesBindingPostProcessor();
        }

        @Bean
        public RestTemplate restTemplate() {
            return new RestTemplate();
        }
    }
}