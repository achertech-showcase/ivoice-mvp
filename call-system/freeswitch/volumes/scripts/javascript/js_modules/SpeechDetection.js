use('XML');

// grammar ex.: yes_no_ru.gram
function initAsr(grammar) {
    session.execute("detect_speech", "pocketsphinx yes_no_ru "+grammar);
    session.execute("detect_speech", "pause");
}

function stopDetection() {
    session.execute("detect_speech", "stop");
}

function play(audio) {
    session.execute("playback", audio);
}

function Phrase(name, phrase, final) {
    this.name = name;
    this.phrase = phrase;
    this.run = function () {
        if (final) {
            stopDetection();
        }
        console_log("running phrase " + this.name);
        session.streamFile(phrase, ignoreInput);
        if (final) {
            session.hangup();
        }
    }
}

function YnRequest(name, requestPhrase, timeout, retryAttempts) {
    this.name = name;
    this.timeout = timeout;
    this.retryAttempts = retryAttempts;
    this.attemptsMade = 0;
    this.result = "silence"
    this.recognitions = [];
    this.mostConfidentRecognition = null;

    this.run = function () {
        this.attemptsMade++;

        console_log("running request " + this.name);
        session.streamFile(requestPhrase, ignoreInput);
        session.execute("detect_speech", "resume");
        session.streamFile(yesNoAudio, onInput);
        session.collectInput(onInput, this, timeout);
        while ((this.result == "silence" || this.result == "unrecognized") && this.attemptsMade < this.retryAttempts) {
            this.processUnrecognized();
        }
        // if (this.result == "unrecognized") {
        //     console_log("no more recognition attemtps left; result = unrecognized");
        // }
        session.execute("detect_speech", "pause");

        var recignitionsAmount = this.recognitions.length;
        if (recignitionsAmount == 1) {
            this.mostConfidentRecognition = request1.recognitions[0];
        } else if (recignitionsAmount == 2) {
            if (request1.recognitions[0].confidence >= request1.recognitions[1].confidence) {
                this.mostConfidentRecognition = request1.recognitions[0];
            } else {
                this.mostConfidentRecognition = request1.recognitions[1];
            }
        } else {
            console_log("ERROR! expected 0,1, max 2 recognitions, but found " + recignitionsAmount);
        }
    }

    this.processUnrecognized = function () {
        this.attemptsMade++;
        console_log("performing next recognition attempt; " + this.attemptsMade + "/" + this.retryAttempts);
        session.execute("detect_speech", "resume");
        session.streamFile(pleaseRepeatAudio, onInput);
        session.collectInput(onInput, this, timeout);
    }

    this.updateResult = function (input, confidence) {
        console_log("updating result for " + input + ";" + confidence);
        var recognition = {};
        recognition.input = input;
        recognition.confidence = confidence;
        this.recognitions.push(recognition);
        var result = this[input];
        //console_log("checking branch " + JSON.stringify(result));
        if (confidence >= result.neededConfidence) {
            console_log("enough confidence; progressing dialplan")
            this.result = result.name;
            // stop collectInput
            session.execute("detect_speech", "pause");
            return false;
        } else {
            console_log("not enough confidence; result = unrecognized");
            this.result = "unrecognized";
            return false;
        }
    }
}

function Result(name, neededConfidence) {
    this.name = name;
    this.neededConfidence = neededConfidence;
}

function AddressRequest(requestAudio, checkingAddressAudio, arrivalOnAudio, isAllRightAudio, city, maxAttempts) {
    this.requestAudio = requestAudio;
    this.checkingAddressAudio = checkingAddressAudio;
    this.arrivalOnAudio = arrivalOnAudio;
    this.isAllRightAudio = isAllRightAudio;
    this.city = city;
    this.maxAttempts = maxAttempts;

    this.eventHandler = null;

    this.addressRecordName;

    this.attemptsMade = 0;

    this.run = function () {
        if (this.attemptsMade >= this.maxAttempts) {
            console_log("attemtps exceeded: " + this.attemptsMade + "/" + this.maxAttempts);
            return;
        }
        this.attemptsMade++;
        console_log("address request " + this.attemptsMade + "/" + this.maxAttempts);

        play(this.requestAudio);
        this.addressRecordName = "addr_arriv_" + session.getVariable("uuid") + ".wav";
        session.execute("record", "$${recordings_dir}/" + this.addressRecordName + " 10 400 3");
        var recordMs = session.getVariable("record_ms");
        var recordSamples = session.getVariable("record_samples");
        console_log("recording finished; record ms = " + recordMs + "; record samples = " + recordSamples);
        if (recordMs > 0) {
            this.sendRecordedEvent();
            this.startListen();
            // user got immediate feedback, while recognition performed in background
            play(this.checkingAddressAudio);
        } else {
            console_log("error", "no record (record_ms=0), repeat")
            return this.run();
        }

        this.waitForAddressRecognized();
        if (!this.addressRecognized){
            console_log("address not recognized, repeat");
            return this.run();
        }

        play(this.arrivalOnAudio);
        play("$${recordings_dir}/" + this.recognizedAddressRecord);

        var validationRequest = new YnRequest("validateAddress",this.isAllRightAudio,3, 1);
        validationRequest.run();
        console_log("validation result = "+validationRequest.result);
    }

    this.sendRecordedEvent = function () {
        console_log("firing recorded event")
        e = new Event("custom", "message");
        e.addHeader("sessionUuid", session.getVariable("uuid"));
        e.addHeader("messageType", "ADDRESS_RECORDED")
        e.addHeader("service", "addressService");
        e.addHeader("city", this.city);
        e.addHeader("record", this.addressRecordName);
        e.fire();
    }

    this.startListen = function () {
        this.eventHandler = new EventHandler('CUSTOM');
        this.eventHandler.addFilter("sessionUuid", session.getVariable("uuid"));
    }

    this.waitForAddressRecognized = function () {
        var evt = this.eventHandler.getEvent(20000);
        console_log("event catched = " + evt.serialize());
        this.recognizedAddressRecord = evt.getHeader("recognizedAddress");
        this.addressRecognized = true;

        this.eventHandler.destroy();
    }

}

// listeners
function ignoreInput(session, type, data, arg) {
    if (type == "dtmf") {
        console_log("data=" + JSON.stringify(data));
    }
    return true;
}

// arg = request
function onInput(session, type, data, request) {
    //console_log("processing onInput, type=" + type + "; data="+JSON.stringify(data));
    if (type != "event") {
        console_log("type is not event, type=" + type + "; returning");
        return true;
    }
    //console_log("processing event " + data.serialize())
    var eventName = data.getHeader("Event-Name");
    if (eventName != "DETECTED_SPEECH") {
        console_log("not detected speech event: " + eventName);
        return true;
    }
    var speechType = data.getHeader("Speech-Type");
    if (speechType == "begin-speaking") {
        console_log("begin speaking detected, do nothign");
        return true;
    }
    if (speechType != "detected-speech") {
        console_log("expected detected-speech event.speechType, but got " + speechType + "; ignore");
        return true;
    }

    var body = data.getBody();
    if (!body) {
        console_log("body is empty: " + data.serialize());
        return true;
    }

    var xml;
    var result;
    var interpretation;
    var input;
    var confidence;

    body = body.replace(/<\?.*?\?>/g, '');
    xml = new XML("<xml>" + body + "</xml>");
    result = xml.getChild('result');
    interpretation = result.getChild('interpretation');
    input = interpretation.getChild('input').data;
    confidence = interpretation.getAttribute('confidence');

    console_log("result=" + input + ";" + confidence);

    //return request.processResult(input, confidence);
    return request.updateResult(input, confidence);
}