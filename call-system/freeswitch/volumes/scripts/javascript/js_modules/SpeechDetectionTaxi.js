use('XML');

// grammar ex.: yes_no_ru.gram
function initAsr(grammar) {
    session.execute("detect_speech", "pocketsphinx yes_no_ru " + grammar);
    session.execute("detect_speech", "pause");
}

function stopDetection() {
    session.execute("detect_speech", "stop");
}

function play(audio) {
    session.execute("playback", audio);
}

function YnRequest(name, requestPhrase, repeatPhrase, timeout, retryAttempts) {
    this.name = name;
    this.requestPhrase = requestPhrase;
    this.repeatPhrase = repeatPhrase;
    this.timeout = timeout;
    this.retryAttempts = retryAttempts;
    this.attemptsMade = 0;
    this.result = "silence"
    this.recognitions = [];
    this.mostConfidentRecognition = null;

    this.reset = function () {
        this.result = "silence";
        this.attemptsMade = 0;
        this.recognitions = [];
    }

    this.run = function () {
        this.attemptsMade++;

        console_log("running request " + this.name);
        session.execute("detect_speech", "resume");
        session.streamFile(this.requestPhrase, this.onInput);
        session.collectInput(this.onInput, this, timeout);
        while ((this.result == "silence" || this.result == "unrecognized") && this.attemptsMade < this.retryAttempts) {
            this.processUnrecognized();
        }
        // if (this.result == "unrecognized") {
        //     console_log("no more recognition attemtps left; result = unrecognized");
        // }
        session.execute("detect_speech", "pause");

        var recignitionsAmount = this.recognitions.length;
        if (recignitionsAmount == 1) {
            this.mostConfidentRecognition = this.recognitions[0];
        } else if (recignitionsAmount == 2) {
            if (this.recognitions[0].confidence >= this.recognitions[1].confidence) {
                this.mostConfidentRecognition = this.recognitions[0];
            } else {
                this.mostConfidentRecognition = this.recognitions[1];
            }
        } else {
            console_log("ERROR! expected 0,1, max 2 recognitions, but found " + recignitionsAmount);
        }
    }

    this.processUnrecognized = function () {
        this.attemptsMade++;
        console_log("performing next recognition attempt; " + this.attemptsMade + "/" + this.retryAttempts);
        session.execute("detect_speech", "resume");
        session.streamFile(this.repeatPhrase, this.onInput);
        session.collectInput(this.onInput, this, timeout);
    }

    this.updateResult = function (input, confidence) {
        console_log("updating result for " + input + ";" + confidence);
        var recognition = {};
        recognition.input = input;
        recognition.confidence = confidence;
        this.recognitions.push(recognition);
        var result = this[input];
        //console_log("checking branch " + JSON.stringify(result));
        if (confidence >= result.neededConfidence) {
            console_log("enough confidence; progressing dialplan")
            this.result = result.name;
            // stop collectInput
            session.execute("detect_speech", "pause");
            return false;
        } else {
            console_log("not enough confidence; result = unrecognized");
            this.result = "unrecognized";
            return false;
        }
    }

    this.onInput=function(session, type, data, request) {
        console_log("on input request = "+request);
        //console_log("processing onInput, type=" + type + "; data="+JSON.stringify(data));
        if (type != "event") {
            console_log("type is not event, type=" + type + "; returning");
            return true;
        }
        //console_log("processing event " + data.serialize())
        var eventName = data.getHeader("Event-Name");
        if (eventName != "DETECTED_SPEECH") {
            console_log("not detected speech event: " + eventName);
            return true;
        }
        var speechType = data.getHeader("Speech-Type");
        if (speechType == "begin-speaking") {
            console_log("begin speaking detected, do nothign");
            return true;
        }
        if (speechType != "detected-speech") {
            console_log("expected detected-speech event.speechType, but got " + speechType + "; ignore");
            return true;
        }
    
        var body = data.getBody();
        if (!body) {
            console_log("body is empty: " + data.serialize());
            return true;
        }
    
        var xml;
        var result;
        var interpretation;
        var input;
        var confidence;
    
        body = body.replace(/<\?.*?\?>/g, '');
        xml = new XML("<xml>" + body + "</xml>");
        result = xml.getChild('result');
        interpretation = result.getChild('interpretation');
        input = interpretation.getChild('input').data;
        confidence = interpretation.getAttribute('confidence');
    
        console_log("result=" + input + ";" + confidence);
    
        //return request.processResult(input, confidence);
        //session.execute("break");
        if (request){
            return request.updateResult(input, confidence);
        }
        return;
    }
}

function Result(name, neededConfidence) {
    this.name = name;
    this.neededConfidence = neededConfidence;
}

function AddressRequest(requestAudio, repeatAudio, doNotUnderstandAudio, checkingAddressAudio, arrivalOnAudio, isAllRightAudio, city, maxAttempts) {
    this.requestAudio = requestAudio;
    this.repeatAudio = repeatAudio;
    this.doNotUnderstandAudio = doNotUnderstandAudio;
    this.checkingAddressAudio = checkingAddressAudio;
    this.arrivalOnAudio = arrivalOnAudio;
    this.isAllRightAudio = isAllRightAudio;
    this.city = city;
    this.maxAttempts = maxAttempts;

    this.isAllRightRequest = new YnRequest("isAllRight", this.isAllRightAudio, this.repeatAudio, 2000, 1);
    var yesRes = new Result("да", 85);
    this.isAllRightRequest["да"] = yesRes;
    this.isAllRightRequest["верно"] = yesRes;
    this.isAllRightRequest["нет"] = new Result("нет", 85);

    this.eventHandler = null;

    this.addressRecognized = false;
    this.addressRecordName;

    this.attemptsMade = 0;

    this.result;

    this.run = function () {
        if (!session.ready()){
            session.hangup();
            return;
        }
        this.result = null;
        this.addressRecognized = false;
        this.isAllRightRequest.reset();
        if (this.attemptsMade >= this.maxAttempts) {
            console_log("attemtps exceeded: " + this.attemptsMade + "/" + this.maxAttempts);
            return;
        }
        this.attemptsMade++;
        console_log("address request " + this.attemptsMade + "/" + this.maxAttempts);

        if (this.attemptsMade == 1) {
            play(this.requestAudio);
        } else {
            play(this.repeatAudio);
        }

        this.addressRecordName = "addr_arriv_" + session.getVariable("uuid") + ".wav";
        session.execute("record", "$${recordings_dir}/" + this.addressRecordName + " 10 400 2");
        var recordMs = session.getVariable("record_ms");
        var recordSamples = session.getVariable("record_samples");
        console_log("recording finished; record ms = " + recordMs + "; record samples = " + recordSamples);
        if (recordMs > 0) {
            this.sendRecordedEvent();
            this.startListen();
            // user got immediate feedback, while recognition performed in background
            play(this.checkingAddressAudio);
        } else {
            console_log("error", "no record (record_ms=0), repeat")
            return this.run();
        }

        this.waitForAddressRecognized();
        if (!this.addressRecognized) {
            console_log("address not recognized, repeat");
            play(this.doNotUnderstandAudio);
            return this.run();
        }

        play(this.arrivalOnAudio);
        play("$${recordings_dir}/" + this.recognizedAddressRecord);

        this.isAllRightRequest.run();
        console_log("validation result = " + this.isAllRightRequest.result);
        if (this.isAllRightRequest.result == "нет") {
            return this.run();
        } else if (this.isAllRightRequest.result == "silence") {
            console_log("silence considered as yes");
        } else if (this.isAllRightRequest.result == "unrecognized") {
            if (this.isAllRightRequest.mostConfidentRecognition.input == "нет") {
                var input = this.isAllRightRequest.mostConfidentRecognition.input;
                var confidence = this.isAllRightRequest.mostConfidentRecognition.confidence;
                console_log("unsure not considered as not: " + input + "(" + confidence + ")");
                return this.run();
            } else {
                var input = this.isAllRightRequest.mostConfidentRecognition.input;
                var confidence = this.isAllRightRequest.mostConfidentRecognition.confidence;
                console_log("unrecognized and " + input + "(" + confidence + ") accepted as yes");
            }
        }

        console_log("arrival address acquired: " + this.result);
    }

    this.sendRecordedEvent = function () {
        console_log("firing recorded event")
        e = new Event("custom", "message");
        e.addHeader("sessionUuid", session.getVariable("uuid"));
        e.addHeader("messageType", "ADDRESS_RECORDED");
        e.addHeader("service", "addressService");
        e.addHeader("city", this.city);
        e.addHeader("record", this.addressRecordName);
        e.fire();
    }

    this.startListen = function () {
        this.eventHandler = new EventHandler('CUSTOM');
        this.eventHandler.addFilter("sessionUuid", session.getVariable("uuid"));
        this.eventHandler.addFilter("messageType", "ADDRESS_RECOGNITION_RESULT");
    }

    this.waitForAddressRecognized = function () {
        var evt = this.eventHandler.getEvent(5000);
        if (!evt) {
            console_log("timeout");
            this.addressRecognized = false;
            this.result = null;
            this.eventHandler.destroy();
            return;
        }

        console_log("event catched = " + evt.serialize());
        if (evt.getHeader("result") == "SUCCESS") {
            this.recognizedAddressRecord = evt.getHeader("recognizedAddress");
            this.addressRecognized = true;
            this.result = {};
            this.result.fullAddress = evt.getHeader("fullAddress");
            this.result.city = evt.getHeader("city");
            this.result.street = evt.getHeader("street");
            this.result.house = evt.getHeader("house");
        } else {
            this.addressRecognized = false;
            this.result = null;
        }

        this.eventHandler.destroy();
    }

}

function RecordRequest(requestAudio, thanksAudio, name, timeout, silence) {
    this.requestAudio = requestAudio;
    this.thanksAudio = thanksAudio;
    this.name = name;
    this.timeout = timeout;
    this.silence = silence;

    this.eventHandler = null;

    this.recordName = null;

    this.recognized = false;
    this.result = null;

    this.run = function () {
        play(this.requestAudio);
        session.execute("sleep", 200);
        play("common/beep2.wav");

        this.recordName = this.name + "_" + session.getVariable("uuid") + ".wav";
        session.execute("record", "$${recordings_dir}/" + this.recordName + " " + this.timeout + " 400 " + this.silence);

        var recordMs = session.getVariable("record_ms");
        if (recordMs > 0) {
            this.sendRecordedEvent();
            this.startListen();
            play(this.thanksAudio);

            this.waitForRecordRecognized();
        };
    }

    this.sendRecordedEvent = function () {
        console_log("firing recorded event")
        e = new Event("custom", "message");
        e.addHeader("sessionUuid", session.getVariable("uuid"));
        e.addHeader("messageType", "CUSTOM_RECORD");
        e.addHeader("name", this.name);
        e.addHeader("service", "recordRecognitionService");
        e.addHeader("record", this.recordName);
        e.fire();
    }

    this.startListen = function () {
        this.eventHandler = new EventHandler('CUSTOM');
        this.eventHandler.addFilter("sessionUuid", session.getVariable("uuid"));
        this.eventHandler.addFilter("messageType", "RECORD_RECOGNITION_RESULT");
    }

    this.waitForRecordRecognized = function () {
        var evt = this.eventHandler.getEvent(5000);
        if (!evt) {
            console_log("timeout");
            this.recognized = false;
            this.result = null;
            this.eventHandler.destroy();
            return;
        }

        console_log("event catched = " + evt.serialize());
        if (evt.getHeader("result") == "SUCCESS") {
            this.recognized = true;
            this.result = evt.getHeader("recognition");
            console_log("record" + this.name + " recognized: " + this.result);
        } else {
            this.recognized = false;
            this.result = null;
            console_log("record" + this.name + " not recognized");
        }

        this.eventHandler.destroy();
    }
}

function ServerRequest(requestAudio, service, messageType, headers, responseTimeout, headersExtractOnSuccess) {
    this.requestAudio = requestAudio;
    this.messageType = messageType;
    this.service = service;
    this.headers = headers;
    this.responseTimeout = responseTimeout;
    this.headersExtractOnSuccess = headersExtractOnSuccess;

    this.result = null;
    this.error = null;
    this.successHeaders = {};

    this.fireRequestEvent = function () {
        e = new Event("custom", "message");
        e.addHeader("sessionUuid", session.getVariable("uuid"));
        e.addHeader("messageType", this.messageType);
        e.addHeader("service", this.service);

        if (this.headers) {
            this.headers.forEach(function (header) {
                e.addHeader(header.name, header.value);
            });
        }
        e.fire();
        console_log("event fired; service = " + this.service + "; messageType = " + this.messageType);
    }

    this.startListenResponseEvent = function () {
        this.eventHandler = new EventHandler('CUSTOM');
        this.eventHandler.addFilter("sessionUuid", session.getVariable("uuid"));
        this.eventHandler.addFilter("messageType", this.messageType);
    }

    this.waitForResponseEvent = function () {
        var evt = this.eventHandler.getEvent(this.responseTimeout);
        if (!evt) {
            console_log("event timeout; service = " + this.service + "; messageType = " + this.messageType);
            this.eventHandler.destroy();
            this.error = "timeout";
            return;
        }
        console_log("event catched = " + evt.serialize());
        this.result = evt.getHeader("result");
        if (this.result == "false") {
            this.result = false;
        }
        this.eventHandler.destroy();
        if (this.result && this.headersExtractOnSuccess) {
            var _this = this;
            this.headersExtractOnSuccess.forEach(function (header) {
                var value = evt.getHeader(header);
                console_log("extracting additional header on success: " + header + ":" + value);
                _this.successHeaders[header] = value;
            });
        }
    }

    this.run = function () {
        this.startListenResponseEvent();
        this.fireRequestEvent();
        play(this.requestAudio);
        this.waitForResponseEvent();
        console_log("ServerRequest finished with result = " + this.result);
        return this.result;
    }
}

// listeners
function ignoreInput(session, type, data, arg) {
    if (type == "dtmf") {
        console_log("data=" + JSON.stringify(data));
    }
    return true;
}

// arg = request
