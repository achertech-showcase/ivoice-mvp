include("js_modules/SpeechTools.js");

function on_dtmf(a, b, c) { }

var dft_min = 70;
var dft_confirm = 70;

/***************** Initialize The Speech Detector  *****************/
var asr = new SpeechDetect(session, "pocketsphinx");

/***************** Be more verbose *****************/
asr.debug = 1;

/***************** Set audio params *****************/
asr.setAudioBase("drivers-dev/");
asr.setAudioExt(".wav");

/***************** Unload the last grammar whenever we activate a new one *****************/
asr.AutoUnload = true;

/***************** Create And Configure The Pizza *****************/
var dialog = new Object();

dialog.firstRequest = new SpeechObtainer(asr, 1, 5000);
dialog.firstRequest.setGrammar("yes_no_ru", "", "result.getChild('interpretation').getChild('input').data", dft_min, dft_confirm, true);
dialog.firstRequest.setTopSound("1_greet_request");
dialog.firstRequest.setBadSound("0_please_repeat");
dialog.firstRequest.addItemAlias("^да", "yes");
dialog.firstRequest.addItemAlias("^нет", "no");
dialog.firstRequest.setMaxAttempts(3);

dialog.secondRequest = new SpeechObtainer(asr, 1, 5000);
dialog.secondRequest.setGrammar("yes_no_ru", "", "result.getChild('interpretation').getChild('input').data", dft_min, dft_confirm, true);
dialog.secondRequest.setTopSound("2_details_question");
dialog.secondRequest.setBadSound("0_please_repeat");
dialog.secondRequest.addItemAlias("^да", "yes");
dialog.secondRequest.addItemAlias("^нет", "no");
dialog.secondRequest.setMaxAttempts(3);

/***************** Tie It All Together *****************/
dialog.run = function () {
    if (!session.ready()) {
        console_log("session not ready");
        return false;
    }
    if (!session.answered()){
        console_log("session not answered");
        return false;    
    }

    // first request
    var request1result = false;
    var resultsStr="интересует ли предложение - ";
    items = dialog.firstRequest.run();
    if (items[0] == "yes") {
        console_log("1 request result - yes");
        request1result = true;
        resultsStr += "да";
    } else if (items[0] == "no") {
        console_log("1 request result - no");
        request1result = false;
        resultsStr += "нет";
    } else {
        console_log("1 request result - not recognized");
        resultsStr += "ошибка распознавания";
    }

    if (!request1result){
        asr.streamFile("4_thanks_fail");
        return;
    }

    // second request
    var detailsResult=false;
    resultsStr+="; есть ли автомобиль - "
    items = dialog.secondRequest.run();
    if (items[0] == "yes") {
        console_log("2 details result - yes");
        detailsResult = true;
        resultsStr += "да";
    } else if (items[0] == "no") {
        console_log("2 details result - no");
        detailsResult = false;
        resultsStr += "нет";
    } else {
        console_log("2 details result - not recognized");
        resultsStr += "ошибка распознавания";
    }

    session.setVariable("results",resultsStr);
    console_log("results = "+resultsStr);

    if (!detailsResult){
        asr.streamFile("4_thanks_fail");
        return;
    }

    asr.streamFile("3_thanks_success");

};

/***************** Begin Program *****************/
session.answer();
dialog.run();
asr.stop();
