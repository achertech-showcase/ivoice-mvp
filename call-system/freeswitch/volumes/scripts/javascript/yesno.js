include("js_modules/SpeechTools.js");

function on_dtmf(a, b, c) { }

var dft_min = 70;
var dft_confirm = 70;

/***************** Initialize The Speech Detector  *****************/
var asr = new SpeechDetect(session, "pocketsphinx");

/***************** Be more verbose *****************/
asr.debug = 1;

/***************** Set audio params *****************/
asr.setAudioBase("yes_no_ru/");
asr.setAudioExt(".wav");

/***************** Unload the last grammar whenever we activate a new one *****************/
asr.AutoUnload = true;

/***************** Create And Configure The Pizza *****************/
var dialog = new Object();

/***************** Yes? No? Maybe So?  *****************/
dialog.yesnoObtainer = new SpeechObtainer(asr, 1, 5000);
dialog.yesnoObtainer.setGrammar("yes_no_ru", "", "result.getChild('interpretation').getChild('input').data", dft_min, 20, true);
dialog.yesnoObtainer.setTopSound("no_yes");
dialog.yesnoObtainer.setBadSound("repeat");
dialog.yesnoObtainer.addItemAlias("^да", "yes");
dialog.yesnoObtainer.addItemAlias("^нет", "no");

/***************** Tie It All Together *****************/
dialog.run = function () {
    if (!session.ready()) {
        console_log("session not ready");
        return false;
    } else {
        console_log("session ready");
    }

    console_log("taskId from script = "+session.getVariable("taskId"));

    var msg = "Hello, welcome to the FreeSWITCH demo application";
    e = new Event("custom", "message");
    e.addBody(msg);
    e.fire();

    for (i=0; i<5; i++) {
        if (!session.ready()) {
            break;
        }
        //dialog.yesnoObtainer.reset();
        items = dialog.yesnoObtainer.run();
        console_log("items = "+items);
	    if (items[0] == "yes") {
            console_log("yes branch");  
            asr.streamFile("answer_yes");
        } else {
            console_log("no branch"); 
            asr.streamFile("answer_no");
        }
        return;
    }
};

/***************** Begin Program *****************/
session.answer();
dialog.run();
asr.stop();
