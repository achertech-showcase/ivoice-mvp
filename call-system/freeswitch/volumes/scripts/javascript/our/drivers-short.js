use('XML');

// audio files
ext = ".wav";
audioParent = "drivers/";

yesNoAudio = audioParent + "0_yes_no" + ext;
pleaseRepeatAudio = audioParent + "0_please_repeat" + ext;
request1Audio = audioParent + "1_greet_request_already_connected" + ext;
//request1Audio = audioParent + "1_greet_request_short" + ext;
successAudio = audioParent + "3_thanks_success" + ext;
failAudio = audioParent + "4_thanks_fail" + ext;

// lib
function YnRequest(name, requestPhrase, timeout, retryAttempts) {
    this.name = name;
    this.timeout = timeout;
    this.retryAttempts = retryAttempts;
    this.attemptsMade = 0;
    this.result = "silence"
    this.recognitions = [];
    this.mostConfidentRecognition = null;

    this.run = function () {
        this.attemptsMade++;

        console_log("running request " + this.name);
        session.streamFile(requestPhrase, ignoreInput);
        session.execute("detect_speech", "resume");
        session.streamFile(yesNoAudio, onInput);
        session.collectInput(onInput, this, timeout);
        while ((this.result == "silence" || this.result == "unrecognized") && this.attemptsMade < this.retryAttempts) {
            this.processUnrecognized();
        }
        // if (this.result == "unrecognized") {
        //     console_log("no more recognition attemtps left; result = unrecognized");
        // }
        session.execute("detect_speech", "pause");

        var recignitionsAmount = this.recognitions.length;
        if (recignitionsAmount == 1) {
            this.mostConfidentRecognition = request1.recognitions[0];
        } else if (recignitionsAmount == 2) {
            if (request1.recognitions[0].confidence >= request1.recognitions[1].confidence) {
                this.mostConfidentRecognition = request1.recognitions[0];
            } else {
                this.mostConfidentRecognition = request1.recognitions[1];
            }
        } else {
            console_log("ERROR! expected 0,1, max 2 recognitions, but found " + recignitionsAmount);
        }
    }

    this.processUnrecognized = function () {
        this.attemptsMade++;
        console_log("performing next recognition attempt; " + this.attemptsMade + "/" + this.retryAttempts);
        session.execute("detect_speech", "resume");
        session.streamFile(pleaseRepeatAudio, onInput);
        session.collectInput(onInput, this, timeout);
    }

    this.updateResult = function (input, confidence) {
        console_log("updating result for " + input + ";" + confidence);
        var recognition = {};
        recognition.input = input;
        recognition.confidence = confidence;
        this.recognitions.push(recognition);
        var result = this[input];
        //console_log("checking branch " + JSON.stringify(result));
        if (confidence >= result.neededConfidence) {
            console_log("enough confidence; progressing dialplan")
            this.result = result.name;
            // stop collectInput
            session.execute("detect_speech", "pause");
            return false;
        } else {
            console_log("not enough confidence; result = unrecognized");
            this.result = "unrecognized";
            return false;
        }
    }
}

function Phrase(name, phrase, final) {
    this.name = name;
    this.phrase = phrase;
    this.run = function () {
        if (final) {
            session.execute("detect_speech", "stop");
        }
        console_log("running phrase " + this.name);
        session.streamFile(phrase, ignoreInput);
        if (final) {
            session.hangup();
        }
    }
}

function Result(name, neededConfidence) {
    this.name = name;
    this.neededConfidence = neededConfidence;
}

// state
this.request1 = new YnRequest("first request", request1Audio, 3000, 2);
var yesRes = new Result("да", 85);
this.request1["да"] = yesRes;
this.request1["готов"] = yesRes;
this.request1["готова"] = yesRes;
this.request1["нет"] = new Result("нет", 85);

this.finishFail = new Phrase("finish fail", failAudio, true);
this.finishSuccess = new Phrase("finish success", successAudio, true);

// listeners
function ignoreInput(session, type, data, arg) {
    if (type == "dtmf") {
        console_log("data=" + JSON.stringify(data));
    }
    return true;
}

// arg = request
function onInput(session, type, data, request) {
    //console_log("processing onInput, type=" + type + "; data="+JSON.stringify(data));
    if (type != "event") {
        console_log("type is not event, type=" + type + "; returning");
        return true;
    }
    //console_log("processing event " + data.serialize())
    var eventName = data.getHeader("Event-Name");
    if (eventName != "DETECTED_SPEECH") {
        console_log("not detected speech event: " + eventName);
        return true;
    }
    var speechType = data.getHeader("Speech-Type");
    if (speechType == "begin-speaking") {
        console_log("begin speaking detected, do nothign");
        return true;
    }
    if (speechType != "detected-speech") {
        console_log("expected detected-speech event.speechType, but got " + speechType + "; ignore");
        return true;
    }

    var body = data.getBody();
    if (!body) {
        console_log("body is empty: " + data.serialize());
        return true;
    }

    var xml;
    var result;
    var interpretation;
    var input;
    var confidence;

    body = body.replace(/<\?.*?\?>/g, '');
    xml = new XML("<xml>" + body + "</xml>");
    result = xml.getChild('result');
    interpretation = result.getChild('interpretation');
    input = interpretation.getChild('input').data;
    confidence = interpretation.getAttribute('confidence');

    console_log("result=" + input + ";" + confidence);

    //return request.processResult(input, confidence);
    return request.updateResult(input, confidence);
}

initAsr = function () {
    session.execute("detect_speech", "pocketsphinx yes_no_ru yes_no_ru.gram");
    session.execute("detect_speech", "pause");
}

// dialplan
if (session.ready()) {
    session.answer();
    if (!session.answered()) {
        console_log("session not answered; finishing");
        return;
    }
    var calltaskid = session.getVariable("calltaskid");
    if (!calltaskid) {
        console_log("calltaskid undefined; finishing");
        return;
    }
    session.execute("record_session", "$${recordings_dir}/" + calltaskid + ".wav");

    initAsr();

    var results = "готовы ли рассмотреть предложение - "

    request1.run();
    console_log("result 1 = " + request1.result);

    session.setVariable("answersAmount", 1); // 1 request - 1 answer
    session.setVariable("recognitions", JSON.stringify(request1.recognitions));

    if (request1.result == "unrecognized") {
        var mostConfidentResult = request1[request1.mostConfidentRecognition.input];
        console_log("result unrecognized. Accepting most confident recognition as result: "+JSON.stringify(request1.mostConfidentRecognition));
        request1.result = mostConfidentResult.name;
    }

    if (request1.result == "silence") {
        results += "молчание";
        session.setVariable("results", results);

        this.finishFail.run();
    } else if (request1.result == "нет") {
        results += "нет";
        session.setVariable("results", results);
        session.setVariable("confidence", request1.mostConfidentRecognition.confidence);

        this.finishFail.run();
    } else if (request1.result == "да") {
        results += "да";
        session.setVariable("results", results);
        session.setVariable("confidence", request1.mostConfidentRecognition.confidence);

        this.finishSuccess.run();
    } else {
        results += "программная ошибка";
        session.setVariable("results", results);

        this.finishFailure.run();
    }
}

