include("../js_modules/SpeechDetectionTaxi.js");
include("../js_modules/base64.min.js");
use('EventHandler');

// audio files
ext = ".wav";
commonsParent = "common/";
taxiParent = "taxi/";

yesNoAudio = commonsParent + "0_yes_no_long" + ext;
pleaseRepeatAudio = commonsParent + "0_please_repeat" + ext;
thanksAudio = commonsParent + "0_thanks" + ext;

arrivalAddressRequestAudio = taxiParent + "23_request_arrival_address" + ext;
arrivalAddressRepeatAudio = taxiParent + "24_request_arrival_address_detailed" + ext;
arrivalOnAudio = taxiParent + "23_arrival_on" + ext;
notUnderstandAddress = taxiParent + "29_do_not_understand_address" + ext;
requestCommentsAudio = taxiParent + "81_request_comments" + ext;

cantProceedAudio = taxiParent + "9_sorry_cant_proceed" + ext;

greetAudio = taxiParent + "1hello" + ext;
conditionsAudio = taxiParent + "1_conditions" + ext;
thanksCheckAddressAudio = commonsParent + "0_thanks_check_address" + ext;
orderCreatedAudio = taxiParent+"9_order_created_thanks" + ext;
isAllValidAudio = commonsParent + "0_is_all_right" + ext;

// requests
requestAddress = new AddressRequest(arrivalAddressRequestAudio, arrivalAddressRepeatAudio, notUnderstandAddress, thanksCheckAddressAudio, arrivalOnAudio, isAllValidAudio, "Кемерово", 3);
requestComments = new RecordRequest(requestCommentsAudio, thanksAudio, "comments", 15, 2);

this.order = {};
this.orderCreatedOnServer = false;

this.fireTaxiOrderCreated = function (order) {
    e = new Event("custom", "message");
    e.addHeader("sessionUuid", session.getVariable("uuid"));
    e.addHeader("messageType", "ORDER_CREATED");
    e.addHeader("service", "taxiOrderService");
    var json = JSON.stringify(order);
    var jsonBase64 = Base64.encode(json);
    e.addBody(jsonBase64);
    e.fire();
    console_log("order event fired");
}

this.startListenOrderCreated = function () {
    this.eventHandler = new EventHandler('CUSTOM');
    this.eventHandler.addFilter("sessionUuid", session.getVariable("uuid"));
    this.eventHandler.addFilter("messageType", "ORDER_CREATE_RESULT");
}

this.waitForOrderCreate = function () {
    var evt = this.eventHandler.getEvent(5000);
    if (!evt) {
        console_log("timeout");
        this.orderCreatedOnServer = false;
        this.eventHandler.destroy();
        return;
    }
    console_log("event catched = " + evt.serialize());
    this.orderCreatedOnServer = evt.getHeader("result") == "SUCCESS";
    this.eventHandler.destroy();
}

// dialplan
if (session.ready()) {
    session.answer();

    this.order.phone = session.getVariable("caller_id_number");

    console_log("uuid = " + session.getVariable("uuid"));
    initAsr("taxi_yes_no.gram");

    // TODO: заказы принимаются на ближайшее время, оплата наличными
    play(greetAudio);
    play(conditionsAudio);

    requestAddress.run();
    stopDetection();
    if (!requestAddress.addressRecognized) {
        play(cantProceedAudio);
        session.hangup();
    }
    this.order.arrivalAddress = requestAddress.result;

    requestComments.run();
    if (requestComments.recognized) {
        this.order[requestComments.name] = requestComments.result;
    }

    this.startListenOrderCreated();
    this.fireTaxiOrderCreated(this.order);
    this.waitForOrderCreate();
    console_log("order created = " + this.orderCreatedOnServer);
    if (this.orderCreatedOnServer){
        play(orderCreatedAudio);
    } else {
        play(cantProceedAudio);
    }

    session.hangup();
}