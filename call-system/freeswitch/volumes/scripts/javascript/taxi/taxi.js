include("../js_modules/SpeechDetectionTaxi.js");
include("../js_modules/base64.min.js");
use('EventHandler');

// audio files
ext = ".wav";
commonsParent = "common/";
taxiParent = "taxi/";

yesNoAudio = commonsParent + "0_yes_no_long" + ext;
pleaseRepeatAudio = commonsParent + "0_please_repeat" + ext;
thanksAudio = commonsParent + "0_thanks" + ext;

arrivalAddressRequestAudio = taxiParent + "23_request_arrival_address" + ext;
arrivalAddressRepeatAudio = taxiParent + "24_request_arrival_address_detailed" + ext;
arrivalOnAudio = taxiParent + "23_arrival_on" + ext;
notUnderstandAddress = taxiParent + "29_do_not_understand_address" + ext;
requestCommentsAudio = taxiParent + "81_request_comments" + ext;

cantProceedAudio = taxiParent + "9_sorry_cant_proceed" + ext;

greetAudio = taxiParent + "1hello" + ext;
conditionsAudio = taxiParent + "1_conditions" + ext;
thanksCheckAddressAudio = commonsParent + "0_thanks_check_address" + ext;
placingOrderAudio = taxiParent + "9_placing_order" + ext;
orderPlacedAudio = taxiParent + "9_order_created_thanks" + ext;
isAllValidAudio = commonsParent + "0_is_all_right" + ext;
thanksForWaitingAudio = commonsParent + "0_thanks_waiting" + ext;
repeatOnlyAudio = commonsParent + "0_repeat_only" + ext;
thanksBuyAudio = commonsParent + "0_common_thanks_buy" + ext;

// order states
requestOrderStateAudio = taxiParent + "5_request_order_status" + ext;
stateAwaitDriverAcceptAudio = taxiParent + "11_order_in_state_await_driver_accept" + ext;
stateCancelledAudio = taxiParent + "11_order_cancelled_start_new_order" + ext;
stateArrivingAudio = taxiParent + "11_order_state_driver_arriving" + ext;
stateDrivingAudio = taxiParent + "11_order_state_driving" + ext;

// menu
menuAudio = taxiParent + "11_menu" + ext;
cancellingAudio = taxiParent + "12_cancelling" + ext;
cancellingSuccessAudio = taxiParent + "12_cancelling_success" + ext;
connectingDispatcherAudio = taxiParent + "12_connecting_dispatcher" + ext;

orderStateAudio = {
    "CREATED": stateAwaitDriverAcceptAudio,
    "CANCELLED": stateCancelledAudio,
    "ARRIVING": stateArrivingAudio,
    "DRIVING": stateDrivingAudio
}

var city = "Кемерово"

// requests
requestAddress = new AddressRequest(arrivalAddressRequestAudio, arrivalAddressRepeatAudio, notUnderstandAddress, thanksCheckAddressAudio, arrivalOnAudio, isAllValidAudio, city, 3);
requestComments = new RecordRequest(requestCommentsAudio, thanksAudio, "comments", 15, 2);

menuRequest = new YnRequest("menu", menuAudio, repeatOnlyAudio, 3000, 2);
var cancelResult = new Result("отмена", 50);
//отмена | отменит | отмените | отменить | отменю | отменяем
menuRequest["отмена"] = cancelResult;
menuRequest["отменит"] = cancelResult;
menuRequest["отмените"] = cancelResult;
menuRequest["отменить"] = cancelResult;
menuRequest["отменю"] = cancelResult;
menuRequest["отменяем"] = cancelResult;

menuRequest["диспетчер"] = new Result("диспетчер", 50);

this.order = {};
this.resetOrder = function () {
    this.order = {};
    this.orderCreatedOnServer = false;
    this.orderPlaced = false;
    this.orderInProgress = false;

    // predefined order properties
    this.order.timeZoneOffset = "GMT+7";
    this.order.arrivalTimeOffset = { value: 1, unit: "DAYS" }; // unit is value of ChroneUnit java enum
    this.order.selectedOptions = {
        "rule": 3,
        "tariff": 1
    };

    this.order.phone = session.getVariable("caller_id_number");
}
this.resetOrder();

this.fireTaxiOrderCreated = function (order) {
    e = new Event("custom", "message");
    e.addHeader("sessionUuid", session.getVariable("uuid"));
    e.addHeader("messageType", "ORDER_CREATED");
    e.addHeader("service", "taxiOrderService");
    var json = JSON.stringify(order);
    var jsonBase64 = Base64.encode(json);
    e.addBody(jsonBase64);
    e.fire();
    console_log("order event fired");
}

// this.fireRequestOrderInProgress = function (order) {
//     e = new Event("custom", "message");
//     e.addHeader("sessionUuid", session.getVariable("uuid"));
//     e.addHeader("messageType", "REQUEST_ORDER_IN_PROGRESS");
//     e.addHeader("service", "taxiOrderInProgressService");
//     var phone = session.getVariable("caller_id_number");
//     e.addHeader("phone", phone);
//     e.fire();
//     console_log("request order in progress event fired");
// }

this.startListenOrderCreated = function () {
    this.eventHandler = new EventHandler('CUSTOM');
    this.eventHandler.addFilter("sessionUuid", session.getVariable("uuid"));
    this.eventHandler.addFilter("messageType", "ORDER_CREATE_RESULT");
}

this.startListenOrderPlaced = function () {
    this.eventHandler = new EventHandler('CUSTOM');
    this.eventHandler.addFilter("sessionUuid", session.getVariable("uuid"));
    this.eventHandler.addFilter("messageType", "ORDER_PLACING_RESULT");
}

// this.startListenOrderInProgressRequestResult = function () {
//     this.eventHandler = new EventHandler('CUSTOM');
//     this.eventHandler.addFilter("sessionUuid", session.getVariable("uuid"));
//     this.eventHandler.addFilter("messageType", "REQUEST_ORDER_IN_PROGRESS_RESULT");
// }

this.waitForOrderCreate = function () {
    var evt = this.eventHandler.getEvent(15000);
    if (!evt) {
        console_log("order create on server timeout");
        this.orderCreatedOnServer = false;
        this.eventHandler.destroy();
        return;
    }
    console_log("event catched = " + evt.serialize());
    this.orderCreatedOnServer = evt.getHeader("result") == "SUCCESS";
    this.eventHandler.destroy();
}

this.waitForOrderPlaced = function () {
    var evt = this.eventHandler.getEvent(30000);
    if (!evt) {
        console_log("order placing timeout");
        this.orderPlaced = false;
        this.eventHandler.destroy();
        return;
    }
    console_log("event catched = " + evt.serialize());
    this.orderPlaced = evt.getHeader("result") == "SUCCESS";
    this.eventHandler.destroy();
}

this.processNewOrder = function () {
    play(conditionsAudio);

    requestAddress.run();
    stopDetection();
    if (!requestAddress.addressRecognized) {
        play(cantProceedAudio);
        session.hangup();
        return;
    }
    this.order.arrivalAddress = requestAddress.result;

    requestComments.run();
    if (requestComments.recognized) {
        this.order[requestComments.name] = requestComments.result;
    }

    if (!session.ready()) {
        session.hangup();
        return;
    }

    this.startListenOrderCreated();
    this.fireTaxiOrderCreated(this.order);
    this.waitForOrderCreate();
    console_log("order created = " + this.orderCreatedOnServer);
    if (this.orderCreatedOnServer) {
        this.startListenOrderPlaced();
        play(placingOrderAudio);
        this.waitForOrderPlaced();
        if (this.orderPlaced) {
            play(orderPlacedAudio);
        } else {
            play(cantProceedAudio);
        }
    } else {
        play(cantProceedAudio);
    }

    session.hangup();
}

this.cancelOrder = function () {
    var cancelOrderRequest = new ServerRequest(
        cancellingAudio,
        "orderCancelService",
        "CANCEL_ORDER",
        [{ name: "orderId", value: this.order.id }],
        30000
    )
    cancelOrderRequest.run();
    if (cancelOrderRequest.result) {
        console_log("order state = " + requestOrderInProgress.successHeaders.state);
        play(cancellingSuccessAudio);
        play(thanksBuyAudio);
    } else {
        console_log("failed to cancel");
        play(cantProceedAudio);
    }
    session.hangup();
}

this.connectDispatcher = function () {
    play(connectingDispatcherAudio);
    session.hangup();
}

this.orderInProgressMenu = function () {
    session.execute("detect_speech", "pocketsphinx taximenu taximenu");
    menuRequest.run();
    console_log("recognition result=" + menuRequest.result);
    var result = menuRequest.result;
    if (result == "unrecognized") {
        if (menuRequest.recognitions && menuRequest.recognitions.length == 2) {
            var rec1 = menuRequest.recognitions[0];
            var rec2 = menuRequest.recognitions[1];
            if (rec1.input == rec2.input) {
                result = rec1.input;
            }
        }
    }
    console_log("menu result=" + result);
    if (result == "отмена") {
        this.cancelOrder();
    } else {
        this.connectDispatcher();
    }
    return;
}

this.processOrderInProgress = function () {
    var requestOrderInProgress = new ServerRequest(
        requestOrderStateAudio,
        "taxiOrderInProgressService",
        "REFRESH_ORDER_STATE",
        [{ name: "orderId", value: this.order.id }],
        40000,
        ["state"]
    )
    requestOrderInProgress.run();
    if (requestOrderInProgress.result) {
        console_log("order state = " + requestOrderInProgress.successHeaders.state);
        play(thanksForWaitingAudio);
        play(orderStateAudio[requestOrderInProgress.successHeaders.state]);
        if (requestOrderInProgress.successHeaders.state == "CANCELLED") {
            this.resetOrder();
            this.processNewOrder();
            return;
        } else {
            this.orderInProgressMenu();
            return;
        }
    } else {
        console_log("can't ack state");
    }
}

// dialplan
if (session.ready()) {
    session.answer();
    if (!session.answered()) {
        console_log("session not answered; finishing");
        return;
    }

    session.execute("detect_speech", "pocketsphinx taxi_yes_no_ru taxi_yes_no");
    session.execute("detect_speech", "pause");

    this.order.phone = session.getVariable("caller_id_number");

    console_log("uuid = " + session.getVariable("uuid"));

    var requestOrderInProgress = new ServerRequest(
        greetAudio,
        "taxiOrderInProgressService",
        "REQUEST_ORDER_IN_PROGRESS",
        [{ name: "phone", value: this.order.phone }],
        2000,
        ["orderId"]
    );
    this.orderInProgress = requestOrderInProgress.run();
    if (requestOrderInProgress.error) {
        play(cantProceedAudio);
        session.hangup();
        return;
    }

    console_log("order in progress = " + this.orderInProgress);
    if (!this.orderInProgress) {
        console_log("no order in progress, processing new order");
        this.processNewOrder();
    } else {
        this.order.id = requestOrderInProgress.successHeaders.orderId;
        console_log("order in progress detected, requesting status; orderId = " + this.order.id);
        this.processOrderInProgress();
    }


}