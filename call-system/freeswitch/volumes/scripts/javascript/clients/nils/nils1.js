include("../../js_modules/SpeechDetection.js");

// audio files
ext = ".wav";
commonsParent = "common/"
audioParent = "clients/nils/";

yesNoAudio = commonsParent + "0_yes_no_long" + ext;
pleaseRepeatAudio = commonsParent + "0_please_repeat" + ext;

//request1Audio = commonsParent + "1_greet_request_short" + ext;
//request2Audio = commonsParent + "2_details_question_short" + ext;
request1Audio = audioParent + "1_greet_request" + ext;
request2Audio = audioParent + "2_request_contacts" + ext;

successAudio = commonsParent + "0_success_short_spec_will_call" + ext;
failAudio = commonsParent + "4_thanks_fail" + ext;
beep = commonsParent + "0_beep" + ext;

// state
this.request1 = new YnRequest("first request", request1Audio, 3000, 1);
var yesRes = new Result("да", 85);
this.request1["да"] = yesRes;
this.request1["интересно"] = yesRes;
this.request1["нет"] = new Result("нет", 85);

this.finishFail = new Phrase("finish fail", failAudio, true);
this.finishSuccess = new Phrase("finish success", successAudio);
this.requestContact = new Phrase("request contact", request2Audio);

this.calltaskid;

function onInterest(){
    console_log("interest");
    this.requestContact.run();
    console_log("processing details");
    //session.execute("playback", beep);

    var record = this.calltaskid+"_contact.wav";
    session.execute("record","$${recordings_dir}/"+record+" 10 200 3");
    session.setVariable("postprocess", "recognize:"+record);
    this.finishSuccess.run();
    session.hangup();
}

// dialplan
if (session.ready()) {
    session.answer();
    if (!session.answered()) {
        console_log("session not answered; finishing");
        return;
    }
    this.calltaskid = session.getVariable("calltaskid");
    if (!this.calltaskid) {
        console_log("calltaskid undefined; finishing");
        return;
    }
    session.execute("record_session", "$${recordings_dir}/" + this.calltaskid + ".wav");
    initAsr();

    var results = "интересно ли предложение - "

    request1.run();
    stopDetection();
    console_log("result 1 = " + request1.result);

    session.setVariable("answersAmount", 1); // 1 request - 1 answer; variable means that some interaction with user occured
    session.setVariable("recognitions", JSON.stringify(request1.recognitions));

    if (request1.result == "silence") {
        results += "молчание";
        session.setVariable("results", results);

        onInterest();
    } else if (request1.result == "нет") {
        results += "нет";
        session.setVariable("results", results);
        session.setVariable("confidence", request1.mostConfidentRecognition.confidence);

        this.finishFail.run();
    } else if (request1.result == "да") {
        results += "да";
        session.setVariable("results", results);
        session.setVariable("confidence", request1.mostConfidentRecognition.confidence);

        onInterest();
    } else {
        var mostConfidentResult = request1[request1.mostConfidentRecognition.input];
        results += mostConfidentResult.name;
        session.setVariable("confidence", request1.mostConfidentRecognition.confidence);
        session.setVariable("results", results);

        onInterest();
    }
}

