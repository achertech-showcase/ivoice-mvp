use('XML');
use('EventHandler');

// lib
function YnRequest(name, requestPhrase, timeout, retryAttempts) {
    this.name = name;
    this.timeout = timeout;
    this.retryAttempts = retryAttempts;
    this.attemptsMade = 0;
    this.result="unrecognized"

    this.run = function () {
        this.attemptsMade++;
        console_log("running request " + this.name);
        session.streamFile(requestPhrase, ignoreInput);
        session.execute("detect_speech", "resume");
        session.streamFile("drivers-dev/0_no_or_yes.wav", onInput);
        session.collectInput(onInput, this, timeout);
        while (this.result=="unrecognized" && this.attemptsMade < this.retryAttempts) {
            this.processUnrecognized();
        }
        if (this.result=="unrecognized"){
            console_log("no more recognition attemtps left; result = unrecognized");
        }
        session.execute("detect_speech", "pause");
    }

    this.processUnrecognized = function () {
        this.attemptsMade++;
        console_log("performing next recognition attempt; " + this.attemptsMade + "/" + this.retryAttempts);
        session.streamFile("drivers-dev/0_please_repeat.wav", onInput);
        session.collectInput(onInput, this, timeout);
    }

    this.updateResult = function (input, confidence) {
        console_log("updating result for " + input + ";" + confidence);
        var result = this[input];
        console_log("checking branch " + JSON.stringify(result));
        if (confidence >= result.neededConfidence) {
            console_log("enough confidence; progressing dialplan")
            this.result = result.name;
            // stop collectInput
            session.execute("detect_speech", "pause");
            return false;
        } else {
            console_log("not enough confidence; result = unrecognized");
            this.result = "unrecognized";
            return false;
        }
    }
}

function Phrase(name, phrase, final) {
    this.name = name;
    this.phrase = phrase;
    this.run = function () {
        if (final){
            session.execute("detect_speech", "stop");
        }
        console_log("running phrase " + this.name);
        session.streamFile(phrase, ignoreInput);
        if (final){
            session.hangup();
        }
    }
}

function Result(name, neededConfidence) {
    this.name = name;
    this.neededConfidence = neededConfidence;
}

// state
this.request1 = new YnRequest("first request", "drivers-dev/1_greet_request_short.wav", 3000, 2);
this.request1["да"] = new Result("yes", 60);
this.request1["нет"] = new Result("no", 95);

this.request2 = new YnRequest("second request", "drivers-dev/2_details_question_short.wav", 3000, 2);
this.request2["да"] = new Result("yes", 60);
this.request2["нет"] = new Result("no", 95);

this.finishFail = new Phrase("finish fail", "drivers-dev/4_thanks_fail.wav", true);
this.finishSuccess = new Phrase("finish success", "drivers-dev/3_thanks_success.wav", true);

// listeners
function ignoreInput(session, type, data, arg) {
    if (type == "dtmf") {
        console_log("data=" + JSON.stringify(data));
    }
    return true;
}

// arg = request
function onInput(session, type, data, request) {
    //console_log("processing onInput, type=" + type + "; data="+JSON.stringify(data));
    if (type != "event") {
        console_log("type is not event, type=" + type + "; returning");
        return true;
    }
    //console_log("processing event " + data.serialize())
    var eventName = data.getHeader("Event-Name");
    if (eventName != "DETECTED_SPEECH") {
        console_log("not detected speech event: " + eventName);
        return true;
    }
    var speechType = data.getHeader("Speech-Type");
    if (speechType == "begin-speaking") {
        console_log("begin speaking detected, do nothign");
        return true;
    }
    if (speechType != "detected-speech") {
        console_log("expected detected-speech event.speechType, but got " + speechType + "; ignore");
        return true;
    }

    var body = data.getBody();
    if (!body) {
        console_log("body is empty: " + data.serialize());
        return true;
    }

    var xml;
    var result;
    var interpretation;
    var input;
    var confidence;

    body = body.replace(/<\?.*?\?>/g, '');
    xml = new XML("<xml>" + body + "</xml>");
    result = xml.getChild('result');
    interpretation = result.getChild('interpretation');
    input = interpretation.getChild('input').data;
    confidence = interpretation.getAttribute('confidence');

    console_log("result=" + input + ";" + confidence);

    //return request.processResult(input, confidence);
    return request.updateResult(input, confidence);
}

initAsr = function(){
    session.execute("detect_speech", "pocketsphinx yes_no_ru yes_no_ru.gram");
    session.execute("detect_speech", "pause");
}

// dialplan
if (session.ready()) {
    session.answer();
    if (!session.answered()) {
        console_log("session not answered; finishing");
        return;
    }

    initAsr();

    request1.run();
    console_log("result 1 = " + request1.result);

    if (request1.result == "no") {
        this.finishFail.run();
    } else {
        request2.run();
        console_log("result 2 = " + request2.result);

        if (request2.result == "yes"){
            this.finishSuccess.run();
        } else{
            this.finishFail.run();
        }

    }
}

