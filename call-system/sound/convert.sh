# why: from DefinitiveGuide, page 88, need to convert mp3 to uncompressed file to preserve CPU
#usage
#1 generate sounds using PacketSpeechGenerator
#2 ./convert.sh musicName client
#provide musicName without .mp3 extension
#musicName.mp3 should be located in sounds folder
ext="ogg"
mkdir -p sounds/$2
docker run --rm --name sox -v $(pwd)/to-convert/clients/$2:/tmp/sounds sox \
 $1.$ext -t raw -r 16k -e signed-integer -b 16 -c 1 $1.sln \
   && mv -f to-convert/clients/$2/$1.sln sounds/$2/$1.sln16