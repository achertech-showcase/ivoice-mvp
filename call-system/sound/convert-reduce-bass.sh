# why: from DefinitiveGuide, page 88, need to convert mp3 to uncompressed file to preserve CPU
#usage ./convert.sh musicName
#provide musicName without .mp3 extension
#musicName.mp3 should be located in sounds folder
docker run --rm --name sox -v $(pwd)/to-convert:/tmp/sounds sox \
 $1.wav -t raw -r 16k -c 1 -b 16 $1.sln bass -15 treble +10 \
   && cp to-convert/$1.sln sounds/$1.sln16 && rm -f to-convert/$1.sln