import { Injectable } from '@angular/core';
import { Headers, Http, Response } from '@angular/http';
import { Observable } from 'rxjs/Observable';

import 'rxjs/add/operator/map'
import 'rxjs/Rx';

import { Order } from './order';

@Injectable()
export class OrderService {

  private headers = new Headers({ 'Content-Type': 'application/json' });
  private ordersUrl = 'demo-api/orders/last';

  constructor(private http: Http) { }

  getOrders(): Observable<string[]> {
    return this.http.get(this.ordersUrl)
                    .map((res: Response) => res.json())
    //              .do(data => console.log('server data:', data))  // debug
                    .catch(this.handleError);
  }

  private handleError(error: any) {
    // In a real world app, we might use a remote logging infrastructure
    // We'd also dig deeper into the error to get a better message
    let errMsg = (error.message) ? error.message :
      error.status ? `${error.status} - ${error.statusText}` : 'Server error';
    console.error(errMsg); // log to console instead
    return Observable.throw(errMsg);
  }
}

