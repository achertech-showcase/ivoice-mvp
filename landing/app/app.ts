//our root app component
import { Component, NgModule, VERSION } from '@angular/core'
import { HttpModule } from '@angular/http'
import { BrowserModule } from '@angular/platform-browser'

import { Subscription } from 'rxjs/Subscription';
import { TimerObservable } from 'rxjs/observable/TimerObservable';

import { OrderService } from './order.service';
import { Order } from './order';

import 'rxjs/Rx';

@Component({
  selector: 'demo-app',
  templateUrl: './app/orders.html'
})
export class App {
  orders: Order[];
  private ordersRefreshTask:Subscription;
  private errorMessage: string;

  constructor(
    private orderService: OrderService) { }

  ngOnInit(): void {
    console.log('start periodical orders refresh');
    this.ordersRefreshTask = TimerObservable.create(0, 5000).subscribe(
      _ => this.loadOrders()
    );
  }

  private loadOrders(): void {
    this.orderService
      .getOrders()
      .subscribe(
        orders => this.orders = orders,
        error => this.errorMessage = <any>error
      );
  }
}

@NgModule({
  imports: [BrowserModule, HttpModule],
  declarations: [App],
  providers: [OrderService],
  bootstrap: [App]
})
export class AppModule { }