
console.log("got here");
var page = require('webpage').create();

page.onConsoleMessage = function (msg) {
    console.log(msg);
};

document.click = function (el) {
    var ev = document.createEvent("MouseEvent");
    ev.initMouseEvent(
        "click",
        true /* bubble */, true /* cancelable */,
        window, null,
        0, 0, 0, 0, /* coordinates */
        false, false, false, false, /* modifier keys */
        0 /*left*/, null
    );
    el.dispatchEvent(ev);
}

function openPage(pageToOpen){
    console.log("opening page "+pageToOpen);
    page.open(pageToOpen, function(status) {
        window.setTimeout(function () {
            page.render('dispatcher.png');
            phantom.exit();
        }, 5000);
        
    })
}

page.open("https://passport.yandex.ru/auth", function (status) {
    if (status === "success") {
        page.evaluate(function () {
            var click = function (el) {
                var ev = document.createEvent("MouseEvent");
                ev.initMouseEvent(
                    "click",
                    true /* bubble */, true /* cancelable */,
                    window, null,
                    0, 0, 0, 0, /* coordinates */
                    false, false, false, false, /* modifier keys */
                    0 /*left*/, null
                );
                el.dispatchEvent(ev);
            }


            document.querySelector("input[name='login']").value = "name";
            document.querySelector("input[name='passwd']").value = "pass";
            var buttonElement = document.querySelector(".passport-Button-Text");
            click(buttonElement);
            //   document.querySelector("input[name='email']").value = "email";
            //   document.querySelector("input[name='pass']").value = "pass";
            //   document.querySelector("#login_form").submit();

            console.log("Login submitted!");
        });
        window.setTimeout(function () {
            page.render('colorwheel.png');
            openPage("https://lk.taximeter.yandex.ru/dispatcher/new");
            //phantom.exit();
        }, 5000);
    }
});